import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
    orientationProfile: ['profileType']
})

export const orientationTypes = Types
export default Creators


/* ------------- Initial State--------- */

const INITIAL_STATE = Immutable({ profileType: null })

/* ------------- Reducers ------------- */

export const profile = (state: Object, { profileType }: Object) =>
  state.merge({ profileType })


  /* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.ORIENTATION_PROFILE]: profile
})