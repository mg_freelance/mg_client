import { Platform } from 'react-native';
import apisauce from 'apisauce';

import { PRIMARY, COLOR } from '../Config/config';

/**
 * Detect whether a color is a hex code/rgba or a paper element style
 * @param string
 * @returns {*}
 */
export function getColor(string) {
    if (string) {
        if (string.indexOf('#') > -1 || string.indexOf('rgba') > -1) {
            return string;
        }

        if (COLOR[string]) {
            return COLOR[string].color;
        }

        if (COLOR[`${string}500`]) {
            return COLOR[`${string}500`].color;
        }
    }

    return COLOR[`${PRIMARY}500`].color;
}

/**
 * Detect whether a specific feature is compatible with the device
 * @param feature
 * @returns bool
 */
export function isCompatible(feature) {
    const version = Platform.Version;

    switch (feature) {
        case 'TouchableNativeFeedback':
            return version >= 21;
            break;
        case 'elevation':
            return version >= 21;
            break;
        default:
            return true;
            break;
    }
}

export function api(){
    let baseURL = 'http://api.flowlogic.net'; //'http://ec2-35-164-213-235.us-west-2.compute.amazonaws.com';
    const API = apisauce.create({
            "baseURL": baseURL,
            "headers": {
                'Cache-Control': 'no-cache'
            },
            "timeout": 10000
        });

    return API
}
