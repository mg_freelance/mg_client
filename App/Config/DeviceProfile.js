import {PixelRatio} from 'react-native'



export default function getScreenProfile(){
        let ratio = PixelRatio.get()
        console.log(ratio)
        if(ratio == 1){
            return 'mdpi'
        }else if(ratio == 1.5){
            return 'hdpi'
        }else if(ratio == 2){
            return 'xhdpi'
        }else if(ratio == 3){
            return 'xxhdpi'
        }else if(ratio > 3){
            return 'ldpi'
        }else{
            return 'unknown'
        }
}
