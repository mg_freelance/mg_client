// @flow

import React from 'react'
import {
  View,
  ScrollView,
  Text,
  TextInput,
  TouchableOpacity,
  Image,
  Keyboard,
  LayoutAnimation,
  BackAndroid,
  Dimensions,
  StatusBar
} from 'react-native'
import { connect } from 'react-redux'
import Styles from './Styles/LoginScreenStyle'
import {Images, Metrics} from '../Themes'
import LoginActions from '../Redux/LoginRedux'
import { Actions as NavigationActions } from 'react-native-router-flux'
import I18n from 'react-native-i18n'
import dismissKeyboard from 'react-native-dismiss-keyboard'
import TextField from 'react-native-md-textinput'
import Orientation from 'react-native-orientation';

import Pin from './Screens/Login/pinScreenLandscape'
import LoginRow from './Screens/Login/LoginRow'
import {Ripple} from  'react-native-material-design';
import ActionButton from 'react-native-action-button';
import SelectionModal from './Screens/Setup/User/Modals/SelectionModal';
import REDUX_PERSIST from '../Config/ReduxPersist';
import Indicator from '../Components/Indicator';

import FJSON from 'format-json'


//******************* TEMP **********************************



//***********************************************************

type LoginScreenProps = {
  dispatch: () => any,
  fetching: boolean,
  fetched: boolean,
  attemptLogin: () => void
}

const imageDimensions = {
  height: window.height,
  width: window.width
}

class LoginScreen extends React.Component {


  props: LoginScreenProps
  //api: Object

  state: {
    username: string,
    password: string,
    visibleHeight: number,
    topLogo: {
      width: number
    },
    show: boolean,
    isQModelOpen: boolean
  }

  isAttempting: boolean
  keyboardDidShowListener: Object
  keyboardDidHideListener: Object
  response: {
    
  }

  constructor (props: LoginScreenProps) {
    super(props)

    this.state = {  
      username: '',
      password: '',
      sitemail: 'lasitha.petthawadu@bback.com',
      visibleHeight: Dimensions.get('window').height,
      visibleWidth: Metrics.screenWidth,
      topLogo: { width: Metrics.screenWidth },
      show: false,
      isQModelOpen: false,
      currentHighlightedIndx:1,
      users:[],
      isExisting: false,
      animating: true 
      
    }      
       this.isAttempting = false

  }

  componentWillReceiveProps (newProps) {
    this.setState({animating: true})
    this.forceUpdate()
    // Did the login attempt complete?
    if (newProps.fetched && newProps.error==null) {
          NavigationActions.drawer({type:'reset'});
    }else if (newProps.error != null){
          window.alert(newProps.error);
          this.setState({animating: false})
    }
  }


  toggleModal(modalName, openStatus) {
        this.setState({
          isQModelOpen: openStatus,
        });
    }
  componentWillMount () {

    //Orientation Lock
    Orientation.lockToLandscape()

    // Using keyboardWillShow/Hide looks 1,000 times better, but doesn't work on Android
    // TODO: Revisit this if Android begins to support - https://github.com/facebook/react-native/issues/3468
    this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this.keyboardDidShow)
    this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this.keyboardDidHide)
    
    
  }

  componentWillUnmount () {
    this.keyboardDidShowListener.remove()
    this.keyboardDidHideListener.remove()
  }

  keyboardDidShow = (e) => {
    // Animation types easeInEaseOut/linear/spring
    // LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut)
    
    // let newSize = Metrics.screenHeight - e.endCoordinates.height
    this.setState({
      show: true
      
    })
  }

  keyboardDidHide = (e) => {
    // Animation types easeInEaseOut/linear/spring
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut)
    this.setState({
      visibleHeight: Dimensions.get('window').height,
      topLogo: {width: Metrics.screenWidth},
      show: false
    })
  }

  handlePressLogin = () => {
    dismissKeyboard() 
    const { username, password } = this.state
    this.isAttempting = true
   
    // attempt a login - a saga is listening to pick it up from here.
    this.props.attemptLogin(username, password, this.response)
    
  }

 

componentDidMount(){
      let logged = '';
      BackAndroid.addEventListener('hardwareBackPress',()=>{
          return BackAndroid.exitApp(0)
      });
      
      REDUX_PERSIST.storeConfig.storage.getItem('loggedUser', (error, result)=>{
          if(result!= null){
            logged = result
          }
      });

     
      REDUX_PERSIST.storeConfig.storage.multiGet(['User1', 'User2','User3'],(err, value)=>{
             console.log('******************************************************************')
             let userdata = {}
            
             if(err || value[0][1]==null ){
                this.setState({
                  animating: false
                })
             }else{
               //console.log(value[0][1])
                userdata = value.map((usr)=>{
                  return usr[1]
                })
                .reduce((details,usr)=>{
                  
                  if (usr != null) {
                    details.push(JSON.parse(usr))
                    
                  }else{
                    details.push(null)
                  };

                  return details
                }, [])
                .sort((l1, l2)=>{
                  if(l1 == null){
                    return 1
                  }else if(l2 == null){
                    return -1
                  }else{
                    return (new Date(l2.TimeStamp) - new Date(l1.TimeStamp)) < 0 ? -1 : 1 
                  }
                })

                
                this.setState({
                        users: userdata ,
                        password: '',
                        username: userdata[0].username,
                        currentHighlightedIndx: 1, 
                        newUserPresent: false,
                        isExisting:true,
                        animating: false                  
                });     
             }
        });

  }


addUser(text){

  this.setState({
    isQModelOpen: false,
    newUserPresent: true,
    username : text,
    isExisting: false,
    currentHighlightedIndx: 1,
    tempUsr: text
    

  })

}

userPresent(text){
 
  if(this.state.users == null){
    return false
  }else{
    let present = this.state.users.find((user)=>{
        
      if(user != null ){
        return user.username.toUpperCase() == text.toUpperCase()
      }else{
        return false
      }
    })
    return present? true: false
  }
}

getImage(id){
 
  if(this.state.users==null){
    return null
  }else{
    if(this.state.users[id]!=null){
      return this.state.users[id].profileImage
    }else{
      return null
    }
  }
}

getuser(id){    
  if(this.state.users==null){
      return null
  }else{
    if(this.state.users[id]!=null){
        return this.state.users[id].username
    }else{
        return null
    }
  }
}

render () {
    const { username, password } = this.state
    const { fetching } = this.props
    const {fetched} = this.props
    const editable = true//!fetching
    
    const textInputStyle = editable ? Styles.textInput : Styles.textInputReadonly
    const onKeyboardshow = this.state.show ? Styles.onKeyboardShow : Styles.contentContainer

   

    if (this.state.newUserPresent) {

       return (
          <View style={{ flex:1, backgroundColor: 'transparent' }}>
            
            <View>
              <Image style={[Styles.backgroundImage, {position: 'absolute'}]} source={Images.Loginback} />
            </View>
            
            <View contentContainerStyle={onKeyboardshow} style={[Styles.container, {flex: 1, flexDirection:'column', alignItems: 'center', justifyContent: 'center', height: this.state.visibleHeight}]} keyboardShouldPersistTaps>
                  <View style = {[Styles.form, {flex: 2}]}>
                      <Image source={Images.logo} style={[Styles.topLogo]} />
                  </View>

                  <View style={[Styles.panel,{justifyContent:'center', alignItems:'flex-start', marginBottom: 30}]}>
                    <View style={{flex: 1}}></View>
                    <View style={[Styles.form,{flex:4 ,flexDirection: 'column', justifyContent:'flex-end', alignItems: 'flex-end'}]}>
                    
                        <LoginRow direction={'row'} height={85} fontsize={16} fontlftMrgn={-70} width={250} highlighted ={this.state.currentHighlightedIndx} user = {this.state.tempUsr} indx = {1} userImg = {"new"} onPress={
                            (id)=> {this.setState(
                                {username: this.state.tempUsr, currentHighlightedIndx: id}
                            )}
                        }/>

                        <LoginRow direction={'row'} height={85} fontsize={16} fontlftMrgn={-70} width={250} highlighted ={this.state.currentHighlightedIndx} user = {this.getuser(0)} indx = {2} userImg = {this.getImage(0)} onPress={
                            (id)=> {this.setState(
                                {username: this.getuser(0), currentHighlightedIndx: id, isExisting: true}
                            )}
                        }/>

                        <LoginRow direction={'row'} height={85} fontsize={16} fontlftMrgn={-70} width={250} highlighted ={this.state.currentHighlightedIndx} user = {this.getuser(1)} indx = {3} userImg = {this.getImage(1)} onPress={
                            (id)=> {this.setState(
                                {username: this.getuser(1), currentHighlightedIndx: id, isExisting: true}
                            )}
                        }/>
                          
                        <View style={{ flexDirection: 'row',width:250,  height: 100, backgroundColor:'rgba(255,255,255,.3)', justifyContent:'flex-start', alignItems: 'center'}}>
                            <Ripple style={[Styles.rippleContainer,{borderRadius:10}]} color='rgba(255,255,255,.1)' onPress={()=>{this.toggleModal('isQModelOpen', true)}}>
                                <Image source={Images.FABIcon} style={[Styles.FAB,{marginLeft:-60}]} />
                                <Text style={{color: 'white'}}>           ADD USER  </Text>
                            </Ripple>
                        </View>
                    
                    </View>

                    <View style= {[Styles.form, {flex:4, paddingTop: 30, backgroundColor: 'rgba(255,255,255,.08)'}]}>
                          <Pin username={this.state.username} exsiting = {this.state.isExisting}/>   
                    </View>
                    <View style={{flex: 2}}></View>
                </View>
                <View style={{height:30}}></View>
            </View>
            
             <Indicator animating={this.state.animating}/> 
            
            <SelectionModal
                  modalType="loginModel"
                  width = {Dimensions.get('window').width/2}
                  title="NEW USER LOGIN"
                  isOpen={this.state.isQModelOpen}
                  label="Username"
                  onClosed={() => { this.toggleModal('isQModelOpen', false) } }
                  onApply={(text) => { if(text ==''){
                    alert('Username Empty !')
                  }else{
                      if(this.userPresent(text)){
                          alert('User Already Existing !')
                      }else{
                          this.addUser(text)
                      }
                      
                    }
                  }} />
          
          
          
        </View>
      )

    }else{
      return(
        <View style={{ flex:1, backgroundColor: 'transparent' }}>
          
          <View>
            <Image style={[Styles.backgroundImage, {position: 'absolute'}]} source={Images.Loginback} />
          </View>
          
          <View contentContainerStyle={onKeyboardshow} style={[Styles.container, {flex: 1, flexDirection:'column', alignItems: 'center', justifyContent: 'center', height: this.state.visibleHeight}]} keyboardShouldPersistTaps>
                <View style = {[Styles.form, {flex: 2}]}>
                    <Image source={Images.logo} style={[Styles.topLogo]} />
                </View>
                
              <View style={[Styles.panel,{justifyContent:'center', alignItems:'flex-start', marginBottom: 30}]}>
                <View style={{flex: 1}}></View>
                  <View style={[Styles.form,{flex: 4,flexDirection: 'column', justifyContent:'flex-end', alignItems: 'flex-end'}]}>
                    
                        <LoginRow direction={'row'} height={85} fontsize={16} fontlftMrgn={-70} width={250} highlighted ={this.state.currentHighlightedIndx} user = {this.getuser(0)} indx = {1} userImg = {this.getImage(0)} onPress={
                          (id)=> {this.setState(
                            {username: this.getuser(0) , currentHighlightedIndx: id, isExisting: true}
                          )}
                        }/>

                        <LoginRow direction={'row'} height={85} fontsize={16} fontlftMrgn={-70} width={250} highlighted ={this.state.currentHighlightedIndx} user = {this.getuser(1)} indx = {2} userImg = {this.getImage(1)} onPress={
                          (id)=> {this.setState(
                            {username: this.getuser(1), currentHighlightedIndx: id, isExisting: true}
                          )}
                        }/>

                        <LoginRow direction={'row'} height={85} fontsize={16} fontlftMrgn={-70} width={250} highlighted ={this.state.currentHighlightedIndx} user = {this.getuser(2)} indx = {3} userImg = {this.getImage(2)} onPress={
                          (id)=> {this.setState(
                            {username: this.getuser(2), currentHighlightedIndx: id, isExisting:true }
                          )}
                        }/>
                        
                        
                      <View style={{ flexDirection: 'row', width: 250,  height: 100, backgroundColor:'rgba(255,255,255,.3)', justifyContent:'flex-start', alignItems: 'center'}}>
                          <Ripple style={[Styles.rippleContainer,{borderRadius:10}]} color='rgba(255,255,255,.1)' onPress={()=>{this.toggleModal('isQModelOpen', true)}}>
                              <Image source={Images.FABIcon} style={[Styles.FAB,{marginLeft:-60}]} />
                              <Text style={{color: 'white'}}>           ADD USER  </Text>
                          </Ripple>
                      </View>
                   
                  </View>

                  <View style= {[Styles.form, {flex: 4, paddingTop: 30, backgroundColor: 'rgba(255,255,255,.08)'}]}>
                        <Pin username={this.state.username} exsiting = {this.state.isExisting}/>   
                  </View>
                  <View style={{flex: 2}}></View>
              </View>
               <View style={{height:30}}></View>
          </View>

          <Indicator animating={this.state.animating}/> 
          
         <SelectionModal
                modalType="loginModel"
                width = {Dimensions.get('window').width/2}
                title="NEW USER LOGIN"
                isOpen={this.state.isQModelOpen}
                label="Username"
                onClosed={() => { this.toggleModal('isQModelOpen', false) } }
                onApply={(text) => { if(text ==''){
                  alert('Username Empty !')
                }else{
                    if(this.userPresent(text)){
                        alert('User Already Existing !')
                    }else{
                        this.addUser(text)
                    }
                  }
                }} />
        
        
         
        </View>
      )

     }

  }

}

const mapStateToProps = (state) => {
  return {
    fetching: state.login.fetching,
    fetched : state.login.fetched,
    error: state.login.error 
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    attemptLogin: (username, password, response) => dispatch(LoginActions.loginRequest(username, password, response))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(LoginScreen)



/**
 * 

// Hide Status Bar
 <StatusBar hidden= {true}/> 


 <ListView contentContainerStyle={{ width: 275}}
                  dataSource={this.state.dataSource}
                  enableEmptySections={true}
                  initialListSize = {3}
                  renderRow={(rowData, sectionId, rowId) => {return (<LoginRow data={rowData} highlighted={this.state.currentHighlightedIndx} onPress={(id)=>{
                    const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });

                    this.setState({currentUser:this.state.userList[id].username,currentHighlightedIndx:id,dataSource:ds.cloneWithRows(this.state.userList)});
                   }}/>)}}>
                  //renderSeparator={(sectionId, rowId, hightlighted) => <View key={rowId}  style={{height: 1, backgroundColor: hightlighted ? 'transparent' : 'white'}} />}
                  
                </ListView>
              
                <View style={{width: 260, height :140, flexDirection: 'row',margin:10}}>
                    <Ripple style={[Styles.rippleContainer,{borderRadius:10}]} color='rgba(255,255,255,.5)' onPress={()=>{this.toggleModal('isQModelOpen', true)}}>
                        <ActionButton
                          buttonColor="rgba(76,175,80,1)"
                          position= 'left'
                          hideShadow= {true}
                          text= 'test'
                          offsetX= {15}
                          offsetY={-5}
                          onPress={()=>{this.toggleModal('isQModelOpen', true)}}
                        />
                        <Text style= {Styles.text}>Add New User</Text>
                    </Ripple>
                </View>



 * 
 * Model******************************************************************
 * 
 * <SelectionModal
                modalType="loginModel"
                title="NEW USER LOGIN"
                isOpen={this.state.isQModelOpen}
                label="Email"
                onClosed={() => {  } }
                onApply={() => { console.log('Applied') } } />
**************************************************************************



*Login Form 
 * 
 * <TextField
                  //textColor= 'white'
                  label='Username'
                  labelStyle={{fontSize:20}}   
                  ref='username'
                  style={[Styles.row]}
                  value={username}
                  editable={editable}
                  keyboardType='default'
                  returnKeyType='next'
                  autoCapitalize='none'
                  autoCorrect={false}
                  onChangeText={this.handleChangeUsername}
                  underlineColorAndroid='transparent'
                  onSubmitEditing={() => this.refs.password.focus()}
                  highlightColor= "white"
                  //dense= {true}
                
                  //placeholder={I18n.t('username')}
                  
                  />  
                  <View style= {{paddingTop: 20}} >
                  </View>        

                <TextField
                  label= 'PIN'
                  
                  labelStyle={{fontSize:20}}                
                  //dense= {true}  
                  ref='password'
                  style={Styles.row}
                  value={password}
                  editable={editable}
                  keyboardType = 'numeric'
                  returnKeyType='go'
                  autoCapitalize='none'
                  autoCorrect={false}
                  secureTextEntry
                  onChangeText={this.handleChangePassword}
                  underlineColorAndroid='transparent'
                  onSubmitEditing={this.handlePressLogin}
                  maxLength={4}
                  textColor= 'white'
                  highlightColor='white' 
                  fontSize = {20}                     
                  />
            

              <View style={[Styles.loginRow]}>
                <TouchableOpacity style={Styles.loginButtonWrapper} onPress={this.handlePressLogin}>
                  <View style={Styles.loginButton}>
                    <Text style={Styles.loginText}>{I18n.t('signIn')}</Text>
                  </View>
                </TouchableOpacity>           
              </View>
 * 
 */