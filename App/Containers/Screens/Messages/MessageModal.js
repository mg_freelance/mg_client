// @flow
import React, { Component } from 'react';
import { Colors, Metrics } from '../../../Themes'
import {
    StyleSheet,
    ListView,
    Text,
    ScrollView,
    View,
    Dimensions,
    TextInput
} from 'react-native';
import { Avatar, Checkbox, Drawer, Divider, PRIMARY_COLORS, TYPO, Ripple } from 'react-native-material-design';
import ActionButton from 'react-native-action-button';
import Icon from 'react-native-vector-icons/Ionicons';
import Modal from 'react-native-modalbox';
//import TextField from 'react-native-md-textinput';
import Button from '../../../Components/Button'



export default class MessageModal extends Component {

    constructor() {
        super();
        this.state = {
            initial: true,
            checkbox: {},
            selectionList: [],
            //width: modelWidth
        };
    }

    
    getModelWidth(){
        let modelWidth = Dimensions.get('window').width

        return Dimensions.get('window').width>500 ? modelWidth/3: 7* modelWidth/8
    }

    getModelHeight(){
        let modelHeight = Dimensions.get('window').height

        return modelHeight>500 ? modelHeight/2: 7* modelHeight/8
    }

    componentWillMount() {
        this.state = { 
                ...this.state,
                textField: ''

        }
    }

    updateState() {
        this.setState({ 
            ...this.state,
        });
}

componentWillReceiveProps(nextProps) {
    
}

render() {

        return (
            <Modal onClosed={() => { this.props.onClosed() } } position="top" isOpen={this.props.isOpen} style={{ zIndex: 1000, marginTop: 80, padding: 20, width: this.getModelWidth() , height: this.getModelHeight(), backgroundColor: '#ffffff' }}>
                <View style={{flex:4}}>
                    <Text style={{fontSize: 18, color: 'black', fontWeight: '200', paddingBottom: 10}}>{this.props.title}</Text>
                    <ScrollView>
                         <Text style={{color: '#555555'}}>{this.props.description}</Text>
                    </ScrollView>
                </View>
                <View style={{flex:1,flexDirection: 'row', marginTop: 25, alignItems:'flex-end', justifyContent: 'flex-end'}}>
                    <Button overrides={{ backgroundColor: '#5fa54a', textColor: '#ffffff', rippleColor: '#a5f48d' }} text="     Reply     " raised={true} onPress={() => {
                            this.props.onApply(this.state.textField);
                    } } />

                    <Button overrides={{ backgroundColor: '#5fa54a', textColor: '#ffffff', rippleColor: '#a5f48d' }} text="      OK       " raised={true} onPress={() => {
                            this.props.onApply(this.state.textField);
                    } } />
                </View>
            </Modal>
        );   



}

}