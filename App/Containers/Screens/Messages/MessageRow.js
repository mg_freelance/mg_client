import React, { Component } from 'react';
import { View, Text, StyleSheet, Image, Dimensions } from 'react-native';
import { Avatar, Drawer, Divider, PRIMARY_COLORS, TYPO, Ripple } from 'react-native-material-design';
import { Actions as NavigationActions } from 'react-native-router-flux';
import ActionButton from 'react-native-action-button';
import Icon from 'react-native-vector-icons/MaterialIcons';






const styles = StyleSheet.create({
    container: {
        flex: 1,
        margin: 5,
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: 'white',
        elevation: 2
    },
    text: {
        marginLeft: 12,
        marginTop: 0,
        fontSize: 16,
    },
    photo: {
        height: 60,
        width: 60,
        borderRadius: 60,
    },
    rippleContainer: {
        flex: 1,
        flexDirection: 'row',
        height: 80,
        padding: 12,
    },
});

export default class MessageRowLand extends React.Component {
    constructor(props) {
        super(props)
    }

    getflexValue(){
        let modelWidth = Dimensions.get('window').width

        return modelWidth>500 ? 1: 2
    }
    
    MsgIcons = {
            "notification": 'error-outline',
            "message": "assignment",
            "critical": 'assignment-late',
        }

    getContent(){
        if(this.props.content == 'message'){
            //console.log(this.MsgIcons[this.props.Msgtype])
            return (
                    " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer sed est ligula. Mauris arcu dolor, dignissim non ornare ut, auctor nec nulla. Maecenas et arcu molestie, tempus dui sit amet, finibus lacus. Donec faucibus urna eget lacus venenatis, eu dictum sapien euismod. Vivamus posuere sapien ligula, luctus aliquam ante ullamcorper ac. Curabitur nec tristique turpis. Proin accumsan est eu ipsum mattis, non sagittis ex malesuada. Praesent tristique leo quis magna hendrerit finibus. Maecenas sollicitudin tortor ac luctus consequat. Sed odio eros,ultricies in neque id, tristique bibendum massa. Maecenas orci nisl, interdum at nunc id, scelerisque sollicitudin ipsum. Fusce placerat, diam suscipit venenatis sollicitudin, orci ligula suscipit lectus, nec consectetur dui felis a nisl."
            )
                
        }else if(this.props.content == 'notification'){
            return ("this is a notification..")
            
        }

    }

    render() {
        return (
            <View style={styles.container}>
                <Ripple text="" style={styles.rippleContainer} color='rgba(0,0,0,.5)' onPress={()=>{
                    this.props.content == 'notification'? NavigationActions.userView({ data: this.props.data,create:false })
                    : this.props.onPress(true, this.getContent())
                }} >

                    <Image source={{ uri: this.props.data.profileImg }} style={styles.photo} />

                    <View style={{ flex: 4, flexDirection: 'column',justifyContent: 'center'}}>
                        <View style= {{flex: 1, marginTop:5}}>
                            <Text style={[styles.text,{color: 'black', fontWeight: '200'}]}>
                                {this.props.data.firstName} {this.props.data.surName}
                            </Text>
                        </View>
                        <View style= {{flex: 1}}>
                            <Text numberOfLines = {1} style= {styles.text}>
                                {this.getContent()}
                            </Text>    
                        </View>
                    </View>
                    <View style={{flex: this.getflexValue(), flexDirection: 'column', alignItems:'flex-end'}}>
                        <View style= {{flex: 1, paddingLeft: 15}}>
                            <Text style={styles.text}>Today</Text>    
                        </View>
                    
                        <Icon name={this.MsgIcons[this.props.content]} size={25} color="#aaaaaa" />
                     
                       
                    </View>
                </Ripple>
            </View>
        );
    }
}


/**
 * 
 * <Ripple style={styles.rippleIconcontainer} color='rgba(0,0,0,.5)'>
                             <Image source={Images.ApproveIcon} style={[styles.photo]} />
                        </Ripple>
                         <Ripple style={styles.rippleIconcontainer} color='rgba(0,0,0,.5)'>
                             <Image source={Images.RejectIcon} style={[styles.photo]} />
                        </Ripple>
 */