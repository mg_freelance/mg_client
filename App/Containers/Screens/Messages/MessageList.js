// @flow
import React, { Component } from 'react';
import { Colors, Metrics } from '../../../Themes'
import {
  StyleSheet,
  ListView,
  Text,
  ScrollView,
  View,
  Dimensions
} from 'react-native';
import MessageRow from './MessageRow';
import ActionButton from 'react-native-action-button';
import { Actions as NavigationActions } from 'react-native-router-flux'
import Icon from 'react-native-vector-icons/MaterialIcons';
import {api} from '../../../Lib/helpers';


const MessageIcon = (<Icon name="mail-outline" size={25} color="#ffffff" />)


//const getUsers = () => api.get('user');

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#eeeeee',
    paddingHorizontal: 5
  },
  separator: {
    flex: 1,
    height: StyleSheet.hairlineWidth,
    backgroundColor: '#8E8E8E',
  },
});

export default class MessageList extends Component {
  constructor() {
    super();

    const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
    this.state = {
      dataSource: ds.cloneWithRows([]),
    };

  }

  componentDidMount(){
    api().get('user').then((response) => {
      //console.log(response);
      const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
      this.setState({

        dataSource: ds.cloneWithRows(response.data),
      });
    });
  }

  // getMsgType(){
  //   let messages = ['normalMsg', 'alert']
  //   return messages[Math.floor(Math.random()*2)]
  // }

  getcontentType(){
    let messages = ['message', 'notification']
    return messages[Math.floor(Math.random()*2)]
  }

  // getRowView(){
  //       let modelWidth = Dimensions.get('window').width

  //       return Dimensions.get('window').width>500 ? ((rowData) => { 
  //         return (
  //           <MessageRowLand 
  //             onPress={(pressed, description)=>{this.props.modalState(pressed, rowData.firstName+ " "+ rowData.surName, description)}} 
  //             data={rowData} 
  //             content ={this.getcontentType()}/>) } )

  //       :((rowData) => { 
  //         return (
  //           <MessageRow
  //             onPress={(isOpen, approvedState)=>{this.props.modalState(isOpen, approvedState)}} 
  //             data={rowData} 
  //             content ='message'/>) } )
  // }
  
  // offset = 0
  // scrollDirection(){
  //   (event)=>{
  //       let currentOffset = event.nativeEvent.contentOffset.y;
  //       let direction = currentOffset > this.offset ? 'down' : 'up';
  //      // this.offset = currentOffset;
  //       console.log(direction);
  //   }
      
  // }

  render() {
     
    return (
      <View style={styles.container}>
        <ScrollView style={styles.container} onScroll= {console.log('********SCROLLED')}>
          <ListView style={{ backgroundColor: 'transparent' }}
            dataSource={this.state.dataSource}
            enableEmptySections={true}
            renderRow={
               (rowData) => { 
                  return (
                    <MessageRow 
                      onPress={(pressed, description)=>{this.props.modalState(pressed, rowData.firstName+ " "+ rowData.surName, description)}} 
                      data={rowData} 
                      content ={this.getcontentType()}/>
                  ) 
                }   
            }
            //renderSeparator={(sectionId, rowId) => <View key={rowId} style={styles.separator} />}
            >
          </ListView>
        </ScrollView>
        <ActionButton style={styles.container} buttonColor="#408fff" icon={MessageIcon} offsetY={0} onPress={()=>{alert('To be added')}}>
        </ActionButton>
        
      </View>
      
    );
  }
}