import React, { Component } from 'react';
import { View, StyleSheet, Alert,Dimensions } from 'react-native';
import MessageList from './MessageList';
import { Colors, Metrics } from '../../../Themes';
import MessageModal from './MessageModal'


export default class MessagesScreen extends Component {

     constructor(){
        super();
        this.state= {
            messageMdl:false,
            message: ''
        }
    }

    toggleModal(openStatus) {
        this.setState({
          messageMdl: openStatus
        });
    }

    render(){
        return (
            <View style ={styles.container}>
                <MessageList
                    modalState={(pressed, user, description)=>{
                        this.setState({
                            messageMdl: pressed,
                            name: user,
                            message: description
                        })
                    }}
                />

                <MessageModal
                    //width = {3*Dimensions.get('window').width/4}
                    title={this.state.name}
                    //approvedState = {this.state.isapproved}
                    isOpen={this.state.messageMdl}
                    onClosed={() => {  } }
                    onApply={(text) => { 
                        this.toggleModal(false)
                    }}
                    description ={this.state.message} 
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop:60,
    backgroundColor: Colors.background
  },
  page: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  tabbar: {
    backgroundColor:  Colors.background,
  },
  indicator: {
    backgroundColor: '#408fff',
  },
  label: {
    color: '#222',
    fontWeight: '400',
  },
});