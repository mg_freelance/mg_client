import React, { Component } from 'react';
import { View, StyleSheet, Alert, Text, Dimensions, Picker, Image, ListView, TouchableHighlight,  Animated } from 'react-native';
import HolidayList from './Holidays/HolidayList';
import { Colors, Metrics } from '../../../Themes';
import Button from '../../../Components/Button';
import { Actions as NavigationActions } from 'react-native-router-flux';
import { Images } from '../../../Themes'
import ModalDropdown from 'react-native-modal-dropdown';
import Icon from 'react-native-vector-icons/MaterialIcons';
import DateSelector from './Components/DateSelector';
import { TabViewAnimated, TabBarTop } from 'react-native-tab-view';
import Calendar from './Components/CalendarView/Calendar';
import DayScreen from './Components/DayView/Day';
import ActionButton from 'react-native-action-button';
import ApproveModal from './Holidays/Modals/ApproveModal';
import SetRoleModel from './Components/setRoleModel';
import {api} from '../../../Lib/helpers';


const getPickerWidth = (type) => {
    let Width = Dimensions.get('window').width
    return Width > 500 ? Width / 6 : (type == 'grp' ? Width / 4 : type == 'filter' ? Width / 4 : Width / 2)
}

export default class CalendarScreen extends Component {

    constructor() {
        super();
        this.state = {
            index: 2,
            routes: [
                { key: 'day', title: 'Day' },
                { key: 'week', title: 'Week' },
                { key: 'month', title: 'Month' },
            ],
            groupval: "Group 1",
            filtertype: "Filter by person",
            persons: [],
            roles:[],
            deptments:[]
        }

    }

    componentDidMount() {
        api().get('user').then((response) => {
            let personData = response.data.map((usr) => {
                return {
                    "name": usr.firstName + " " + usr.surName,
                    "image": usr.profileImg,
                    "id": usr.userId
                }
            })
            personData.unshift({name: 'All Users', image: 'http://lambofgodchurch.net/wp-content/uploads/inclusive-icon.png', id: '0'})

            api().get('role').then((roleResponse)=>{
                let roleData = roleResponse.data.map((role)=>{
                  return role.roleName
                })
                roleData.unshift('All roles')


                api().get('departments').then((deptResponse)=>{
                    //console.log(deptResponse.data)
                    //let deptDetails = deptResponse.data.map
                    deptResponse.data.unshift( { departmentId: 'Dept000', departmentName: 'All Departments' })
                    
                    this.setState({
                        persons: personData,
                        roles: roleData,
                        personName: personData[0].name,
                        personImage: personData[0].image,
                        selectedRole: roleData[0],
                        deptments: deptResponse.data,
                        selectedDept: deptResponse.data[0].departmentName
                    })
                })
                
                
            })
            
            
        });
    }

    personRow(rowData) {
        //console.log(rowData[1])
        //let icon = rowData.image
        return (
            <TouchableHighlight style={{ flex: 1, width: getPickerWidth() - 10 }} >
                <View style={{ flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center', padding: 10 }}>
                    <Image style={styles.image} source={{ uri: rowData[1] }} />
                    <Text style={{ paddingLeft: 10, color: 'black', fontSize: 16 }}>{rowData[0]}</Text>
                </View>
            </TouchableHighlight>
        );
    }

    roleRow(roleData) {
        //console.log(rowData[1])
        //let icon = rowData.image
        return (
            <TouchableHighlight style={{ flex: 1, width: getPickerWidth() - 10 }} >
                <View style={{ flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center', padding: 10 }}>
                    <Icon name="build" style={[styles.image,{backgroundColor:'#01579b',padding: 10}]} color= 'white' size={20}/>
                    <Text style={{ paddingLeft: 10, color: 'black', fontSize: 16 }}>{roleData}</Text>
                </View>
            </TouchableHighlight>
        );
    }

    deptRow(roleData) {
        //console.log(rowData[1])
        //let icon = rowData.image
        return (
            <TouchableHighlight style={{ flex: 1, width: getPickerWidth() - 10 }} >
                <View style={{ flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center', padding: 10 }}>
                    <Icon name="storage" style={[styles.image,{backgroundColor:'#01579b',padding: 10}]} color= 'white' size={20}/>
                    <Text style={{ paddingLeft: 10, color: 'black', fontSize: 16 }}>{roleData}</Text>
                </View>
            </TouchableHighlight>
        );
    }

    renderPerson(){        
        return (
            <View style={{ flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center', padding: 10 }}>
                <Image style={[styles.image]} source={{ uri: this.state.personImage }} />
                <Text style={{ paddingLeft: 5, paddingRight: 15, color: 'black', fontSize: 16 }}>{this.state.personName}</Text>
                <Icon name="arrow-drop-down" size={20} color="#000000" />
            </View>
            
        )
    }

    renderRole(){
        return (
                <View style={{ flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center', padding: 10 }}>
                    <Icon name="build" style={[styles.image,{backgroundColor:'#01579b',padding: 10}]} color= 'white' size={20}/>
                    <Text style={{ paddingLeft: 10, color: 'black', fontSize: 16 }}>{this.state.selectedRole}</Text>
                    <Icon name="arrow-drop-down" size={20} color="#000000" />
                </View>
            
        )
    }

    renderDept(){
        return (
                <View style={{ flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center', padding: 10 }}>
                    <Icon name="storage" style={[styles.image,{backgroundColor:'#01579b',padding: 10}]} color= 'white' size={20}/>
                    <Text style={{ paddingLeft: 10, color: 'black', fontSize: 16 }}>{this.state.selectedDept}</Text>
                    <Icon name="arrow-drop-down" size={20} color="#000000" />
                </View>
            
        )
    }

    toggleModal(state){
        this.setState({
            requestMdl: state,
            roleMdl: state
        })
    }

    _handleChangeTab = (index) => {
        this.setState({ index });
    };

    _renderCalendarScene = ({ route }) => {
        switch (route.key) {
            case 'day':
                return (<View style={{ flex:1, backgroundColor: '#ffffff' }}>
                    <DayScreen type="day"/> 
                </View>);
            case 'week':
                return (<View style={{ flex:1, backgroundColor: '#ffffff' }}>
                    <DayScreen type="week"/> 
                </View>);
            case 'month':
                return (<View style={{ flex:1, backgroundColor: '#ffffff' }}>
                    <Calendar data = {[this.state.persons, this.state.roles]}/> 
                </View>);
            default:
                return null;
        }
    };

    render() {
        return (

            <View style={styles.container}>
                <View style={{ backgroundColor: '#FD7C7C', height: 50, flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                    <Text style={styles.text}>5 Leave Requests</Text>
                    <Button 
                        overrides={{ backgroundColor: 'transparent', textColor: '#ffffff', rippleColor: '#ffffff' }} 
                        fontSize={16} 
                        text="Reply" 
                        border={{ color: 'white', radius: 10, width: 1 }} 
                        onPress={() => {
                            NavigationActions.HolidayScreen({personData: this.state.persons, roleData: this.state.roles}) 
                        } } 
                    />
                </View>
                <View style={styles.selectionBar}>
                    
                    <ModalDropdown
                        style={[{ width: getPickerWidth('filter'), flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'flex-start', marginRight:10 }]}
                        options={this.state.persons.map((person) => {
                                        let det = []
                                        det.push(person.name)
                                        det.push(person.image)
                                        return det
                                    })
                                }
                        onSelect={(idx, value)=>{this.setState({ personName: value[0], personImage: value[1] })}}
                        renderRow={this.personRow.bind(this)}
                        >
                        {this.renderPerson()}
                    </ModalDropdown>

                    <ModalDropdown
                        style={[{ width: getPickerWidth('filter'), flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'flex-start', marginRight:10 }]}
                        options={this.state.deptments.map((department) => {
                                        let det = []
                                        det.push(department.departmentName)
                                        return det
                                    })
                                }
                        onSelect={(idx, value)=>{this.setState({ selectedDept: value})}}
                        renderRow={this.deptRow.bind(this)}
                        >
                        {this.renderDept()}
                    </ModalDropdown>

                    <ModalDropdown
                        style={[styles.picker, { width: getPickerWidth(), height: 55, flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'flex-start' }]}
                        options={this.state.roles.map((role) => {
                                        let det = []
                                        det.push(role)
                                        return det
                                    })
                                }
                        onSelect={(idx, value) =>{this.setState({selectedRole: value})}}
                        renderRow={this.roleRow.bind(this)}
                        >
                        {this.renderRole()}
                    </ModalDropdown>
                </View>

                <View style={{ flex: 1, flexDirection: 'row', backgroundColor: Colors.background, justifyContent: 'flex-end', alignItems: 'center' }}>

                    <TabViewAnimated
                        style={{
                            flex: 1,
                            
                        }}
                        navigationState={this.state}
                        renderScene={(route) => {
                            return (
                                <View style={{ flex:1,padding: 10, backgroundColor: '#EEEEEE' }}>
                                    {this._renderCalendarScene(route)}
                                </View>);
                        }}
                        renderHeader={(props) => {
                            return (
                                <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', height: 60, borderBottomWidth: 3, borderBottomColor: '#D6D6D6' }}>
                                    <View style = {{flex:1}}>
                                        <DateSelector selected = {this.state.index}/>
                                    </View>
                                    <View style={{flex:3,alignItems:'flex-end'}}>
                                        <TabBarTop {...props}
                                            tabWidth={80}
                                            style={styles.tabbar}
                                            labelStyle={styles.label}
                                            onRequestChangeTab={this._handleChangeTab}
                                            renderIndicator = {(props)=>{ 
                                                const {position} = props;
                                                const width = 80;
                                                const translateX = Animated.multiply(position, width);
                                                
                                                return (
                                                <Animated.View
                                                    style={[ styles.indicator, { width, transform: [ { translateX } ] }, styles.indicatorStyle ]}
                                                />
                                                );
                                            }}
                                            //indicatorStyle={styles.indicator}
                                            />
                                    </View>
                                </View>
                            );
                        } }
                        onRequestChangeTab={this._handleChangeTab}
                        lazy = {false}
                        />
                </View>

                <ActionButton style={styles.container} buttonColor="#408fff" offsetY={0} >
                
                    <ActionButton.Item buttonColor='#408fff' title="Create Pattern" style ={{height: 20}} onPress={()=>{NavigationActions.PatternView()}}>
                        <Icon name="group-work" style={styles.actionButtonIcon} />
                    </ActionButton.Item>

                    <ActionButton.Item buttonColor='#408fff' title="Assign Role (day)" style ={{height: 20}} onPress={()=>{this.setState({roleMdl: true})}}>
                        <Icon name="account-circle" style={styles.actionButtonIcon} />
                    </ActionButton.Item>

                    <ActionButton.Item buttonColor='#408fff' title="Request Leave" style ={{height: 20}} onPress={()=>{this.setState({requestMdl: true})}}>
                        <Icon name="home" style={styles.actionButtonIcon} />
                    </ActionButton.Item>

                </ActionButton>

                
                <ApproveModal
                    //width = {3*Dimensions.get('window').width/4}
                    modelType={'Request'}
                    title={''}
                    userData = {this.state.persons}
                    roleData = {this.state.roles}
                    userType = {this.state.userType}
                    isOpen={this.state.requestMdl}
                    onClosed={() => { this.toggleModal(false) } }
                    onApply={(data) => { 
                        this.setState({
                            leaveData: data,
                            requestMdl: false
                        })
                        {<HolidayList dataRecieved={data}
                            //onRequest={()=>{this.props.request()}}
                        />}
                    }} 
                />

                 <SetRoleModel
                    //width = {3*Dimensions.get('window').width/4}
                    title={''}
                    userData = {this.state.persons}
                    roleData = {this.state.roles}
                    userType = {this.state.userType}
                    isOpen={this.state.roleMdl}
                    onClosed={() => { this.toggleModal(false) } }
                    onApply={(text) => { 
                        this.toggleModal(false)
                    }} 
                />

            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginTop: 56,
        backgroundColor: Colors.background
    },
    page: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    tabbar: {
        width:240,
        backgroundColor: Colors.background,
        elevation: 0,

    },
    indicatorStyle: {
        backgroundColor: '#408fff',
        width:75,
        position:'absolute'
        
    },
    indicator: {
        backgroundColor: 'white',
        position: 'absolute',
        left: 0,
        bottom: 0,
        right: 0,
        height: 2,
    },
    label: {
        color: '#222',
        fontWeight: '400',
        fontSize: 14,
        margin: 6
    },
    text: {
        color: 'white',
        fontSize: 16,
    },
    image: {
        height: 40,
        width: 40,
        borderRadius: 60
    },
    actionButtonIcon: {
        fontSize: 20,
        height: 22,
        color: 'white',
    },
    selectionBar: {
        height: 70, 
        paddingLeft: 10,
        borderStyle: 'solid', 
        borderBottomWidth: 3, 
        borderBottomColor: '#D6D6D6', 
        flexDirection: 'row', 
        justifyContent: 'flex-start', 
        alignItems: 'center' 
    }
});


/**
 * 
 * <ModalDropdown options={DEMO_OPTIONS_1}
                           onSelect={(idx, value) => this._dropdown_6_onSelect(idx, value)}>
                        <Image style={styles.dropdown_6_image}
                                source={dropdown_6_icon}
                        />
                    </ModalDropdown>
 */