import React, { Component } from 'react';
import { View, StyleSheet, Alert,Dimensions } from 'react-native';
import HolidayList from './HolidayList';
import { Colors, Metrics } from '../../../../Themes';
import ApproveModal from './Modals/ApproveModal';
import ActionButton from 'react-native-action-button';


export default class HolidayScreen extends Component {
    constructor(){
        super();
        this.state= {
            approveMdl:false,
            requestMdl:false,
            userdata: [],
            userType: 'manager'
        }
    }

    toggleModal(openStatus) {
        this.setState({
          approveMdl: openStatus,
          requestMdl: openStatus
        });
    }

    render(){
        //console.log(this.props.personData)
        return (
            <View style ={styles.container}>
                <HolidayList
                    personData= {this.props.personData}
                    modalState={(state, profileImage, name, start, end, leaveid)=>{
                        let data = [profileImage, name, start, end, leaveid]
                        this.setState({
                            approveMdl: state,
                            userdata: data
                        })
                    }}
                    editModel = {(leaveid, img, name, start)=>{

                    }}
                    dataRecieved={this.state.requestData}
                    requestApproved = {this.state.approved}
                    //onRequest={()=>{this.props.request()}}
                />

                <ActionButton style={styles.container} buttonColor="#408fff" offsetY={0} onPress={()=>{this.setState({requestMdl: true})}}>
                </ActionButton>

                <ApproveModal
                    //width = {3*Dimensions.get('window').width/4}
                    modelType={'Approve'}
                    title={''}
                    userData = {this.state.userdata}
                    isOpen={this.state.approveMdl}
                    onClosed={() => { this.toggleModal(false) } }
                    onApply={(approvedata) => {
                        this.setState({
                            approved: approvedata,
                            approveMdl: false
                        }) 
                        this.toggleModal(false)
                    }} 
                
                />

                <ApproveModal
                    //width = {3*Dimensions.get('window').width/4}
                    modelType={'Request'}
                    title={''}
                    userData = {this.props.personData}
                    roleData = {this.props.roleData}
                    userType = {this.state.userType}
                    isOpen={this.state.requestMdl}
                    onClosed={() => { this.toggleModal(false) } }
                    onApply={(data) => { 
                        this.setState({
                            requestData: data,
                            requestMdl: false
                        })
                        //this.toggleModal(false)
                    }} 
                />


            </View>
        );
    }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop:57,
    backgroundColor: Colors.background
  },
});
