// @flow
import React, { Component } from 'react';
import { Colors, Metrics } from '../../../../Themes'
import {
  StyleSheet,
  ListView,
  Text,
  ScrollView,
  View,
  Dimensions,
  Alert
} from 'react-native';
import HolidayRowLand from './HolidayRowLand';
import HolidayRowPort from './HolidayRowPort';
import Icon from 'react-native-vector-icons/Ionicons';
import { Actions as NavigationActions } from 'react-native-router-flux'
import {api} from '../../../../Lib/helpers';

//const getUsers = () => api.get('user');

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#eeeeee',
    paddingHorizontal: 5
  },
  separator: {
    flex: 1,
    height: StyleSheet.hairlineWidth,
    backgroundColor: '#8E8E8E',
  },
});

export default class HolidayList extends Component {
  constructor() {
    super();

    const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
    this.state = {
      dataSource: ds.cloneWithRows([]),
    };

  }

  componentDidMount(){
   api().get('calendar/leaves').then((response) => {     
      let persons = this.props.personData
      
      let leaveList = response.data.reduce((output, row)=>{
        
        let personRowId = persons.findIndex((item)=>{
          return item.id == row.userId
        })
       
        let obj = {
          name: persons[personRowId].name, 
          profileImg: persons[personRowId].image
        }
        //console.log(Object.assign(row, obj))
        output.push(Object.assign(row, obj))
        return output
      }, [])
      .sort((row1,row2)=>{
        
        if(row1.status == 'pending' || (row1.status == 'rejected' && row2.status == 'approved' )){
          return -1
        }else if(row1.status == 'rejected' || row1.status == 'approved' ){         
          return 1
        }else{
          return 0
        }

      })
       //console.log(leaveList)
     

      const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
      this.setState({
        leaveList : response.data,
        // newUserTemplate:{
        //     firstName: '',
        //     surName: '',
        //     username: '',
        //     siteEmail: 'test@bback.com',
        //     roles: [
               
        //     ],
        //     active: false,
        //     temporaryStaff: false,
        //     contractStartDate: '',
        //     contractEndDate: '',
        //     qualifications: [],
        //     skill: [],
        //     profileImg: 'https://www.outsystems.com/PortalTheme/img/UserImage.png?24860',
        //     password: '',
        // },
        dataSource: ds.cloneWithRows(leaveList),
      });
    });
  }

  getRowView(){
        let modelWidth = Dimensions.get('window').width

        return Dimensions.get('window').width>500 ? ((rowData) => {

          return (
            <HolidayRowLand 
              onPress={(isOpen, start, end, leaveid)=>{this.props.modalState(isOpen, rowData.profileImg, rowData.name, start, end, leaveid)}} 
              data={rowData} 
              onEdit={(leaveid, start)=>{
                this.props.editModel(isOpen, leaveid, rowData.profileImg, rowData.name, start)
              }}
            />) } )

        :((rowData) => { 
          return (
            <HolidayRowPort
              onPress={(isOpen)=>{this.props.modalState(isOpen, rowData.profileImg, rowData.name)}} 
              data={rowData} />) } )
  }


  componentWillReceiveProps(newProps){
    this.componentDidMount()
    if(newProps.dataRecieved != this.props.dataRecieved){
        //console.log(newProps.dataRecieved) 
        let leaveData = newProps.dataRecieved
        api().post('calendar/leave', {
            userId: leaveData[0],
            startDate: leaveData[1],
            endDate: leaveData[2],
            reason: leaveData[3]
        }).then((response) => {
            //console.log(response.data)
                        Alert.alert(
                            'Success',
                            'User Created Successfully',
                            [
                                {
                                    text: 'OK', onPress: () => {
                                        NavigationActions.pop();
                                    }
                                },
                            ]
                        );
            this.forceUpdate()
        });
    }else if(newProps.requestApproved != this.props.requestApproved){
        let leavePerson = this.state.leaveList.find((data)=>{
          if(data.leaveId == newProps.requestApproved[0]){
              data.status = newProps.requestApproved[2]
              data.reason = newProps.requestApproved[1]
              return data
          }
        })
        api().put('calendar/leave/' + newProps.requestApproved[0],leavePerson)
        .then((response) => {
                            //console.log(response.data)
                            Alert.alert(
                                'Success',
                                'User Updated Successfully',
                                [
                                    {
                                        text: 'OK', onPress: () => {
                                            //NavigationActions.pop();
                                        }
                                    },
                                ]
                            );
                        });

      console.log(newProps.requestApproved)
      
    }

  }

  approveRequest(){

  }
  
  // offset = 0
  // scrollDirection(){
  //   (event)=>{
  //       let currentOffset = event.nativeEvent.contentOffset.y;
  //       let direction = currentOffset > this.offset ? 'down' : 'up';
  //      // this.offset = currentOffset;
  //       console.log(direction);
  //   }
      
  // }

  render() {
    
    return (
      <View style={styles.container}>
        <ScrollView style={styles.container}>
          <ListView style={{ backgroundColor: 'transparent' }}
            dataSource={this.state.dataSource}
            enableEmptySections={true}
            renderRow={this.getRowView()}
            >
          </ListView>
        </ScrollView>
        
      </View>
      
    );
  }
}