import React, { Component } from 'react';
import { View, Text, StyleSheet, Image, TouchableHighlight } from 'react-native';
import { Avatar, Drawer, Divider, PRIMARY_COLORS, TYPO, Ripple } from 'react-native-material-design';
import { Actions as NavigationActions } from 'react-native-router-flux';
import ActionButton from 'react-native-action-button';
import Icon from 'react-native-vector-icons/MaterialIcons';
import ModalDropdown from 'react-native-modal-dropdown';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        margin: 5,
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: 'white',
        elevation: 2
    },
    text: {
        marginLeft: 12,
        marginTop: 0,
        fontSize: 16,
        
    },
    photo: {
        height: 60,
        width: 60,
        borderRadius: 60,
    },
    rippleContainer: {
        flex: 1,
        flexDirection: 'row',
        height: 80,
        padding: 12,
    },
});

const months = ["Jan", "Feb", "Mar", "Apr", "May", "June", "July", "Aug", "Sep", "Oct", "Nov", "Dec"];

export default class HolidayRowLand extends React.Component {
    constructor(props) {
        super(props)
    }

    getStatus(){
        let status = ['pending', 'approved', 'rejected']

        let num = status.indexOf(this.props.data.status)
        //console.log(this.props.data.status)
        let color = num == 0 ? '#ffbf2e': num == 1 ? '#4fab6e': '#fd7c7c'
        return (<Text style={[styles.text,{color: color, paddingRight: 10, fontWeight: '300'}]}>{status[num].toUpperCase()}</Text>)
    }

    getDays(){
        let milisec = new Date(this.props.data.endDate) - new Date(this.props.data.startDate) 
        return milisec/(1000*60*60*24)
    }

    getDayMonth(fulldate){
        let date = new Date(fulldate)
        return (date.getDate() +' '+ months[date.getMonth()])
    }
    setStatus(status){
        //Status to be set
    }

    dropDown(rowData) {
        //console.log(rowData)
        //let icon = rowData.image
        return (
            <TouchableHighlight style={{ flex: 1}} >
                <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center', padding: 10 }}>
                    <Text style={{fontSize: 16 }}>{rowData}</Text>
                </View>
            </TouchableHighlight>
        );
    }

    render() {
        return (
            <View style={styles.container}>
                <Ripple text="" style={styles.rippleContainer} color='rgba(0,0,0,.5)' onPress={()=>{
                    this.props.onPress(true, this.props.data.startDate, this.props.data.endDate, this.props.data.leaveId, false)
                }}>
                    <Image source={{ uri: this.props.data.profileImg }} style={styles.photo} />
                    <View style={{ flex: 1, flexDirection: 'column',justifyContent: 'center' }}>
                        <Text style={[styles.text,{color: 'black'}]}>
                            {this.props.data.name}
                        </Text>
                         <View style={{flex: 3, flexDirection: 'row', justifyContent:'flex-start', alignItems:'center'}}>
                            <Text style={styles.text}>
                                {this.getDays()} Days
                            </Text>
                            <View style={{flexDirection:'row', paddingLeft: 30}}>
                                <Text style={styles.text}>{this.getDayMonth(this.props.data.startDate)}</Text>
                                <Text style={styles.text}>-</Text>
                                <Text style={styles.text}>{this.getDayMonth(this.props.data.endDate)}</Text>
                            </View>
                        </View>
                    </View>
                    
                    <View style={{flex: 1, flexDirection: 'row',justifyContent:'flex-end', paddingTop: 15, height: 80}}>
                        <Text style={[styles.text,{marginLeft: 20}]}>{this.props.data.reason}</Text>
                        <View style={{alignItems: 'flex-end', width:120}}>{this.getStatus()}</View>
                        <Ripple style={{height:25}} color='rgba(0,0,0,.5)' onPress={()=>{
                            this.props.onEdit(this.props.leaveId, this.props.data.startDate)    
                        }}>
                           
                        </Ripple>                  
                    </View>
                </Ripple>
            </View>
        );
    }
}


/**
 *  <ModalDropdown
                                        dropdownStyle={{height: 43}}
                                        options={['EDIT']}
                                        onSelect={(idx, value) => this.setStatus(value)}
                                        renderRow ={this.dropDown.bind(this)}>
                                        <Icon name="more-vert" size={20} color="#000000" />
                            </ModalDropdown>
 * 
 * 
 * 
 * 
 * <Ripple style={styles.rippleIconcontainer} color='rgba(0,0,0,.5)'>
                             <Image source={Images.ApproveIcon} style={[styles.photo]} />
                        </Ripple>
                         <Ripple style={styles.rippleIconcontainer} color='rgba(0,0,0,.5)'>
                             <Image source={Images.RejectIcon} style={[styles.photo]} />
                        </Ripple>

 <ActionButton icon ={approveIcon} offsetY={0} position= 'left' buttonColor="#4caf50" hideShadow={true} onPress={()=>{
                            this.props.onPress(true, 'approve')
                        }}>
                        </ActionButton>
                        <ActionButton icon ={rejectIcon} offsetY={0} position= 'right'buttonColor="#dc143c" hideShadow={true} onPress={()=>{
                            this.props.onPress(true, 'reject')
                        }}>
                        </ActionButton>

 */