import React, { Component } from 'react';
import { View, Text, StyleSheet, Image, TouchableHighlight } from 'react-native';
import { Avatar, Drawer, Divider, PRIMARY_COLORS, TYPO, Ripple } from 'react-native-material-design';
import { Actions as NavigationActions } from 'react-native-router-flux';
import ActionButton from 'react-native-action-button';
import Icon from 'react-native-vector-icons/MaterialIcons';
import ModalDropdown from 'react-native-modal-dropdown';


const styles = StyleSheet.create({
    container: {
        flex: 1,
        margin: 5,
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: 'white',
        elevation: 2
    },
    text: {
        marginLeft: 12,
        marginTop: 0,
        fontSize: 16,
    },
    photo: {
        height: 60,
        width: 60,
        borderRadius: 60,
    },
    rippleContainer: {
        flex: 1,
        flexDirection: 'row',
        height: 80,
        padding: 12,
    },
});

export default class HolidayRowPort extends React.Component {
    constructor(props) {
        super(props)
    }

     getStatus(){
        let status = ['PENDING', 'APPROVED', 'DECLINED']

        let num = Math.floor(Math.random()*3)
        let color = num == 0 ? '#ffbf2e': num == 1 ? '#4fab6e': '#fd7c7c'
        return (<Text style={[{color: color, paddingTop:20}]}>{status[num]}</Text>)
    }

    setStatus(status){
        //Status to be set
    }

    dropDown(rowData) {
        console.log(rowData)
        //let icon = rowData.image
        return (
            <TouchableHighlight style={{ flex: 1}} >
                <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center', padding: 10 }}>
                    <Text style={{fontSize: 16 }}>{rowData}</Text>
                </View>
            </TouchableHighlight>
        );
    }

    render() {
        return (
            <View style={styles.container}>
                <Ripple text="" style={styles.rippleContainer} color='rgba(0,0,0,.5)' onPress={()=>{this.props.onPress(true)}}>
                    <Image source={{ uri: this.props.data.profileImg }} style={styles.photo} />
                    <View style={{ flex: 2, flexDirection: 'column',justifyContent: 'center', marginVertical:-5}}>
                        <Text style={[styles.text,{color: 'black'}]}>
                            {this.props.data.firstName} {this.props.data.surName}
                        </Text>
                        <View style={{flex: 1, flexDirection: 'row', justifyContent:'flex-start', alignItems:'center', paddingLeft: 11, margin: 2}}>
                            <Text >3 days   </Text>
                            <View style={{flexDirection:'row',justifyContent:'flex-start', alignItems:'center'}}>
                                <Text >1 Jan</Text><Text>-</Text><Text>3 Jan</Text>
                            </View>
                        </View>
                         <Text style={{flex: 1, flexDirection: 'row', justifyContent:'flex-start', alignItems:'center', paddingLeft: 12}} >Comment display here</Text>
                        
                    </View>
                    <View style={{flex: 1, flexDirection: 'row',alignItems:'flex-start', justifyContent: 'flex-end', paddingBottom: 20, height: 80, marginHorizontal:-13}}>
                        {this.getStatus()}
                        <Ripple style={{height:25}} color='rgba(0,0,0,.5)' onPress={()=>{this.props.onPress(true)}}>
                            <ModalDropdown
                                        dropdownStyle={{height: 43}}
                                        options={['EDIT']}
                                        onSelect={(idx, value) => this.setStatus(value)}
                                        renderRow ={this.dropDown.bind(this)}>
                                        <Icon name="more-vert" size={20} color="#000000" />
                            </ModalDropdown>
                        </Ripple>  
                        
                    </View>
                </Ripple>
            </View>
        );
    }
}


/**
 * 
 * <Ripple style={styles.rippleIconcontainer} color='rgba(0,0,0,.5)'>
                             <Image source={Images.ApproveIcon} style={[styles.photo]} />
                        </Ripple>
                         <Ripple style={styles.rippleIconcontainer} color='rgba(0,0,0,.5)'>
                             <Image source={Images.RejectIcon} style={[styles.photo]} />
                        </Ripple>
 */