// @flow
import React, { Component } from 'react';
import { Colors, Metrics } from '../../../../../Themes'
import {
    StyleSheet,
    ListView,
    Text,
    ScrollView,
    View,
    Dimensions,
    TextInput,
    Image,
    TouchableHighlight,
    DatePickerAndroid,
    TouchableOpacity
} from 'react-native';
import { Avatar, Checkbox, Drawer, Divider, PRIMARY_COLORS, TYPO, Ripple } from 'react-native-material-design';
import ActionButton from 'react-native-action-button';
import Icon from 'react-native-vector-icons/MaterialIcons';
import Modal from 'react-native-modalbox';
import TextField from 'react-native-md-textinput';
import Button from '../../../../../Components/Button';
import ModalDropdown from 'react-native-modal-dropdown';
import dismissKeyboard from 'react-native-dismiss-keyboard';


const approveIcon = (<Icon name="done" size={30} color="#ffffff" />)

const months = ["Jan", "Feb", "Mar", "Apr", "May", "June", "July", "Aug", "Sep", "Oct", "Nov", "Dec"];

export default class ApproveModal extends Component {

    constructor() {
        super();
        this.state = {
            initial: true,
            checkbox: {},
            selectionList: [],
            //width: modelWidth
        };
    }

    
    getModelWidth(){
        let modelWidth = Dimensions.get('window').width

        return Dimensions.get('window').width>500 ? modelWidth/3: 7* modelWidth/8
    }

    componentWillMount() {
        this.state = { 
               
                textField: ''

        }
    }
    componentDidMount(){
       
    }

    updateState() {
        this.setState({ 
            ...this.state,
        });
}

componentWillReceiveProps(nextProps) {
     if(nextProps.modelType =='Request'){
            let date = new Date();
        

            this.setState({
                personName: nextProps.userData[1].name,
                personImage: nextProps.userData[1].image,
                fromDate : date.getFullYear() +"/"+( date.getMonth()+1) +"/"+ date.getDate(),
                toDate : date.getFullYear() +"/"+( date.getMonth()+1) +"/"+ date.getDate(),
                personID : nextProps.userData[1].id
            })
    }
}

personRow(rowData) {
        return (
            <TouchableHighlight style={{ flex: 1, width: 200 }} >
                <View style={{ flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center', padding: 10 }}>
                    <Image style={styles.image} source={{ uri: rowData[1] }} />
                    <Text style={{ paddingLeft: 10, color: 'black', fontSize: 16 }}>{rowData[0]}</Text>
                </View>
            </TouchableHighlight>
        );
}

showPicker = async (options, type) => {
        try {
            var newState = {};
            const {action, year, month, day} = await DatePickerAndroid.open(options);
            if (action === DatePickerAndroid.dismissedAction) {
                newState[stateKey + 'Text'] = 'dismissed';
            } else {
                var newState = {...this.state };
                var date = new Date(year, month, day);
                let ISOdate = date.getFullYear() +"/"+( date.getMonth()+1) +"/"+ (date.getDate());
                //console.log(date.getDate())
                (type == 'from') ? newState.fromDate = ISOdate : newState.toDate = ISOdate;
                this.setState(newState);

            }
            this.setState(newState);
        } catch ({code, message}) {
            console.warn(`Error in example '${stateKey}': `, message);
        }
};

getDayMonth(fulldate){
        let date = new Date(fulldate)
        return (date.getDate() +' '+ months[date.getMonth()])
}



render() {

    if(this.props.modelType =='Approve'){ 
        return (
                <Modal onClosed={() => { this.props.onClosed(), this.state.textField = '' } } position="top" isOpen={this.props.isOpen} style={{ zIndex: 1000, marginTop: 80, padding: 20, width: this.getModelWidth() , height: 300, backgroundColor: '#ffffff' }}>
                    <View style= {{flexDirection: 'row', justifyContent:'center', alignItems: 'center' ,backgroundColor:'dodgerblue', height: 60, margin: -20, marginBottom: 10}}>
                    <View style = {{flex: 1,justifyContent: 'flex-start', alignItems:'flex-start', paddingLeft: 15}}>
                        <Text style = {{ fontSize: 18, color: 'white'}}>Leave Approval</Text>
                    </View>
                    <View style = {{flex: 1,justifyContent: 'flex-end', alignItems:'flex-end', paddingRight:15}}>
                        <Ripple color='rgba(255,255,255,.5)' onPress={()=>{this.props.onClosed()}}>
                            <Icon name="close" size={25} color="#ffffff" />
                        </Ripple>
                    </View>    
                    </View>
                    <View>
                        <View style = {{flexDirection: 'row', alignItems: 'center', marginBottom:10, paddingTop: 10}}> 
                            <Image source={{ uri: this.props.userData[0] }} style={styles.photo} />
                            <View style = {{flexDirection: 'column'}}> 
                                <Text style={[styles.text,{color: 'black'}]}>{this.props.userData[1]}</Text>
                                <View style={{flexDirection:'row'}}>
                                    <Text style={styles.text}>{this.getDayMonth(this.props.userData[2])}</Text>
                                    <Text style={styles.text}>-</Text>
                                    <Text style={styles.text}>{this.getDayMonth(this.props.userData[3])}</Text>
                                </View>
                            </View>                  
                        </View>
                        <View style={{justifyContent:'center',marginHorizontal: 10}}>
                            <TextField
                                label={'Reply comment'}
                                highlightColor={'dodgerblue'}
                                value={this.state.textField}
                                dense={true}
                                onChangeText={(text) => this.setState({ ...this.state, textField: text })}
                            />
                        </View>
                        <View style={{ marginTop: 20, justifyContent:'center', flexDirection: 'row'}}>
                            <Ripple style={[styles.button,{backgroundColor: 'red',}]} color='rgba(255,255,255,.5)' onPress={()=>{
                                let rejectData = [ this.props.userData[4], this.state.textField, 'rejected']
                                this.props.onApply(rejectData)
                            }}>
                                <Icon name="close" size={25} color="#ffffff" />
                                <Text style={[styles.text,{color: 'white'}]}>  REJECT</Text>
                            </Ripple>

                            <Ripple style={[styles.button, {backgroundColor: '#4fab6e',}]} color='rgba(255,255,255,.5)' onPress={()=>{
                                    //console.log(this.props.userData)
                                    let approveData = [ this.props.userData[4], this.state.textField, 'approved']
                                    this.props.onApply(approveData)
                            }}>
                                <Icon name="done" size={25} color="#ffffff" />
                                <Text style={[styles.text,{color: 'white'}]}>APPROVE</Text>
                            </Ripple>                       
                            
                        </View>
                    </View>
                </Modal>
            );

        }else if(this.props.modelType =='Request'){

            return (
                <Modal onClosed={() => { this.props.onClosed(), this.state.textField = '' } } position="top" isOpen={this.props.isOpen} style={{ zIndex: 1000, marginTop: 80, padding: 20, width: this.getModelWidth() , height: 300, backgroundColor: '#ffffff' }}>
                    <View style= {{flexDirection: 'row', justifyContent:'center', alignItems: 'center' ,backgroundColor:'dodgerblue', height: 60, margin: -20, marginBottom: 10}}>
                    <View style = {{flex: 1,justifyContent: 'flex-start', alignItems:'flex-start', paddingLeft: 15}}>
                        <Text style = {{ fontSize: 18, color: 'white'}}>Leave Request</Text>
                    </View>
                    <View style = {{flex: 1,justifyContent: 'flex-end', alignItems:'flex-end', paddingRight:15}}>
                        <Ripple color='rgba(255,255,255,.5)' onPress={()=>{this.props.onClosed()}}>
                            <Icon name="close" size={25} color="#ffffff" />
                        </Ripple>
                    </View>    
                    </View>
                    <View>
                        <View style = {{flexDirection: 'row', alignItems: 'center', marginBottom:10, paddingTop: 10}}> 
                            
                            <View style = {{flexDirection: 'column'}}> 

                                 <View style={{flexDirection:'row'}}>
                                   <ModalDropdown
                                        style={[{ width: 200, height: 50, flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'flex-start' }]}
                                        options={
                                                this.props.userData.reduce((details,person) => {
                                                    let det = null
                                                    if(person.id != 0){ 
                                                        det = []                                                   
                                                        det.push(person.name)
                                                        det.push(person.image)
                                                        det.push(person.id)
                                                        details.push(det)
                                                    }
                                                    
                                                    return details
                                                },[])                                          
                                        }
                                        onSelect={(idx, value) =>{ 
                                            //console.log(value)
                                            this.setState({ personName: value[0], personImage: value[1] , personID: value[2]})
                                        }}
                                        renderRow={this.personRow.bind(this)}
                                        >
                                        <View style={{ flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center', padding: 10 }}>
                                            <Image style={[styles.image]} source={{ uri: this.state.personImage }} />
                                            <Text style={{ paddingLeft: 10, paddingRight: 10, color: 'black', fontSize: 16 }}>{this.state.personName}</Text>
                                            <Icon name="arrow-drop-down" size={20} color="#000000" />
                                        </View>
                                    </ModalDropdown>
                                </View>
                                <View style={{flexDirection:'row',  height: 40, width: 300}}>
                                    <TouchableOpacity style={{ width: 100, paddingTop: -15, paddingLeft: 10}} onPressIn={() => { dismissKeyboard(); } } onPressOut={() => { dismissKeyboard(); } } onPress={() => { dismissKeyboard(); } }>
                                        <TextField
                                            label={'from'}
                                            highlightColor={'#00BCD4'}
                                            value={this.state.fromDate}
                                            onFocus={() => {
                                                dismissKeyboard();
                                                this.showPicker({
                                                    date: new Date(this.state.fromDate)
                                                }, 'from');
                                            } }
                                            onChangeText={(text) => {
                                                var newState = {...this.state};
                                                    newState.userData.contractStartDate=text;
                                                    this.setState(newState);
                                                }
                                                }
                                            dense={true}
                                        />
                                    </TouchableOpacity>
                                    <TouchableOpacity style={{ width: 100, paddingTop: -15, paddingLeft: 30}} onPressIn={() => { dismissKeyboard(); } } onPressOut={() => { dismissKeyboard(); } } onPress={() => { dismissKeyboard(); } }>
                                        <TextField
                                            label={'To'}
                                            highlightColor={'#00BCD4'}
                                            value={this.state.toDate}
                                            onFocus={() => {
                                                dismissKeyboard();
                                                this.showPicker({
                                                    date: new Date()
                                                }, 'to');
                                            } }
                                            onChangeText={(text) => {
                                                var newState = {...this.state};
                                                    newState.userData.contractStartDate=text;
                                                    this.setState(newState);
                                                }
                                                }
                                            dense={true}
                                        />
                                    </TouchableOpacity>
                                </View>

                            </View>                  
                        </View>
                        <View style={{justifyContent:'center',marginHorizontal: 10, paddingTop: -25}}>
                            <TextField
                                label={'Reason'}
                                highlightColor={'dodgerblue'}
                                value={this.state.textField}
                                dense={true}
                                onChangeText={(text) => this.setState({ ...this.state, textField: text })}
                            />
                        </View>
                        <View style={{ marginTop: 10, justifyContent:'flex-end', flexDirection: 'row'}}>
                            <Ripple style={[styles.button, {backgroundColor: '#4fab6e',}]} color='rgba(255,255,255,.5)' onPress={()=>{
                                let requestData = [this.state.personID, this.state.fromDate, this.state.toDate, this.state.textField]
                                //console.log(requestData)
                                this.props.onApply(requestData)
                            }}>
                                <Icon name="done" size={25} color="#ffffff" />
                                <Text style={[styles.text,{color: 'white'}]}>REQUEST</Text>
                            </Ripple>                       
                            
                        </View>
                    </View>
                </Modal>);
        }

    }

}

const styles = StyleSheet.create({
     photo: {
        height: 60,
        width: 60,
        borderRadius: 60,
    },
    text: {
        marginLeft: 12,
        marginTop: 0,
        fontSize: 16,
        
    },
    button: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems:'center',
        padding:10,
        marginHorizontal: 20,
        elevation: 2
    },
    image: {
        height: 40,
        width: 40,
        borderRadius: 60
    }
})