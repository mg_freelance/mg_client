// @flow
import React, { Component } from 'react';
import { Colors, Metrics } from '../../../../Themes'
import {
    StyleSheet,
    ListView,
    Text,
    ScrollView,
    View,
    Dimensions,
    TextInput,
    Image,
    TouchableHighlight,
    DatePickerAndroid,
    TouchableOpacity
} from 'react-native';
import { Avatar, Checkbox, Drawer, Divider, PRIMARY_COLORS, TYPO, Ripple } from 'react-native-material-design';
import ActionButton from 'react-native-action-button';
import Icon from 'react-native-vector-icons/MaterialIcons';
import Modal from 'react-native-modalbox';
import TextField from 'react-native-md-textinput';
import ModalDropdown from 'react-native-modal-dropdown';
import dismissKeyboard from 'react-native-dismiss-keyboard';


const approveIcon = (<Icon name="done" size={30} color="#ffffff" />)

export default class SetRoleModel extends Component {

    constructor() {
        super();
        this.state = {
            
            //width: modelWidth
        };
    }

    
    getModelWidth(){
        let modelWidth = Dimensions.get('window').width

        return Dimensions.get('window').width>500 ? modelWidth/3: 7* modelWidth/8
    }

    componentWillMount() {
        this.state = { 
               
                textField: ''

        }
    }
    componentDidMount(){
       
    }

    updateState() {
        this.setState({ 
            ...this.state,
        });
}

componentWillReceiveProps(nextProps) {
            let date = new Date();

            this.setState({
                personName: nextProps.userData[1].name,
                personImage: nextProps.userData[1].image,
                selectedRole: nextProps.roleData[1],
                fromDate : date.getUTCFullYear() +"/"+( date.getUTCMonth()+1) +"/"+ date.getUTCDate(),
                toDate : date.getUTCFullYear() +"/"+( date.getUTCMonth()+1) +"/"+ date.getUTCDate()
            })
}

personRow(rowData) {
        return (
            <TouchableHighlight style={{ flex: 1, width: this.getModelWidth()/3 + 30 }} >
                <View style={{ flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center', padding: 5 }}>
                    <Image style={styles.image} source={{ uri: rowData[1] }} />
                    <Text style={{ paddingLeft: 10, paddingRight: 20, color: 'black', fontSize: 14 }}>{rowData[0]}</Text>
                </View>
            </TouchableHighlight>
        );
}

roleRow(roleData) {
        //console.log(rowData[1])
        //let icon = rowData.image
        return (
            <TouchableHighlight style={{ flex: 1, width: this.getModelWidth()/3 + 25 }} >
                <View style={{ flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center', padding: 5 }}>
                    <Icon name="build" style={[styles.image,{backgroundColor:'#01579b',padding: 10}]} color= 'white' size={15}/>
                    <Text style={{ paddingLeft: 5,paddingRight:20, color: 'black', fontSize: 14 }}>{roleData}</Text>
                </View>
            </TouchableHighlight>
        );
    }

showPicker = async (options, type) => {
        try {
            var newState = {};
            const {action, year, month, day} = await DatePickerAndroid.open(options);
            if (action === DatePickerAndroid.dismissedAction) {
                newState[stateKey + 'Text'] = 'dismissed';
            } else {
                var newState = {...this.state };
                var date = new Date(year, month, day);
                let ISOdate = date.getUTCFullYear() +"/"+( date.getUTCMonth()+1) +"/"+ (date.getUTCDate()+1);
                (type == 'from') ? newState.fromDate = ISOdate : newState.toDate = ISOdate;
                this.setState(newState);

            }
            this.setState(newState);
        } catch ({code, message}) {
            console.warn(`Error in example '${stateKey}': `, message);
        }
};

render() {   

            return (
                <Modal onClosed={() => { this.props.onClosed(), this.state.textField = '' } } position="top" isOpen={this.props.isOpen} style={{ zIndex: 1000, marginTop: 80, padding: 20, width: this.getModelWidth() , height: 300, backgroundColor: '#ffffff' }}>
                    <View style= {{flexDirection: 'row', justifyContent:'center', alignItems: 'center' ,backgroundColor:'dodgerblue', height: 60, margin: -20, marginBottom: 10}}>
                    <View style = {{flex: 1,justifyContent: 'flex-start', alignItems:'flex-start', paddingLeft: 15}}>
                        <Text style = {{ fontSize: 18, color: 'white'}}>Assign Role</Text>
                    </View>
                    <View style = {{flex: 1,justifyContent: 'flex-end', alignItems:'flex-end', paddingRight:15}}>
                        <Ripple color='rgba(255,255,255,.5)' onPress={()=>{this.props.onClosed()}}>
                            <Icon name="close" size={25} color="#ffffff" />
                        </Ripple>
                    </View>    
                    </View>
                    <View>
                        <View style = {{flexDirection: 'row', alignItems: 'center', marginBottom:10, paddingTop: 10}}> 
                            
                            <View style = {{flexDirection: 'column'}}> 

                                 <View style={{flexDirection:'row',justifyContent: 'space-between', width: this.getModelWidth()-40}}>
                                   <ModalDropdown
                                        style={[{ width: this.getModelWidth()/3 + 30, height: 50, flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'flex-start' }]}
                                        options={
                                              this.props.userData.reduce((details, person) => {
                                                    
                                                    if(person.name != 'All Users'){
                                                        details.push([person.name, person.image])
                                                    }
                                                    return details
                                                },[])                                          
                                        }
                                        onSelect={(idx, value) =>{ 
                                            this.setState({ personName: value[0], personImage: value[1] })
                                        }}
                                        renderRow={this.personRow.bind(this)}
                                        >
                                        <View style={{ flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center', padding: 5 }}>
                                            <Image style={[styles.image]} source={{ uri: this.state.personImage }} />
                                            <Text style={{ paddingLeft: 10, paddingRight: 20, color: 'black', fontSize: 14 }}>{this.state.personName}</Text>
                                            <Icon name="arrow-drop-down" size={20} color="#000000" />
                                        </View>
                                    </ModalDropdown>

                                    <ModalDropdown
                                        style={[{ width: this.getModelWidth()/3 + 40, height: 50, flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'flex-start' }]}
                                        options={
                                              this.props.roleData.reduce((roleName, role)=>{
                                                  if(role != 'All roles'){
                                                      roleName.push(role)
                                                  }
                                                  return roleName
                                              }, [])                                          
                                        }
                                        onSelect={(idx, value) =>{ 
                                            this.setState({ selectedRole : value})
                                        }}
                                        renderRow={this.roleRow.bind(this)}
                                        >
                                         <View style={{ flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center', padding: 15 }}>
                                            <Icon name="build" style={[styles.image,{backgroundColor:'#01579b',padding: 10}]} color= 'white' size={15}/>
                                            <Text style={{ paddingLeft: 5, paddingRight: 10, color: 'black', fontSize: 14 }}>{this.state.selectedRole}</Text>
                                        </View>
                                    </ModalDropdown>
                                     
                                </View>
                                <View style={{flexDirection:'row', justifyContent: 'space-between', height: 70, width: this.getModelWidth()-40}}>
                                    <TouchableOpacity style={{ width: this.getModelWidth()/3, padding:10}} onPressIn={() => { dismissKeyboard(); } } onPressOut={() => { dismissKeyboard(); } } onPress={() => { dismissKeyboard(); } }>
                                        <TextField
                                            label={'from'}
                                            highlightColor={'#00BCD4'}
                                            value={this.state.fromDate}
                                            onFocus={() => {
                                                dismissKeyboard();
                                                this.showPicker({
                                                    date: new Date(this.state.fromDate)
                                                }, 'from');
                                            } }
                                            onChangeText={(text) => {
                                                var newState = {...this.state};
                                                    newState.userData.contractStartDate=text;
                                                    this.setState(newState);
                                                }
                                                }
                                            dense={true}
                                        />
                                    </TouchableOpacity>
                                    <TouchableOpacity style={{ width: this.getModelWidth()/3, padding: 10}} onPressIn={() => { dismissKeyboard(); } } onPressOut={() => { dismissKeyboard(); } } onPress={() => { dismissKeyboard(); } }>
                                        <TextField
                                            label={'To'}
                                            highlightColor={'#00BCD4'}
                                            value={this.state.toDate}
                                            onFocus={() => {
                                                dismissKeyboard();
                                                this.showPicker({
                                                    date: new Date()
                                                }, 'to');
                                            } }
                                            onChangeText={(text) => {
                                                var newState = {...this.state};
                                                    newState.userData.contractStartDate=text;
                                                    this.setState(newState);
                                                }
                                                }
                                            dense={true}
                                        />
                                    </TouchableOpacity>
                                </View>

                            </View>                  
                        </View>
                        
                        <View style={{ marginTop: 10, justifyContent:'flex-end', flexDirection: 'row'}}>
                            <Ripple style={[styles.button, {backgroundColor: '#4fab6e',}]} color='rgba(255,255,255,.5)' onPress={()=>{this.props.onClosed()}}>
                                <Icon name="done" size={25} color="#ffffff" />
                                <Text style={[styles.text,{color: 'white'}]}>ASSIGN</Text>
                            </Ripple>                       
                            
                        </View>
                    </View>
                </Modal>
        );       

    }

}

const styles = StyleSheet.create({
     
    text: {
        marginLeft: 12,
        marginTop: 0,
        fontSize: 16,
        
    },
    button: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems:'center',
        padding:10,
        marginHorizontal: 20,
        elevation: 2
    },
    image: {
        height: 35,
        width: 35,
        borderRadius: 60
    }
})


