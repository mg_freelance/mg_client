import React, { Component } from 'react';
import { View, StyleSheet, Alert, Text } from 'react-native';
import { Actions as NavigationActions } from 'react-native-router-flux';
import ModalDropdown from 'react-native-modal-dropdown';
import Icon from 'react-native-vector-icons/FontAwesome';
import { Ripple } from 'react-native-material-design';

export default class DateSelector extends Component {
    constructor(){
        super()
        this.state = {
            count : 0,
            date: new Date()
        }
        
    }

    changeDays(isAdding){
        let currentDate = this.state.date
        if(isAdding){
            switch(this.props.selected){
                case 0:
                    return currentDate.setDate(currentDate.getDate() + 1);
                
                case 1: 
                    return currentDate.setDate(currentDate.getDate() + 7);
                
                case 2:
                    return currentDate.setMonth(currentDate.getMonth() + 1);
                
                default:
                    return currentDate;
            }
        }else{
            switch(this.props.selected){
                case 0:
                    return currentDate.setDate(currentDate.getDate() - 1);
                
                case 1: 
                    return currentDate.setDate(currentDate.getDate() - 7);
                
                case 2:
                    return currentDate.setMonth(currentDate.getMonth() - 1);
                
                default:
                    return currentDate;
            }
        }
    }

    setDay(day){
        let days = ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"];
        let month = ["Jan", "Feb", "Mar", "Apr", "May", "June", "July", "Aug", "Sep", "Oct", "Nov", "Dec"]
        let weekNumber = [ '1', '2', '3', '4', '5', '6']
        let weekMonday = (day.getDate()-(day.getDay()==0?6:day.getDay()-1)) + 7

        switch(this.props.selected){
            case 0:
                return (days[day.getDay()]+', '+ day.getUTCDate() +"/"+( day.getUTCMonth()+1) +"/"+ day.getUTCFullYear());
            
            case 1: 
                return ('Week ' + weekNumber[ 0| weekMonday/7 ] +', '+ month[day.getMonth()] +", "+ day.getUTCFullYear());
            
            case 2:
                return (month[day.getMonth()] +", "+ day.getUTCFullYear() );
            
            default:
                return '';
        }
    }
    render() {
        return (
            <View style={styles.calendar_header_item}>
                <Ripple style={{ width: 20, height: 30, padding: 4 }} onPress = {()=>{this.setState({
                        date : new Date(this.changeDays(false))
                })}} >
                    <Icon name="chevron-left" size={16} color="#333" />
                </Ripple>
                <Text style={styles.calendar_header_text}>{this.setDay(this.state.date)}</Text>
                <Ripple style={{ width: 20, height: 30, padding: 4 }} onPress= {()=>{
                        this.setState({
                        date : new Date(this.changeDays(true))
                    })}} >
                    <Icon name="chevron-right" size={16} color="#333" />
                </Ripple>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    calendar_header_item: {
        //width: 150,
        flex:1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingTop: 5,
        paddingRight: 5,
        paddingLeft: 0
    },
    calendar_header_text: {
        fontWeight: 'bold',
        fontSize: 14,
        paddingBottom: 5,
        textAlign: 'center'
    },
});