import React, { Component } from 'react';
import { Colors, Metrics } from '../../../../../Themes'
import {
  Alert,
  StyleSheet,
  ListView,
  Text,
  ScrollView,
  View,
  Dimensions,
  TouchableHighlight,
  TimePickerAndroid
} from 'react-native';
//import PatternRow from './PatternRow';
import Icon from 'react-native-vector-icons/MaterialIcons';
import ModalDropdown from 'react-native-modal-dropdown';
import {Ripple} from 'react-native-material-design';
import {api} from '../../../../../Lib/helpers';


export default class PatternList extends React.Component{

    constructor() {
        super();

        const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
        this.state = {
           rowData : [
                        {day: 'MONDAY', data:{}},
                        {day: 'TUESDAY', data:{}},
                        {day: 'WEDNESDAY', data:{}},
                        {day: 'THURSDAY', data:{}},
                        {day: 'FRIDAY', data:{}},
                        {day: 'SATURDAY', data:{}},
                        {day: 'SUNDAY', data:{}},
            ],
            dataSource: ds.cloneWithRows([]),
            roleData:[ ],

        };

    }

    componentDidMount(){
        const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
        api().get('role').then((roleResponse)=>{
                let roleData = roleResponse.data.map((role)=>{
                  return role.roleName
                })

                this.setState({
                    roleData: roleData,
                    dataSource: ds.cloneWithRows(this.state.rowData)
                })
        })
        
    }

    dropDown(rowData) {
        return (
            <TouchableHighlight style={{ flex: 1}} >
                <View style={{ width: 200, flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center', padding: 10 }}>
                    <Text style={{fontSize: 16 }}>{rowData}</Text>
                </View>
            </TouchableHighlight>
        );
    }

    _formatTime(time) {
        let hour = time.hours
        let minute = time.minutes
        let timeofDay = hour<12? 'a.m':'p.m'
        return (timeofDay == 'a.m'? hour:hour-12) + ':' + (minute < 10 ? '0' + minute : minute) + ' ' + timeofDay;
    }

    showTimePicker = async (stateKey, options, rowData, isStart) => {
        try {
            var newTime ={};
            const {action, minute, hour} = await TimePickerAndroid.open(options);
            if (action === TimePickerAndroid.timeSetAction) {
                newTime = {hours: hour, minutes: minute}
                let rowId = this.state.rowData.findIndex((row)=>row.day == rowData.day)

                let totalstartTime =  this.state.rowData[rowId].data.start != undefined? 
                    this.state.rowData[rowId].data.start.hours*60 + this.state.rowData[rowId].data.start.minutes : 0

                let totalendTime =  this.state.rowData[rowId].data.end != null? 
                    this.state.rowData[rowId].data.end.hours*60 + this.state.rowData[rowId].data.end.minutes : 0

                if( isStart == false && (newTime.hours*60 + newTime.minutes) <= totalstartTime){
                    Alert.alert('Error!', 'Please Select a Time after Starting Time')
                }else if( isStart == true && totalendTime != 0 && (newTime.hours*60 + newTime.minutes) > totalendTime){
                    Alert.alert('Error!', 'Please Select a Time before Ending Time')
                }else{
                    isStart? this.state.rowData[rowId].data.start = newTime : this.state.rowData[rowId].data.end = newTime;
                    this.setState(this.state)
                }
            } else if (action === TimePickerAndroid.dismissedAction) {
                newTime[stateKey + 'Text'] = 'dismissed';
            }
            //console.log(newTime)
            
        } catch ({code, message}) {
            console.warn(`Error in example '${stateKey}': `, message);
        }
    };

    getRow(rowData){
        return(
            <View style={Styles.rowContainer}>
                <View style={Styles.rippleContainer} >
                        
                        <View style= {{flex: 1, flexDirection: 'row', alignItems: 'center'}}>
                            <Text style={[Styles.text,{color: 'black', fontWeight: '200'}]}>
                            {rowData.day}
                            </Text>
                        </View>

                        <Ripple text=""  style ={Styles.pickerBox} color='rgba(255,255,255,.2)' onPress={ () => {
                            let Time = new Date();
                            this.showTimePicker('start', {
                                hour: Time.getHours(),
                                minute: Time.getMinutes(),
                                is24Hour: false,
                            }, rowData, true)
                        }} >                     
                            <Text style = {[Styles.text,{paddingRight: 5}]} >{rowData.data.start == null ? 'Start Time': this._formatTime(rowData.data.start)}</Text>
                            <Icon name="arrow-drop-down" size={20} color="#000000" />
                        </Ripple>

                        <Ripple text=""  style ={Styles.pickerBox} color='rgba(255,255,255,.2)' onPress={ () => {
                            let Time = new Date();
                            this.showTimePicker('start', {
                                hour: Time.getHours(),
                                minute: Time.getMinutes(),
                                is24Hour: false,
                            }, rowData, false)
                        }} >                     
                            <Text style = {[Styles.text,{paddingRight: 5}]} >{rowData.data.end == null ? 'End Time': this._formatTime(rowData.data.end)}</Text>
                            <Icon name="arrow-drop-down" size={20} color="#000000" />
                        </Ripple>

                        <ModalDropdown
                            style={[Styles.pickerBox]}
                            options={this.state.roleData}
                            onSelect={(idx, value) => {
                                let rowId = this.state.rowData.findIndex((row)=>row.day == rowData.day)
                                this.state.rowData[rowId].data.role = value
                                this.setState(this.state)
                            }}
                            renderRow ={this.dropDown.bind(this)}
                            >
                            <View style={Styles.dropdown}>
                                <Text style={{ paddingLeft: 20, color: 'black', fontSize: 16 }}>{rowData.data.role == null? 'Select Role': rowData.data.role}</Text>
                                <Icon name="arrow-drop-down" size={20} color="#000000" />
                            </View>
                        </ModalDropdown>

                        <ModalDropdown
                            style={Styles.pickerBox}
                            options={['Workshop 1', 'Workshop 2']}
                            onSelect={(idx, value) => {
                                let rowId = this.state.rowData.findIndex((row)=>row.day == rowData.day)
                                this.state.rowData[rowId].data.department = value
                                this.setState(this.state)
                            }}
                            renderRow ={this.dropDown.bind(this)}
                            >
                            <View style={Styles.dropdown}>
                                <Text style={{paddingLeft:20,  color: 'black', fontSize: 16 }}>{rowData.data.department == null? 'Select Department': rowData.data.department}</Text>
                                <Icon name="arrow-drop-down" size={20} color="#000000" />
                            </View>
                        </ModalDropdown>
                    </View>
                </View>
        );
    }

    

    render(){
           
        return (
            <View style={Styles.container}>
                <ScrollView style={Styles.container}>
                    
                    <ListView 
                        style={{ backgroundColor: 'transparent' }}
                        dataSource={this.state.dataSource}
                        enableEmptySections={true}
                        renderRow={(rowData) => this.getRow(rowData)}
                        //renderSeparator={(sectionId, rowId) => <View key={rowId} style={styles.separator} />}
                        />
                       
                </ScrollView>  
            </View>
        
        );
  }
}

const Styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#eeeeee',
    paddingHorizontal: 5
  },
  rowContainer:{
    flex: 1,
    margin: 5,
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: 'white',
    elevation: 2
  },
  separator: {
    flex: 1,
    height: StyleSheet.hairlineWidth,
    backgroundColor: '#8E8E8E',
  },
  text: {
        paddingLeft: 15,
        color: '#000000',
        fontSize: 16
    },
    photo: {
        height: 60,
        width: 60,
        borderRadius: 60,
    },
    rippleContainer: {
        flex: 1,
        flexDirection: 'row',
        height: 80,
        padding: 12,
        justifyContent: 'space-between',
    },
    pickerBox:{
        flex:1,
        marginHorizontal: 10,
        borderColor: '#D6D6D6', 
        borderWidth: 1,
        flexDirection:'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    dropdown:{
        marginLeft: 5, 
        flex: 1, width: 
        Dimensions.get('window').width/6,
        flexDirection: 'row', 
        justifyContent: 'space-between', 
        alignItems: 'center',
    }
});


/**
 * 
 * 
 *  
 */