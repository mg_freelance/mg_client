import React, { Component } from 'react';
import { View, StyleSheet, Alert, Text, Dimensions, DatePickerAndroid, Image, ListView, TouchableHighlight,  Animated } from 'react-native';
import ModalDropdown from 'react-native-modal-dropdown';
import {Colors} from '../../../../../Themes'
import Icon from 'react-native-vector-icons/MaterialIcons';
import {Ripple} from 'react-native-material-design';
import PatternList from './PatternList';
import { Actions as NavigationActions } from 'react-native-router-flux';
import {api} from '../../../../../Lib/helpers';


export default class PatternView extends React.Component{
    constructor(props){
        super(props)
        this.state = {
            startDate: new Date(),
            endDate: new Date().setDate(new Date().getDate() + 7),
            persons:[],
            selectedPerson: []
        }
    }

    componentDidMount() {
       api().get('user').then((response) => {
            let personData = response.data.map((usr) => {
                return {
                    "name": usr.firstName + " " + usr.surName,
                    "image": usr.profileImg
                }
            })            
            this.setState({
                    persons: personData,
            })
        });
    }

    componentWillMount(){
        this.props.setActionHandlers('confirm', () => {
            Alert.alert(
                'Success',
                'Pattern Created Successfully',
                [
                    {
                        text: 'OK', onPress: () => {
                            NavigationActions.pop();
                        }
                    },
                ]
            );
        });
        this.props.setActionHandlers('delete', () => {
            Alert.alert(
                'Delete',
                'Are you sure you want to Delete?',
                [
                    { text: 'Cancel', onPress: () => { } },
                    {
                        text: 'OK', onPress: () => {
                            NavigationActions.pop();
                        }
                    },
                ]
            )
        });
    }

    getDay(day){
        let month = ["Jan", "Feb", "Mar", "Apr", "May", "June", "July", "Aug", "Sep", "Oct", "Nov", "Dec"]
        
        return (day.getDate() +' '+ month[day.getMonth()] +', '+ day.getFullYear());
    }

    personRow(rowData) {

        return (
            <View style={{ width: 220, flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center',padding: 10 }}>
                <Image style={[Styles.image]} source={{ uri: rowData[1] }} />
                <Text style={{ paddingLeft: 20, color: 'black', fontSize: 16 }}>{rowData[0]}</Text>
            </View>
        );
    }

    showDatePicker = async (stateKey, options, isStart) => {
        try {
            var newState = {};
            const {action, year, month, day} = await DatePickerAndroid.open(options);
            if (action === DatePickerAndroid.dismissedAction) {
                newState[stateKey + 'Text'] = 'dismissed';
            } else if (action === DatePickerAndroid.dateSetAction) {
                var newState = {...this.state };
                console.log(isStart)
                var date = new Date(year, month, day);
                
                if(isStart){
                    newState.startDate = date;
                }else{
                    newState.endDate = date;
                }
                this.setState(newState);

            }
            this.setState(newState);
        } catch ({code, message}) {
            console.warn(`Error in example '${stateKey}': `, message);
        }
    };

    render(){
        return( 
            <View style = {Styles.container} >
                <View style= {Styles.selectionBar} >
                    <Text style = {Styles.text}>Start Date:</Text>
                    <Ripple text=""  style ={Styles.pickerBox} color='rgba(255,255,255,.2)' onPress={ () => {
                            this.showDatePicker('startDate', {
                                            date: this.state.startDate
                                        }, true);
                    }} >                     
                        <Text style = {[Styles.text,{paddingRight: 5}]} >
                            {this.getDay(this.state.startDate)}
                        </Text>
                        <Icon name="arrow-drop-down" size={20} color="#000000" />
                    </Ripple>

                    <Text style = {Styles.text}>End Date:</Text>
                    <Ripple text=""  style ={Styles.pickerBox} color='rgba(255,255,255,.2)' onPress={ () => {
                            this.showDatePicker('endDate', {
                                            date: new Date(this.state.endDate)
                                        }, false);
                    }} >                     
                        <Text style = {[Styles.text,{paddingRight: 5}]} >
                            {this.getDay(new Date(this.state.endDate))}
                        </Text>
                        <Icon name="arrow-drop-down" size={20} color="#000000" />
                    </Ripple>

                    <ModalDropdown
                        style={[Styles.pickerBox,{marginLeft: 30}]}
                        options={this.state.persons.map((person) => {
                                    let det = []
                                    det.push(person.name)
                                    det.push(person.image)
                                    return det
                                })}
                        onSelect={(idx, value) => this.setState({ selectedPerson: value })}
                        
                        renderRow = {this.personRow.bind(this)}
                        >
                        <View style={{ width: 200, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                            <Image style={[Styles.image]} source={{ uri: this.state.selectedPerson[1] }} />
                            <Text style={{ paddingLeft: 20, color: 'black', fontSize: 16 }}>{this.state.selectedPerson[0] == null ? 'Select User': this.state.selectedPerson[0]}</Text>
                            <Icon name="arrow-drop-down" size={20} color="#000000" />
                        </View>
                    </ModalDropdown>
                </View>

                <PatternList/>

            </View>
        );
    }
}

const Styles = StyleSheet.create({
    container: {
        flex: 1,
        marginTop: 56,
        backgroundColor: Colors.background
    },
    selectionBar: {
        height: 70, 
        borderStyle: 'solid', 
        borderBottomWidth: 3, 
        borderBottomColor: '#D6D6D6', 
        flexDirection: 'row', 
        justifyContent: 'flex-start', 
        alignItems: 'center' 
    },
    text: {
        paddingLeft: 15,
        color: '#000000',
        fontSize: 16
    },
    pickerBox:{
        marginLeft: 5, 
        padding: 10,
        paddingLeft:5, 
        borderColor: '#D6D6D6', 
        borderWidth: 1,
        flexDirection:'row'
    },
    image: {
        height: 30,
        width: 30,
        borderRadius: 60
    },
});