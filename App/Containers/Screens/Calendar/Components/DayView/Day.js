import React, { Component } from 'react';
import { View, ScrollView, StyleSheet, Alert, Text, Dimensions, Picker, Image, ListView, TouchableHighlight, Animated } from 'react-native';
import { Colors, Metrics } from '../../../../../Themes';
import { Actions as NavigationActions } from 'react-native-router-flux';
import { Images } from '../../../../../Themes'
import Icon from 'react-native-vector-icons/MaterialIcons';
import CellItem from '../Cells/CellItem';
export default class DayScreen extends Component {
    hours = ['6 am','7 am','8 am', '9 am', '10 am',
        '11 am', '12 pm', '1 pm',
        '2 pm', '3 pm', '4 pm',
        '5 pm', '6 pm', '7 pm',
        '8 pm'];
    weekdays = [' ','sun', 'mon', 'tue', 'wed', 'thu', 'fri', 'sat'];
    renderWeekDaysHeader() {

        return this.weekdays.map((day) => {
            return (
                <Text key={day} style={styles.calendar_weekdays_text}>{day.toUpperCase()}</Text>
            );
        });
    }    
    renderRow(cellCount) {
        var rows = [];
        for (var hour in this.hours) {
            rows.push(
                <View style={{ flex: 1, flexDirection: 'row' }}>
                    <View style={styles.calendar_cell}>
                        <Text style={{ padding: 10,paddingRight: 5, color: '#747474', fontSize: 16 }}>
                            {this.hours[hour]}
                        </Text>
                    </View>
                    {this.renderCells(cellCount)}
                </View>)
        }
        return rows;
    }

    renderCells(count){
        var cells=[];
        for(var i=0;i<count;i++){
          cells.push(<View style={[styles.calendar_cell, { flex: (7/count) }]}>
                         {this.randomCellType()}
                    </View>);
        }
        return cells;
    }

     randomCellType(){
        var cells=[];
        var count = Math.floor((Math.random()*100) % 2);
        var assigned =[];
       for (var i=0;i<=count;i++)
        {
            if (i!=0){
            cells.push(<CellItem type='day'/>);
            }
        }

        if (cells.length==0){
            return null;
        }
        return cells;
    }

    render() {
        if (this.props.type == "day") {
            return (

                <ScrollView style={{ flex: 1, elevation: 2, flexDirection: 'column' }}>
                    {this.renderRow(1)}
                </ScrollView>);
        } else {
            return (

                <ScrollView style={{ flex: 1, elevation: 2, flexDirection: 'column' }}>
                     <View style={{ flex:1,padding: 10, flexDirection: 'row', borderBottomWidth: 2, borderBottomColor: '#d2d2d2', height: 45 }}>
                    {this.renderWeekDaysHeader()}
                </View>
                   
                        {this.renderRow(7)}

                </ScrollView>);

        }
    }
}

const styles = StyleSheet.create({
    calendar_weekdays_text: {
        flex: 1,
        color: '#303030',
        textAlign: 'center',
        padding: 6,
        paddingTop: 0,
    },
    calendar_cell: {
        flex: 1,
        height: 50,
        borderRightWidth: 1,
        borderRightColor: '#d2d2d2',
        borderBottomWidth: 1,
        borderBottomColor: '#d2d2d2',
    }
});