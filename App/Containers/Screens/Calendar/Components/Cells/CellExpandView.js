import React, { Component } from 'react';
import {ScrollView, ListView, View, StyleSheet, Alert,Dimensions, Text } from 'react-native';
import { Colors, Metrics } from '../../../../../Themes';
import CellExpandRow from './CellExpandRow'
import SetRoleModel from '../setRoleModel'

export default class CellExpandView extends Component {

     constructor(props){
        super(props);
        const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
        this.state = {
            dataSource: ds.cloneWithRows([
                'day 1', 'day 2', 'day 3', 'day 4', 'day 5', 'day 6', 'day 7'
            ]),
            roleMdl: false
        };
    }

    // toggleModal(openStatus) {
    //     this.setState({
    //       roleMdl: openStatus
    //     });
    // }

    render(){
        return (
             <View style={styles.container}>
                <ScrollView style={styles.container} onScroll= {()=>{}}>
                <ListView style={{ backgroundColor: 'transparent' }}
                    dataSource={this.state.dataSource}
                    enableEmptySections={true}
                    renderRow={
                        (rowData) => { 
                            return (
                                <CellExpandRow 
                                    data={this.props.data} 
                                />
                            ) 
                        }   
                    }
                    
                    //renderSeparator={(sectionId, rowId) => <View key={rowId} style={styles.separator} />}
                    >
                </ListView>
                </ScrollView>

               
                
            </View>
        );
    }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 30,
    backgroundColor: '#dddddd'
  },
  page: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  tabbar: {
    backgroundColor:  Colors.background,
  },
  indicator: {
    backgroundColor: '#408fff',
  },
  label: {
    color: '#222',
    fontWeight: '400',
  },
});

/**
 *  <SetRoleModel
                    //width = {3*Dimensions.get('window').width/4}
                    title={''}
                    userData = {this.props.roleData[0]}
                    roleData = {this.props.roleData[1]}
                    userType = {this.state.userType}
                    isOpen={this.state.roleMdl}
                    onClosed={() => { this.toggleModal(false) } }
                    onApply={(text) => { 
                        this.toggleModal(false)
                    }} 
                />
 */