import React, { Component } from 'react';
import { View, ScrollView, StyleSheet, Alert, Text, Dimensions, Picker, Image, ListView, TouchableHighlight, Animated } from 'react-native';
import { Colors, Metrics } from '../../../../../Themes';
import { Actions as NavigationActions } from 'react-native-router-flux';
import { Images } from '../../../../../Themes'
import Icon from 'react-native-vector-icons/FontAwesome';

export default class CellItem extends Component {

    render() {
        if (this.props.type == 'manager') {
            return (

                <View style={{ flexDirection: 'row', height: 30, padding: 2, paddingLeft: 5, margin: 3, backgroundColor: '#696fa7', color: '#fff' }}>
                    <Icon style={{ color: '#fff' }} name="user" size={24}> </Icon>
                    <Text style={{ color: '#fff', fontSize: 14,paddingTop: 2 }}>3</Text>
                     <Text style={{ color: '#fff', fontSize: 14,paddingTop: 2, paddingLeft: 9 }}>Managers</Text>
                </View>

            );
        } else if (this.props.type == 'floater') {
            return (
                <View style={{ flexDirection: 'row', height: 30, padding: 2, paddingLeft: 5, margin: 2, backgroundColor: '#FF9933', color: '#fff' }}>
                    <Icon style={{ color: '#fff' }} name="caret-up" size={26}> </Icon>
                    <Text style={{ color: '#fff', fontSize: 14,paddingTop: 2 }}>1</Text>
                    <Text style={{ color: '#fff', fontSize: 14,paddingTop: 2, paddingLeft: 9 }}>Floater</Text>
                </View>);
        } else if (this.props.type == 'painter') {
            return (
                <View style={{ flexDirection: 'row', height: 30, padding: 2, paddingLeft: 5, margin: 2, backgroundColor: '#63C1D5', color: '#fff' }}>
                    <Icon style={{ color: '#fff' }} name="fire-extinguisher" size={24}> </Icon>
                    <Text style={{ color: '#fff', fontSize: 14,paddingTop: 2 }}>1</Text>
                    <Text style={{ color: '#fff', fontSize: 14,paddingTop: 2, paddingLeft: 9 }}>Painter</Text>
                </View>);
        }else if (this.props.type == 'technician') {
            return (
                <View style={{ flexDirection: 'row', height: 30, padding: 2, paddingLeft: 5, margin: 2, backgroundColor: '#93C265', color: '#fff' }}>
                    <Icon style={{ color: '#fff' }} name="wrench" size={24}> </Icon>
                    <Text style={{ color: '#fff', fontSize: 14,paddingTop: 2 }}>1</Text>
                    <Text style={{ color: '#fff', fontSize: 14,paddingTop: 2, paddingLeft: 9 }}>Technician</Text>
                </View>);
        }else if (this.props.type == 'specialist') {
            return (
                <View style={{ flexDirection: 'row', height: 30, padding: 2, paddingLeft: 5, margin: 2, backgroundColor: '#CC6699', color: '#fff' }}>
                    <Icon style={{ color: '#fff' }} name="eye" size={22}> </Icon>
                    <Text style={{ color: '#fff', fontSize: 14,paddingTop: 2 }}>2</Text>
                    <Text style={{ color: '#fff', fontSize: 14,paddingTop: 2, paddingLeft: 9 }}>Specialist</Text>
                </View>);
        }else if (this.props.type == 'day') {
            return (
                <View style={{ flexDirection: 'row',flex:1, padding: 2, paddingLeft: 5, backgroundColor: '#CC6699', color: '#fff' }}>
                    <Icon style={{ color: '#fff' }} name="eye" size={24}> </Icon>
                    <Text style={{ color: '#fff', fontSize: 14,paddingTop: 2, paddingLeft: 9 }}>Specialist</Text>
                </View>);
        }else{
           return( <View></View>)
        }
    }
}