import React, { Component } from 'react';
import { View, Text, StyleSheet, Image, Dimensions } from 'react-native';
import { Avatar, Drawer, Divider, PRIMARY_COLORS, TYPO, Ripple } from 'react-native-material-design';
import { Actions as NavigationActions } from 'react-native-router-flux';
import ActionButton from 'react-native-action-button';
import Icon from 'react-native-vector-icons/MaterialIcons';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        margin: 5,
        marginHorizontal: 10,
        flexDirection: 'column',
        alignItems: 'center',
        backgroundColor: 'white',
        elevation: 2,
        height: 300
    },
    text: {
        marginLeft: 12,
        marginTop: 0,
        fontSize: 18,
        color: 'black'
    },
    photo: {
        height: 60,
        width: 60,
        borderRadius: 60,
    },
    header: {
        flexDirection: 'row',
        alignItems: 'flex-start',
        height: 55,
        width: Dimensions.get('window').width-10,
        //backgroundColor: '#408fff',
        borderBottomWidth : 2,
        borderBottomColor: '#dddddd'
    },
});

export default class CellExpandRow extends React.Component {
    constructor(props) {
        super(props)
    }

    getflexValue(){
        let modelWidth = Dimensions.get('window').width

        return modelWidth>500 ? 1: 2
    }
    
    MsgIcons = {
            "notification": 'error-outline',
            "message": "assignment",
            "critical": 'assignment-late',
        }

    getDetails(data){
        let roleData =[]
        for(let i =0; i< (data.length-1); i++){
           roleData.push(data[i])
        }

         return(
                <View style ={{padding: 2, paddingHorizontal: 15,justifyContent: 'flex-start', flex: 1 , width:Dimensions.get('window').width}}>
                    {roleData}
                </View>
            )
    }

    

    render() {
        return (
            <View style={styles.container}>
                <View style ={styles.header}>
                    <View style={{flex: 1, flexDirection: 'row', justifyContent: 'space-between', padding: 10}}>
                        <View style ={{flexDirection: 'column', justifyContent: 'flex-start', padding: -5}}>   
                            <Text style ={styles.text}>13 November</Text>
                            <Text style ={[styles.text, {fontSize: 14, color: '#888888'}]}>Friday</Text>
                        </View>
                    </View>
                </View>
                {this.getDetails(this.props.data)}
            </View>
        );
    }
}


/**
 * 
 * <Ripple style={styles.rippleIconcontainer} color='rgba(0,0,0,.5)'>
                             <Image source={Images.ApproveIcon} style={[styles.photo]} />
                        </Ripple>
                         <Ripple style={styles.rippleIconcontainer} color='rgba(0,0,0,.5)'>
                             <Image source={Images.RejectIcon} style={[styles.photo]} />
                        </Ripple>
 */