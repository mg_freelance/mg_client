import React, { Component } from 'react';
import { View, ScrollView, StyleSheet, Alert, Text, Dimensions, Picker, Image, ListView, TouchableHighlight, Animated } from 'react-native';
import { Colors, Metrics } from '../../../../../Themes';
import { Actions as NavigationActions } from 'react-native-router-flux';
import { Images } from '../../../../../Themes'
import Icon from 'react-native-vector-icons/MaterialIcons';
import CalendarCell from './CalendarCell'
export default class CalendarRow extends Component {
    constructor(props) {
        super(props);
    }

    renderRows() {

        var rows = [];
        for (var weekNo = 0; weekNo <= (this.props.dayList.length); weekNo += 7) {
            rows.push(
                <CalendarCell dayList={this.props.dayList} weekNo={weekNo} roleData= {this.props.roleData}/>
            );
        }
        return rows;
    }

    render() {
        return (
            <View style={{ flex: 1, flexDirection: 'column' }}>

                {this.renderRows()}
            </View>
        );
    }
}