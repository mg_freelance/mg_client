import React, { Component } from 'react';
import { View, ScrollView, StyleSheet, Alert, Text, Dimensions, Picker, Image, ListView, TouchableHighlight, Animated } from 'react-native';
import { Colors, Metrics } from '../../../../../Themes';
import { Actions as NavigationActions } from 'react-native-router-flux';
import { Images } from '../../../../../Themes'
import Icon from 'react-native-vector-icons/MaterialIcons';
import CellItem from '../Cells/CellItem';
export default class CalendarCell extends Component {
    cellType=['','manager','floater','technician','specialist','painter'];
    constructor(props){
        super(props)
    }

    renderCells() {
        var cells = [];
        
        for (var day = this.props.weekNo; day < (this.props.weekNo + 7); day++) {
            cells.push(
                <View style={styles.calendar_cell}>
                    <Text style={{ padding: 5, color: '#747474', fontSize: 16 }}>
                        {this.props.dayList[day]}
                    </Text>
                    {this.randomCellType(this.props.dayList[day])}
                       
                </View>)
        }
        return cells;
    }

    randomCellType(days){
        var cells=[];
        var count = Math.floor((Math.random()*100) % 3);
        var assigned =[];
        if (days == undefined || days.length==0)
            return null;
       
        for (var i=0;i<=count;i++)
        {
            var type= Math.floor((Math.random()*100) % 6);
            while(this.assigned(assigned,type)==true){
                type= Math.floor((Math.random()*100) % 6);
            }
            assigned.push(type);
           //  window.alert(this.cellType[type]);
            cells.push(<CellItem type={this.cellType[type]}/>);
        }

        if (cells.length <= 1){
            return null;
        }else {
            cells.push(<Icon
                        name="more-horiz" 
                        size={20} 
                        color="#000000" 
                        onPress={()=>{NavigationActions.CellExpandView({ data: cells, roleData: this.props.roleData})}}
                    /> )
            return cells;
        }
        
        
    }
    assigned(assigned,type){
          for (var j in assigned)
            {
                if (assigned[j] == type){
                    return true;
                }
            }
            return false;
    }
    render() {
        return (
            <View style={{ flex: 1, flexDirection: 'row' }}>
                {this.renderCells()}
            </View>
        )
    }
}

const styles = StyleSheet.create({
    calendar_weekdays_text: {
        flex: 1,
        color: '#303030',
        textAlign: 'center',
        padding: 6,
        paddingTop: 0,
        
    },
    calendar_cell: {
        flex: 1,
        height: 165,
        borderRightWidth: 1,
        flexDirection: 'column',
        borderRightColor: '#d2d2d2',
        borderBottomWidth: 1,
        borderBottomColor: '#d2d2d2',
      
    }
});