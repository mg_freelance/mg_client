import React, { Component } from 'react';
import { View, ScrollView, StyleSheet, Alert, Text, Dimensions, Picker, Image, ListView, TouchableHighlight, Animated } from 'react-native';
import { Colors, Metrics } from '../../../../../Themes';
import { Actions as NavigationActions } from 'react-native-router-flux';
import { Images } from '../../../../../Themes'
import Icon from 'react-native-vector-icons/MaterialIcons';
import CalendarRow from './CalendarRow'
export default class CalendarScreen extends Component {
    weekdays = ['sun', 'mon', 'tue', 'wed', 'thu', 'fri', 'sat'];

    renderWeekDaysHeader() {

        return this.weekdays.map((day) => {
            return (
                <Text key={day} style={styles.calendar_weekdays_text}>{day.toUpperCase()}</Text>
            );
        });
    }

    renderCalendarData() {
        var dayList = [];
        var selectedDate = new Date(2017, 3);
        var firstDate = new Date(selectedDate);
        firstDate.setDate(1);
        var firstDay = firstDate.getDay();
        var daysTillFirstDay = firstDay;

        while (daysTillFirstDay > 0) {
            dayList.push('');
            --daysTillFirstDay;
        }

        daysInMonth = new Date(selectedDate.getYear(), selectedDate.getMonth() + 1, 0).getDate();

        for (var i = 1; i <= daysInMonth; i++) {
            dayList.push(i);
        }
        
        return (
            <CalendarRow dayList={dayList} roleData = {this.props.data}></CalendarRow>
        );

    }
    render() {
        return (
            <View style={{ flex: 1, elevation: 2, flexDirection: 'column' }}>
                <View style={{ padding: 10, flexDirection: 'row', borderBottomWidth: 2, borderBottomColor: '#d2d2d2', height: 45 }}>
                    {this.renderWeekDaysHeader()}
                </View>
                <ScrollView style={{ flex: 1 }}>
                    {this.renderCalendarData()}
                </ScrollView>
            </View>
        );
    }
}
const styles = StyleSheet.create({
    calendar_weekdays_text: {
        flex: 1,
        color: '#303030',
        textAlign: 'center',
        padding: 6,
        paddingTop: 0,
    },
    calendar_cell: {
        flex: 1,
        height: 100,
        borderRightWidth: 1,
        borderRightColor: '#d2d2d2',
        borderBottomWidth: 1,
        borderBottomColor: '#d2d2d2',
    }
});