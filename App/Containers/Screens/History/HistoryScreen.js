import React, { Component } from 'react';
import { View, ListView, ScrollView, StyleSheet, Text } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import {api} from '../../../Lib/helpers';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginTop: 35,

    },
    containerInner: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        marginLeft: 10,
        paddingTop:20,
        paddingBottom:20
    },
    photo: {
        height: 60,
        width: 60,
        paddingTop: 15,
        paddingLeft: 20,
        borderRadius: 60,
        backgroundColor: '#01579b',
        color: 'white',
        fontSize: 30
    },
    text: {
        marginLeft: 12,
        fontSize: 16,
    },
    separator: {
        flex: 1,
        height: StyleSheet.hairlineWidth,
        backgroundColor: '#8E8E8E',
    },
});


export default class HistoryScreen extends Component {

    constructor() {
        super();
        const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
        this.state = {
            dataSource: ds.cloneWithRows([{ test: 'test' }]),
        };
    }
    componentWillMount() {
        api().get('history').then((response) => {
            const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
            this.setState({
                dataSource: ds.cloneWithRows(response.data),
            });
        });
    }
    render() {
        return (
            <View style={styles.container}>
                <ScrollView style={styles.container}>
                    <ListView
                        dataSource={this.state.dataSource}
                        enableEmptySections={true}
                        renderRow={(rowData) => {
                            return (
                                <View style={styles.containerInner}>
                                    <View style={{ flex: 1, flexDirection: 'column' }}>
                                        <View style={{ flex: 1, flexDirection: 'row' }}>
                                            <Icon name="md-repeat" style={styles.photo} />

                                            <View style={{ flex: 1, flexDirection: 'column' }}>
                                                <Text style={styles.text}>
                                                    {rowData.history} - {rowData.userFirstName} {rowData.userSurname}
                                            </Text>
                                                <Text style={styles.text}>{rowData.timestamp}</Text>
                                            </View>
                                        </View>

                                    </View>
                                </View>)
                        } }
                        renderSeparator={(sectionId, rowId) => <View key={rowId} style={styles.separator} />}
                        >
                    </ListView>
                </ScrollView>
            </View>);
    }
}