import React, { Component } from 'react';
import { View, StyleSheet, Alert, Text, Dimensions, Picker, Image, ListView, TouchableHighlight,  Animated } from 'react-native';
import { Colors, Metrics } from '../../../Themes';
import Button from '../../../Components/Button';
import { Actions as NavigationActions } from 'react-native-router-flux';
import Icon from 'react-native-vector-icons/MaterialIcons';
import { TabViewAnimated, TabBarTop } from 'react-native-tab-view';
import ActionButton from 'react-native-action-button';

import {api} from '../../../Lib/helpers'
import DetailsMain from  './Components/DetailsView/DetailsMain';
import LinesMain from './Components/LinesView/LinesMain';
import WorkMain from './Components/WorkView/WorkMain';

export default class WorkScreen extends Component {

     constructor() {
        super();
        this.state = {
            index: 0,
            routes: [
                { key: 'details', title: 'Job Details' },
                { key: 'lines', title: 'Job Lines' },
                { key: 'work', title: 'Work' },
            ],

            jobEvents: [
                {
                    EventName: 'Starting Job',
                    Scheduled: ['9/2/2017', {hours: '11', minutes: '0'}],
                    inSequence: [],
                    inProduction: [],
                    inCompletion: []
                },
                {
                    EventName: 'Completing Job',
                    Scheduled: ['2/13/2017',  {hours: '15', minutes: '0'}],
                    inSequence: [],
                    inProduction: [],
                    inCompletion: []
                }
            ]
        }

    }

    _handleChangeTab = (index) => {
        this.setState({ index: index });
    };

    _renderWorkScene = ({ route }) => {
        switch (route.key) {
            case 'details':
                return <DetailsMain events= {this.state.jobEvents}/>
            case 'lines':
                return (<LinesMain />)
            case 'work':
                return (<View style={{ flex:1, backgroundColor: '#ffffff' }}>
                   <WorkMain/>
                </View>);
            default:
                return null;
        }
    };

    render(){
        return (
            <View style={styles.container}>
                    <TabViewAnimated
                        style={{
                            flex: 1,
                            
                        }}
                        navigationState={this.state}
                        renderScene={(route) => {
                            return (
                                <View style={{ flex:1,padding: 8, backgroundColor: '#EEEEEE' }}>
                                    {this._renderWorkScene(route)}
                                </View>);
                        }}
                        renderHeader={(props) => {
                            return (
                                <View style={{ flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center', height: 60, borderBottomWidth: 2, borderBottomColor: '#D6D6D6' }}>
                                    <View style={{flex:3,alignItems:'flex-start'}}>
                                        <TabBarTop {...props}
                                            tabWidth={120}
                                            style={styles.tabbar}
                                            labelStyle={styles.label}
                                            onRequestChangeTab={this._handleChangeTab}
                                            renderIndicator = {(props)=>{ 
                                                const {position} = props;
                                                const width = 120;
                                                const translateX = Animated.multiply(position, width);
                                                
                                                return (
                                                <Animated.View
                                                    style={[ styles.indicator, { width, transform: [ { translateX } ] }, styles.indicatorStyle ]}
                                                />
                                                );
                                            }}
                                            //indicatorStyle={styles.indicator}
                                            />
                                    </View>
                                </View>
                            );
                        } }
                        onRequestChangeTab={this._handleChangeTab}
                        lazy = {false}
                        />
                </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'row',
        marginTop: 56,
        backgroundColor: Colors.background,
        alignItems: 'center' 
    },
    page: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    tabbar: {
        width:360,
        backgroundColor: Colors.background,
        elevation: 0,

    },
    indicatorStyle: {
        backgroundColor: '#408fff',
        width:120,
        position:'absolute'
        
    },
    indicator: {
        backgroundColor: 'white',
        position: 'absolute',
        left: 0,
        bottom: 0,
        right: 0,
        height: 2,
    },
    label: {
        color: '#222',
        fontWeight: '400',
        fontSize: 14,
        margin: 10
    },
    text: {
        color: 'white',
        fontSize: 16,
    },
    image: {
        height: 40,
        width: 40,
        borderRadius: 60
    },
    actionButtonIcon: {
        fontSize: 20,
        height: 22,
        color: 'white',
    },
});