import React, { Component } from 'react';
import { View, StyleSheet, Dimensions, Text, TouchableHighlight, TextInput, Alert } from 'react-native';
import { TabViewAnimated, TabBarTop } from 'react-native-tab-view';
import {Colors} from '../../../../../Themes'
import ModalDropdown from 'react-native-modal-dropdown';
import Icon from 'react-native-vector-icons/MaterialIcons';
import SortableListView from 'react-native-sortable-listview'
import ActionButton from 'react-native-action-button';
//import ChecklistItem from './ChecklistItem'
import ItemModel from './ItemModel'
import { Actions as NavigationActions } from 'react-native-router-flux'
import R from 'ramda';
import {api} from '../../../../../Lib/helpers';
import LinesRow from './LinesRow'

let setupData = {}
let checklistItemData = {}

export default class LinesMain extends Component {
    
    state = {
        index: 0,
        workflowName: 'Select',
        workflowID: '',
        jobLineList : {
            "1": ["1.0", "Strip", "3", "", "SK002", ""],
            "2": ["2.0", "Pull Dents", "4", "", "SK002", ""],
            "3": ["3.0", "Filler", "1", "", "SK001", ""],
            "4": ["4.0", "Primer", "1", "", "SK003", ""],
            "5": ["5.0", "Paint", "3", "", "SK001", ""],
            "6": ["6.0", "Polish", "1", "", "SK001", ""],
            "7": ["7.0", "Refit", "2", "", "SK003", ""]
        },
        order : [],
        workflows: [],
        LineName: '',
        E_Hours: '',
        P_Hours: '',
        Req_Skill: '',
        skills: [],
        departments:[],
        checklists:[],
        isOpen: false
    };

    componentDidMount(){
        api().get('workflows').then((response) => {

            api().get('skill').then((skillResp)=>{

                api().get('departments').then((deptResp)=>{

                    api().get('checklists').then((chkListResp)=>{

                        this.setState({
                            skills: skillResp.data,
                            workflows: response.data,
                            departments: deptResp.data,
                            checklists: chkListResp.data,
                            order : Object.keys(this.state.jobLineList)
                        });
                    })
                });
                
            });
        });
    }
    

    componentWillMount(){

        // // if(!this.props.create){
        // //     api().get('checklist/'+ this.props.id).then((response) => {
        // //         this.setState({
        // //             ChecklistName: response.data[0].checkListName,
        // //             jobLineList: response.data[0].items,
        // //             order: response.data[0].checkListOrder

        // //         });
        // //     });
        // }
        
        // this.props.setActionHandlers('confirm', () => {

        //     if(this.state.ChecklistName == ''){
        //         Alert.alert(
        //             'Error',
        //             'Checklist Name cannot be blank'
        //         );
        //     }else{
        //         let Checklist = { checkListName: this.state.ChecklistName, items: this.state.jobLineList, checkListOrder: this.state.order}
        //         if(this.props.create){

        //             api().post('checklist', JSON.stringify(Checklist))
        //             .then((response) => {
        //                     Alert.alert(
        //                         'Success',
        //                         'Saved Successfully',
        //                         [
        //                             { text: 'OK', onPress: () => NavigationActions.pop() }
        //                         ]);
        //                 });

        //         }else{

        //             api().put('checklist/' + this.props.id, JSON.stringify(Checklist))
        //             .then((response) => {
        //                 Alert.alert(
        //                     'Success',
        //                     'Checklist Updated Successfully',
        //                     [
        //                         {
        //                             text: 'OK', onPress: () => {
        //                                 NavigationActions.pop();
        //                             }
        //                         },
        //                     ]
        //                 );
        //             });

        //         }
        //     }
        // });
        // this.props.setActionHandlers('delete', () => {
        //          Alert.alert(
        //             'Delete',
        //             'Are you sure you want to Delete?',
        //             [
        //                 { text: 'Cancel', onPress: () => { } },
        //                 {
        //                     text: 'OK', onPress: () => {
        //                         if(this.props.create){
        //                             NavigationActions.pop();
        //                         }else{
        //                             api().delete('checklist/'+ this.props.id).then((response) => {
        //                                  NavigationActions.pop();
        //                             });
        //                         }                               
        //                     }
        //                 },
        //             ]
        //         )
        // });
    }

    getDepartment(deptId){
        if(this.state.departments != [] && deptId != ''){
             let dept = this.state.departments.find((deptRow) =>{
                 return deptRow.departmentId == deptId
             })
             return dept
        }
        return {}
       
    }

    getChecklistName(chkID){
        if(this.state.checklists != [] && chkID != ''){
            //console.log(this.state.deptList)
            let checklst = this.state.checklists.find((chkRow)=>{
                return chkRow.checkListId == chkID
            });
            return checklst.checkListName
        }
        return ''
    }

    getWorkflowDetatils(flowIndex){
        api().get('workflow/'+ flowIndex.workflowId).then((response)=>{
            let skillDeptMap = response.data[0].SkillDept
            let isPresent = null
            let newLineList = {}//JSON.parse(JSON.stringify(this.state.jobLineList))

//extracting only the workflow
            let newOrder = this.state.order
            for([key, value] of Object.entries(this.state.jobLineList)){
                if(!isNaN(parseInt(value[0]))){
                    newLineList[key] = value                    
                }else{
                    newOrder = newOrder.filter((val)=>{
                        return val != key
                    })
                }
            }

            
            
//mapping workflow departments to skills 
            for([key, value] of Object.entries(newLineList)){
                isPresent = skillDeptMap.find((skDptrow)=>{
                    //console.log(skDptrow.skillId == value[4])
                    if(skDptrow.skillId == value[4]){
                        newLineList[key][5] = skDptrow.departmentId
                    }
                    return skDptrow.skillId == value[4]
                })

                if(isPresent == null){
                    Alert.alert(
                        'Error',
                        'Some Skills are not mapped to departments in the selected workflow'
                    );
                    break;
                }                
            }
//adding workflow checklists
            if(isPresent != null){
                let wrkflowChkPoints = response.data[0].checkpoints
                let begining = [], ending = []
                let lastKey = !R.isEmpty(newLineList) ? Math.max(...newOrder.map(Number)) :0;
                let CLCount = 1, BFCount = 1
                for([key, value] of Object.entries(wrkflowChkPoints)){
                    
                    newLineList[parseInt(lastKey)+1] = ["CL-WF"+CLCount, this.getChecklistName(value[0]), "", "", "", ""]
                    
                    if(value[1] == "Starting Job"){
                        begining.push(''+(++lastKey))
                    }
                    if(value[1] == "Completing Job"){
                        newOrder.push(''+ (++lastKey)) 
                    }
                   CLCount++
                }
                CLCount = 1
//adding workflow Buffer
                 let wrkflowBuffer = response.data[0].Buffers
                 wrkflowBuffer = wrkflowBuffer.fixedTime != ''? wrkflowBuffer.fixedTime: wrkflowBuffer.Efficiency != ''? wrkflowBuffer.Efficiency: '';
                 newLineList[parseInt(lastKey)+1] = ["BUFFER-WF", "Workflow Buffer", "", ('' + wrkflowBuffer), "", ""]
                 begining.unshift(''+(++lastKey))


                 newOrder = begining.concat(newOrder)
                 //console.log(newOrder)
                // console.log(newLineList)
                
                let completelist = JSON.parse(JSON.stringify(newLineList))
                let completeOrder = JSON.parse(JSON.stringify(newOrder))
                for(let j = 0; j < newOrder.length-1; j++){
                    //console.log(newOrder)
                    let i = newOrder[j]
                    let k = newOrder[j+1]
                    //console.log(i)
                     if(newLineList[i][5] != newLineList[k][5]){
                        if(newLineList[i][5] == ''){

                            let deptDet = this.getDepartment(newLineList[k][5])
                            wrkflowBuffer = deptDet.setup.DeptBufferTime != ''? deptDet.setup.DeptBufferTime: deptDet.setup.DeptBufferPercent != ''?deptDet.setup.DeptBufferPercent: '';
                            completelist[lastKey+1] = ["BUFFER-"+(BFCount++), "Department Buffer", "", ('' + wrkflowBuffer), "", ""]
                            completeOrder.splice(j+1, 0,''+(++lastKey))
                            begining = []
                             for([key, value] of Object.entries(deptDet.checkpoints)){                                
                                if(value[1] == "clocking on"){
                                    completelist[parseInt(lastKey)+1] = ["CL-S"+CLCount, this.getChecklistName(value[0]), "", "", "", ""]
                                    begining.push(''+(++lastKey))
                                    CLCount++
                                }
                             }
                             if(begining != []){
                                 //newOrder = newOrder.slice(0, j+2).concat(begining).concat(newOrder.slice(j+3, newOrder.length))
                                 begining.reverse().map((item)=>{
                                    completeOrder.splice(j+2, 0, item)
                                    //j++
                                 })
                             }
                             
                        }else if(newLineList[k][5] == ''){
                            ending = []
                            let deptDet = this.getDepartment(newLineList[i][5])
                            for([key, value] of Object.entries(deptDet.checkpoints)){                                
                               if(value[1] == "clocking off"){
                                   completelist[parseInt(lastKey)+1] = ["CL-E"+(CLCount++), this.getChecklistName(value[0]), "", "", "", ""]
                                   ending.push(''+(++lastKey))
                               }
                            }
                            if(ending != []){
                                let indx = completeOrder.indexOf(k)
                                ending.reverse().map((item)=>{
                                    completeOrder.splice(indx, 0, item)
                                 })
                            }

                        }else{
                            // console.log(newLineList[i][5] +'    ' + newLineList[k][5])
                            // console.log(i + '     '+ k)
                            
                        //Add next Department before checklists (added as in a stack nextDeptChecklist -> nextDeptBuffers -> currentDeptChecklists)
                            begining = []
                            for([key, value] of Object.entries(this.getDepartment(newLineList[k][5]).checkpoints)){
                                if(value[1] == "clocking on"){
                                    completelist[parseInt(lastKey)+1] = ["CL-S"+(CLCount++), this.getChecklistName(value[0]), "", "", "", ""]
                                    begining.push(''+(++lastKey))
                                }
                            }
                            if(begining != []){
                                let indx = completeOrder.indexOf(k)
                                
                                begining.reverse().map((item)=>{
                                    completeOrder.splice(indx, 0, item)
                                 })
                            }

                       
                        // Add next Department Buffers
                            let bufferAdded = false 
                            
                                let currentDept = response.data[0].TechOpen.find((line)=>{
                                    return line.departmentId == newLineList[i][5]
                                })
                                if(currentDept != null){
                                    let nextisOpen = currentDept.open.find((id) =>{
                                        return id == newLineList[k][5]
                                    })
                                    
                                    if(nextisOpen == null){
                                        let insideBuffer = this.getDepartment(newLineList[k][5]).setup
                                        insideBuffer = insideBuffer.otherBufferTime != ''? 
                                                            insideBuffer.otherBufferTime : insideBuffer.otherBufferPercent != ''? 
                                                                            insideBuffer.otherBufferPercent : ''
                                        
                                        if(insideBuffer != ''){
                                              completelist[lastKey+1] = ["BUFFER-"+(BFCount++), "Inside Department Buffer", "", ('' + insideBuffer), "", ""]
                                              let indx = completeOrder.indexOf(k)
                                              completeOrder.splice(indx-1, 0,''+(++lastKey))
                                              bufferAdded = true
                                        }
                                        
                                    }
                                }
                                
                            
                            if(!bufferAdded){
                                let departmentBuffer = this.getDepartment(newLineList[k][5]).setup
                                departmentBuffer = departmentBuffer.DeptBufferTime != ''? departmentBuffer.DeptBufferTime: departmentBuffer.DeptBufferPercent != ''?departmentBuffer.DeptBufferPercent: '';
                                completelist[lastKey+1] = ["BUFFER-"+(BFCount++), "Department Buffer", "", ('' + departmentBuffer), "", ""]
                                let indx = completeOrder.indexOf(k)
                                completeOrder.splice(indx-1, 0,''+(++lastKey))
                            }


                        
                         // Add previous Deparment after checklists
                            begining = [], ending=[]
                            for([key, value] of Object.entries(this.getDepartment(newLineList[i][5]).checkpoints)){
                                if(value[1] == "clocking off"){
                                    completelist[parseInt(lastKey)+1] = ["CL-E"+(CLCount++), this.getChecklistName(value[0]), "", "", "", ""]
                                    ending.push(''+(++lastKey))
                                }
                            }
                            if(ending != []){
                                let indx = completeOrder.indexOf(i)
                                ending.reverse().map((item)=>{
                                    completeOrder.splice(indx+1, 0, item)
                                 })
                            }
                            
                        }
                     }
                }
                 this.setState({
                    workflowName: flowIndex.workflowName,
                    workflowID: flowIndex,
                    jobLineList: completelist,
                    order: completeOrder
                })
            }
            
        })
    }


    toggleModal(openStatus) {
        this.setState({
          LineName: '',
          E_Hours: '',
          P_Hours: '',
          Req_Skill: '',
          isOpen: openStatus,
        });
    }
    
    

    dropDown(rowData) {
        return (
            <TouchableHighlight style={{ flex: 1}} >
                <View style={{ width: 200, flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center', padding: 10 }}>
                    <Text style={{fontSize: 16 }}>{rowData.workflowName}</Text>
                </View>
            </TouchableHighlight>
        );
    }

    render() {
        return (
            <View style = {{flex: 1}}>
                <View style = {{flexDirection: 'row',paddingBottom:10, paddingTop: 10, borderBottomWidth: 2, borderBottomColor: '#D6D6D6', backgroundColor: '#ffffff'}}>
                    <Text style={{paddingLeft:20, paddingTop: 10,  color: 'black', fontSize: 16 }}>Workflow: </Text>
                    <View> 
                        <ModalDropdown
                            style={{marginLeft:20, height: 40, width: 200, borderColor: 'gray', borderWidth: 1}}
                            options={this.state.workflows}
                            onSelect={(idx, value) => {
                                this.getWorkflowDetatils(value)
                                
                            }}
                            renderRow ={this.dropDown.bind(this)}
                        >
                        <View style={[styles.dropdown]}>
                            <Text style={{  color: 'black', fontSize: 16 }}>
                                {this.state.workflowName}
                            </Text>
                                <Icon name="arrow-drop-down" size={20} color="#000000" />
                        </View>
                        </ModalDropdown>
                    </View>
                </View>

                <View style = {{padding: 20, flexDirection: 'row',paddingBottom:10, borderBottomWidth: 2, borderBottomColor: '#D6D6D6', backgroundColor: '#F6F6F6'}}>
                    <View style={{flex:1}}>
                        <Text style={styles.titleRow}>Ref</Text>
                    </View>
                    <View style={{flex:2}}>
                        <Text style={styles.titleRow}>Job Line </Text>
                    </View>
                    <View style={{flex:2}}>
                        <Text style={styles.titleRow}>Estimated Hrs </Text>
                    </View>
                    <View style={{flex:2}}>
                        <Text style={styles.titleRow}>Planned Hrs </Text>
                    </View>
                    <View style={{flex:2}}>
                        <Text style={styles.titleRow}>Skill Req </Text>
                    </View>
                    <View style={{flex:3}}>
                        <Text style={styles.titleRow}>Department </Text>
                    </View>
                </View>

                <SortableListView
                    style={[styles.container, {paddingTop: 5}]}
                    data={this.state.jobLineList}
                    order={this.state.order}
                    onRowMoved={(e) => {
                        this.state.order.splice(e.to, 0, this.state.order.splice(e.from, 1)[0]);
                        if(this.state.workflowID != ''){
                            this.getWorkflowDetatils(this.state.workflowID)
                        }else{
                            this.forceUpdate();
                        }                        
                        
                    }}
                    renderRow={(row) =>
                        <LinesRow 
                            data = {row}
                            skillData={this.state.skills}
                            deptData={this.state.departments}
                            onIconPress = {(idx)=>{
                                for([key, value] of Object.entries(this.state.jobLineList)){
                                   if(value[0] == idx){
                                       delete this.state.jobLineList[key]
                                       this.state.order = this.state.order.reduce((newOrder, data)=>{
                                           if(data != key){
                                               newOrder.push(data)
                                           }
                                           return newOrder
                                       },[])//Object.keys(this.state.listData)
                                       this.forceUpdate()
                                    }
                                }
                            }}
                            //ref = {this.state.order.find((item)=>{item == })}
                            onPress={(IName, IType, id)=>{
                                this.setState({
                                    itemId: id,
                                    itemName: IName,
                                    itemtype: IType,
                                    isOpen: true
                                });
                                //this.toggleModal(true)
                            }}
                            
                        />} 
                    rowHasChanged ={ (r1, r2)=> r1 !== r2 }                   
                />

                

                <ActionButton buttonColor="#408fff" onPress={()=>{this.toggleModal(true)}}>
            
                </ActionButton>

               <ItemModel
                    //width = {3*Dimensions.get('window').width/4}
                    //modelType={'Approve'}
                    title={''}
                    itemData = {[this.state.itemName, this.state.itemtype, this.state.itemId]}
                    isOpen={this.state.isOpen}
                    onClosed={() => { this.toggleModal(false) } }
                    onApply={(Name, Type, id) => { 
                        if(Name == ''){
                             Alert.alert(
                                'Error',
                                'Name Cannot be Empty'
                            );
                        }else{
                            if(id == 0 ){
                                let lastKey = !R.isEmpty(this.state.jobLineList) ? Math.max(...this.state.order.map(Number)) :0 
                                this.state.jobLineList[parseInt(lastKey)+1] = {itemId: lastKey+1, itemName: Name, type: Type}
                                this.state.order.push(''+(lastKey+1))
                                
                                this.toggleModal(false)
                            }else{
                                for([key, value] of Object.entries(this.state.jobLineList)){
                                   if(value.itemId == id){
                                      
                                       this.state.jobLineList[id] = {itemId: id, itemName: Name, type: Type}
                                       
                                       //this.toggleModal(false)
                                       this.toggleModal(false)
                                       this.forceUpdate()
                                      
                                      
                                    }
                                }
                            }  
                           
                        }
                    }} 
                />

            </View>
      
        );
    }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F6F6F6',
  },
  page: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  tabbar: {
    backgroundColor:  '#fefefe',
  },
  indicator: {
    backgroundColor: '#408fff',
  },
  label: {
    color: '#222',
    fontWeight: '400',
  },
  dropdown:{
    flexDirection: 'row', 
    justifyContent: 'space-around', 
    alignItems: 'center',
    height: 35
  },
  titleRow:{
    paddingBottom: 10,  
    color: 'black', 
    fontSize: 16,
    textAlign: 'center'
  },
});


/**
 * 
 * 
 * 
 * <ChecklistItem 
                            data = {row}
                            onIconPress = {(idx)=>{
                                for([key, value] of Object.entries(this.state.jobLineList)){
                                   if(value.itemId == idx){
                                       delete this.state.jobLineList[key]
                                       this.state.order = this.state.order.reduce((newOrder, data)=>{
                                           if(data != key){
                                               newOrder.push(data)
                                           }
                                           return newOrder
                                       },[])//Object.keys(this.state.listData)
                                       this.forceUpdate()
                                    }
                                }
                            }}
                            itemType ={(selected)=>{
                                for([key, value] of Object.entries(this.state.jobLineList)){
                                   if(value.itemId == row.itemId){
                                       //let val = selected
                                       //console.log(val)
                                      this.state.jobLineList[key].type = selected
                                       //console.log(this.state.checkListData)
                                       this.forceUpdate()
                                    }
                                }
                            }}
                            onPress={(IName, IType, id)=>{
                                //console.log(IType)
                                this.setState({
                                    itemId: id,
                                    itemName: IName,
                                    itemtype: IType,
                                    isOpen: true
                                })
                                //this.toggleModal(true)
                            }}
                            listItems = {(data, order)=>{checklistItemData = {data, order}}} 
                        />
 */