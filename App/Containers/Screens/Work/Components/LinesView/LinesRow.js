import React, { Component } from 'react';
import { View, StyleSheet, Text, ScrollView, ListView, TouchableHighlight, Dimensions} from 'react-native';
import {Ripple} from 'react-native-material-design'
import ModalDropdown from 'react-native-modal-dropdown';
import Icon from 'react-native-vector-icons/MaterialIcons';

export default class LinesRow extends React.Component{

    constructor(){
        super()
        this.state = {
            itemType: '',
            skillList: null,
            deptList: null
        }
    }

    componentDidMount(){
        this.setState({
            itemType: this.props.data.type
        })
    }
    
    componentWillReceiveProps(newProps){
        if(newProps != this.props){
            this.setState({
                skillList: newProps.skillData,
                deptList: newProps.deptData
            })
           
        }
    }

    getSkillName(skillId){
        if(this.state.skillList != null && skillId != ''){
            //console.log(this.state.skillList != null)
            let skill = this.state.skillList.find((skRow)=>{
                return skRow.skillId == skillId
            })
            return skill.skillName
        }
        return ''
    }

    getDeptName(deptId){
        if(this.state.deptList != null && deptId != ''){
            //console.log(this.state.deptList)
            let department = this.state.deptList.find((dptRow)=>{
                return dptRow.departmentId == deptId
            });
            return department.departmentName
        }
        return ''
    }

    getPlannedHrs(skillId){
        if(this.props.data[3] != ''){

            return this.props.data[3]
            
        }else if(this.state.skillList != null && skillId != ''){

            let currentskill = this.state.skillList.find((skillRow)=>{
                return (skillRow.skillId == skillId)
            });

            if(currentskill.efficiencyPercent != ''){
                return (100 - parseInt(currentskill.efficiencyPercent))*parseInt(this.props.data[2])/100
            }else if(currentskill.efficiencyHrs != ''){
                return ''
            }else{
                return ''
            }
        }
        
    }

    dropDown(rowData) {
        return (
            <TouchableHighlight style={{ flex: 1}} >
                <View style={{ width: 200, flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center', padding: 10 }}>
                    <Text style={{fontSize: 16 }}>{rowData}</Text>
                </View>
            </TouchableHighlight>
        );
    }

    getBackground(){
        //console.log(this.props.data[0])
        return this.props.data[0].charAt(0) == 'C'? '#ffa500': this.props.data[0].charAt(0) == 'B' ? '#b0c4de': '#fdfdfd'
    }   

    render(){
        return(
            <TouchableHighlight
                underlayColor={'#eee'}
                delayLongPress={this.props.data[0].charAt(0) == 'B'? 9999999: 250} 
                style={[styles.container, {backgroundColor: this.getBackground()}]}
                onPress = {()=>{
                    //this.props.onPress(this.props.data.itemName, this.state.itemType, this.props.data.itemId)
                }} 
                {...this.props.sortHandlers}
            >
                <View style = {styles.innerContainer}>
                    <View style={{flex:1}}>
                        <Text style={styles.text}>{this.props.data[0]}</Text>
                    </View>
                    <View style={{flex:2, marginLeft: 10}}>
                        <Text style={styles.text}>{this.props.data[1]} </Text>
                    </View>
                    <View style={{flex:2, marginLeft: 10}}>
                        <Text style={styles.text}>{this.props.data[2]} </Text>
                    </View>
                    <View style={{flex:2, marginLeft: 10}}>
                        <Text style={styles.text}>{this.getPlannedHrs(this.props.data[4])}</Text>
                    </View>
                    <View style={{flex:2, marginRight: 20}}>
                        <Text style={styles.text}>{this.getSkillName(this.props.data[4])}</Text>
                    </View>
                    <View style={{flex:2, marginLeft: 35}}>
                        <Text style={styles.text}>{this.getDeptName(this.props.data[5])}</Text>
                    </View>

                     <View style={{width: 60, alignItems: 'flex-end', paddingBottom: -10}}>
                        <Ripple style={styles.RippleStyle} color='rgba(0 ,0 ,0 ,.5)' onPress={()=>{
                            this.props.onIconPress(this.props.data[0])
                        }}>
                            <Icon name="close" size={20} color="#000000" />
                        </Ripple>
                    </View>

                </View>
               
            </TouchableHighlight>
        )
    }
}

const styles = StyleSheet.create({
  container: {
      backgroundColor: '#fdfdfd', 
      height: 60, 
      padding: 10, 
      margin:5,
      borderWidth: 3, 
      borderTopWidth: 0, 
      borderColor:'#dddddd'
    },
    innerContainer: {
      //backgroundColor: [this.getBackground()],
      padding: 10, 
      flexDirection: 'row',
      paddingBottom:10
    },
    text:{
        color: 'black', 
        fontSize: 16, 
       
        textAlign:'center'
    },
    pickerBox:{
        flex:1,
        marginHorizontal: 10,
        borderColor: '#D6D6D6', 
        borderWidth: 1,
        flexDirection:'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        width: 170,
       height:35
    },
    dropdown:{
        
        flex: 1, 
        width: Dimensions.get('window').width/8,
        flexDirection: 'row', 
        justifyContent: 'space-between', 
        alignItems: 'center',
    },
    RippleStyle:{
        width: 30, 
        alignItems: 'center', 
        justifyContent: 'center', 
    }
  
});


/**
 * 
 * 
 * 
 * 
        android:launchMode="singleInstance"  ----->> For singleton in androidManifest.xml
 * 
 * 
 * <ModalDropdown
                            style={styles.pickerBox}
                            options={['checkbox', 'upload']}
                            onSelect={(idx, value) => {
                                this.props.itemType(value)
                                this.setState({
                                    itemType: value 
                                })
                            }}
                            renderRow ={this.dropDown.bind(this)}
                            >
                            <View style={styles.dropdown}>
                                <Text style={{paddingLeft:20,  color: 'black', fontSize: 16 }}>
                                    {this.state.itemType}
                                </Text>
                                <Icon name="arrow-drop-down" size={20} color="#000000" />
                            </View>
                        </ModalDropdown>
 */