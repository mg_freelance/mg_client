import React, { Component } from 'react';
import { View, ScrollView, StyleSheet, Alert, Text, Dimensions, Picker, Image, ListView, TouchableHighlight,  Animated, Color } from 'react-native';
import * as Progress from 'react-native-progress';

export default class WorkMain extends React.Component{
    constructor(){
        super()
        this.state = {
            isWorkshop: false
        }
    }

    getProgress(){
        if(this.state.isWorkshop){
            return(
                <Progress.Pie progress={Math.floor(Math.random()*10)/10} size={Dimensions.get('window').width/3} borderWidth = {3} />
            )
        }else{
            return(
                <View></View>
            )
        }
    }

    render(){
        return(
            <View style = {Styles.container}>

                <View style={Styles.jobSummary}>

                    <View style={Styles.jobSection} >
                        <View style={{padding: 10}}>
                            <Text style={Styles.titleText} >JOB INFO:</Text>
                        </View>
                        <ScrollView style={{paddingLeft: 15}}>
                            <Text style={Styles.text}>Job ID:</Text>
                            <Text style={Styles.text}>Estimate ID:</Text>
                            <Text style={Styles.text}>Claim ID:</Text>
                            <Text style={Styles.text}>BMS ID:</Text>
                            
                        </ScrollView>
                       
                    </View>

                    <View style={Styles.jobSection} >
                        <View style={{padding: 10}}>
                            <Text style={Styles.titleText} >JOB NOTES:</Text>
                        </View>
                        
                    </View>

                    <View style={[Styles.jobSection]} >
                        <View style={{padding: 10}}>
                            <Text style={Styles.titleText} >ATTACHMENTS:</Text>
                        </View>
                    </View>
                </View>

                <View style={Styles.workProgress}>
                    <ScrollView>
                        <View>
                            <TouchableHighlight onPress={()=>{}} activeOpacity={10} underlayColor = {'#7cfc00'}>    
                                <View style={Styles.ProgressSection}>
                                    <Text style={[Styles.titleText, {borderBottomWidth:0}]} >Job Buffer</Text>
                                </View>
                            </TouchableHighlight>

                            <TouchableHighlight onPress={()=>{}} activeOpacity={10} underlayColor = {'#7cfc00'}>    
                                <View style={Styles.ProgressSection}>
                                    <Text style={[Styles.titleText, {borderBottomWidth:0}]} >Pre Release</Text>
                                </View>
                            </TouchableHighlight>
                        </View>

                        <View style={Styles.ProgressWorkshop}>
                            <View style={{flex: 1, borderRightWidth:2, borderColor: '#dddddd'}}>
                                <TouchableHighlight onPress={()=>{this.setState({isWorkshop: this.state.isWorkshop? false: true})}} activeOpacity={10} underlayColor = {'#7cfc00'}  style={{flex: 1, justifyContent: 'center'}}>    
                                    <View style={[{ height: 200, alignItems: 'center', justifyContent: 'center'}]}>
                                        <Text style={[Styles.titleText, {transform: [{ rotateZ: '-90deg'}],borderBottomWidth:0, textAlign: 'center', width:200}]} >WORKSHOP</Text>
                                    </View>
                                </TouchableHighlight>
                            </View>
                            <View style={{flex: 5}}>
                                <TouchableHighlight onPress={()=>{}} activeOpacity={10} underlayColor = {'#7cfc00'}>    
                                    <View style={Styles.ProgressSection}>
                                        <Text style={[Styles.titleText, {borderBottomWidth:0}]} >Department Buffer</Text>
                                    </View>
                                </TouchableHighlight>
                                <TouchableHighlight onPress={()=>{}} activeOpacity={10} underlayColor = {'#7cfc00'}>    
                                    <View style={Styles.ProgressSection}>
                                        <Text style={[Styles.titleText, {borderBottomWidth:0}]} >Strip</Text>
                                    </View>
                                </TouchableHighlight>
                                <TouchableHighlight onPress={()=>{}} activeOpacity={10} underlayColor = {'#7cfc00'}>    
                                    <View style={Styles.ProgressSection}>
                                        <Text style={[Styles.titleText, {borderBottomWidth:0}]} >Pull Dents</Text>
                                    </View>
                                </TouchableHighlight>
                                <TouchableHighlight onPress={()=>{}} activeOpacity={10} underlayColor = {'#7cfc00'}>    
                                    <View style={Styles.ProgressSection}>
                                        <Text style={[Styles.titleText, {borderBottomWidth:0}]} >Filler</Text>
                                    </View>
                                </TouchableHighlight>
                                <TouchableHighlight onPress={()=>{}} activeOpacity={10} underlayColor = {'#7cfc00'}>    
                                    <View style={Styles.ProgressSection}>
                                        <Text style={[Styles.titleText, {borderBottomWidth:0}]} >Filler Photo Checklist</Text>
                                    </View>
                                </TouchableHighlight>
                                <TouchableHighlight onPress={()=>{}} activeOpacity={10} underlayColor = {'#7cfc00'}>    
                                    <View style={Styles.ProgressSection}>
                                        <Text style={[Styles.titleText, {borderBottomWidth:0}]} >Primer</Text>
                                    </View>
                                </TouchableHighlight>
                            </View>
                        </View>

                        <View style={[Styles.ProgressWorkshop,{paddingLeft: 60}]}>
                            <View style={{flex: 1, borderRightWidth:2, borderColor: '#dddddd'}}>
                                <TouchableHighlight onPress={()=>{this.setState({isWorkshop: this.state.isWorkshop? false: true})}} activeOpacity={10} underlayColor = {'#7cfc00'}  style={{flex: 1, justifyContent: 'center'}}>    
                                    <View style={[{height: 100, alignItems: 'center', justifyContent: 'center'}]}>
                                        <Text style={[Styles.titleText, {transform: [{ rotateZ: '-90deg'}],borderBottomWidth:0, textAlign: 'center', width:200}]} >PAINT</Text>
                                    </View>
                                </TouchableHighlight>
                            </View>
                            <View style={{flex: 5}}>
                                <TouchableHighlight onPress={()=>{}} activeOpacity={10} underlayColor = {'#7cfc00'}>    
                                    <View style={Styles.ProgressSection}>
                                        <Text style={[Styles.titleText, {borderBottomWidth:0}]} >Department Buffer</Text>
                                    </View>
                                </TouchableHighlight>
                                <TouchableHighlight onPress={()=>{}} activeOpacity={10} underlayColor = {'#7cfc00'}>    
                                    <View style={Styles.ProgressSection}>
                                        <Text style={[Styles.titleText, {borderBottomWidth:0}]} >Pre-Paint Checklist</Text>
                                    </View>
                                </TouchableHighlight>
                                <TouchableHighlight onPress={()=>{}} activeOpacity={10} underlayColor = {'#7cfc00'}>    
                                    <View style={Styles.ProgressSection}>
                                        <Text style={[Styles.titleText, {borderBottomWidth:0}]} >Paint</Text>
                                    </View>
                                </TouchableHighlight>
                                
                            </View>
                        </View>

                         <View style={Styles.ProgressWorkshop}>
                            <View style={{flex: 1, borderRightWidth:2, borderColor: '#dddddd'}}>
                                <TouchableHighlight onPress={()=>{this.setState({isWorkshop: this.state.isWorkshop? false: true})}} activeOpacity={10} underlayColor = {'#7cfc00'}  style={{flex: 1, justifyContent: 'center'}}>    
                                    <View style={[{height: 100,alignItems: 'center', justifyContent: 'center'}]}>
                                        <Text style={[Styles.titleText, {transform: [{ rotateZ: '-90deg'}],borderBottomWidth:0, textAlign: 'center', width:200}]} >WORKSHOP</Text>
                                    </View>
                                </TouchableHighlight>
                            </View>
                            <View style={{flex: 5}}>
                                <TouchableHighlight onPress={()=>{}} activeOpacity={10} underlayColor = {'#7cfc00'}>    
                                    <View style={Styles.ProgressSection}>
                                        <Text style={[Styles.titleText, {borderBottomWidth:0}]} >Polish</Text>
                                    </View>
                                </TouchableHighlight>
                                <TouchableHighlight onPress={()=>{}} activeOpacity={10} underlayColor = {'#7cfc00'}>    
                                    <View style={Styles.ProgressSection}>
                                        <Text style={[Styles.titleText, {borderBottomWidth:0}]} >Workshop Complete Checklist</Text>
                                    </View>
                                </TouchableHighlight>
                                
                            </View>
                        </View>

                        <View>
                            <TouchableHighlight onPress={()=>{}} activeOpacity={10} underlayColor = {'#7cfc00'}>    
                                <View style={Styles.ProgressSection}>
                                    <Text style={[Styles.titleText, {borderBottomWidth:0}]} >Complete Checklist</Text>
                                </View>
                            </TouchableHighlight>
                        </View>
                    </ScrollView>
                    
                </View>

                <View style={Styles.workDescripton}>
                    {this.getProgress()}
                </View>
            </View>
        );
    }
}

const Styles = StyleSheet.create({
    container: {
        backgroundColor: '#EEEEEE',
        //height: 800,
        flexDirection: 'row'
    },
    jobSummary:{
        flex:2,
        backgroundColor: '#ffffff',
        marginRight: 8
    },
    workProgress:{
        flex: 3,
        backgroundColor: '#ffffff',
        
    },
    workDescripton:{
        flex: 4,
        backgroundColor: '#ffffff',
        marginLeft: 8,
        justifyContent: 'center',
        alignItems: 'center'
    },
    jobSection:{
        flex: 1,
        borderBottomWidth: 1,
        borderColor: '#dddddd'

    },
    ProgressSection:{
        padding: 8, 
        borderBottomWidth: 2, 
        borderColor: '#dddddd'
    },
    titleText:{
        padding: 10,
        fontSize: 16,
        color: '#000000',
        borderBottomWidth: 2,
        borderColor: '#eeeeee'
    },
    text: {
        paddingBottom: 5
    },
    ProgressWorkshop:{
        flexDirection: 'row', 
        borderBottomWidth: 2, 
        borderColor:'#dddddd'
    }
})