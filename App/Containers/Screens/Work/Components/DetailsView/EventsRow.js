import React, { Component } from 'react';
import { View, Text, StyleSheet, Image, Dimensions, TouchableHighlight, TimePickerAndroid, DatePickerAndroid } from 'react-native';
import { Avatar, Drawer, Divider, PRIMARY_COLORS, TYPO, Ripple } from 'react-native-material-design';
import { Actions as NavigationActions } from 'react-native-router-flux';
import ActionButton from 'react-native-action-button';
import Icon from 'react-native-vector-icons/MaterialIcons';
import ModalDropdown from 'react-native-modal-dropdown';


export default class EventsRow extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            scheduled: [],
            inSequence: [],
            inProduction: [],
            inCompletion: []
        }
    }

    componentDidMount(){
        //console.log(this.props.data)
        this.setState({
            scheduled: this.props.data.Scheduled,
            inSequence: this.props.data.inSequence,
            inProduction: this.props.data.inProduction,
            inCompletion: this.props.data.inCompletion,
        })
    }


    formatTime(time) {
        //console.log(time.hours)
        let hour = time.hours
        let minute = time.minutes
        let timeofDay = hour<12? 'a.m':'p.m'
        return (timeofDay == 'a.m'? hour:hour-12) + ':' + (minute < 10 ? '0' + minute : minute) + ' ' + timeofDay;
    }

    getDay(day){        
        return (day.getDate() +'/'+ (day.getMonth()+1) +'/'+ day.getFullYear());
    }

    setDay(day){
        return ((day.getMonth()+1) +'/'+ day.getDate() +'/'+ day.getFullYear())
    }

    showTimePicker = async (stateKey, options) => {
        try {
            var newTime ={};
            const {action, minute, hour} = await TimePickerAndroid.open(options);
            if (action === TimePickerAndroid.timeSetAction) {
                newTime = {hours: hour, minutes: minute}
                this.state.scheduled[1] = newTime
                this.setState({
                    scheduled: this.state.scheduled
                })

            } else if (action === TimePickerAndroid.dismissedAction) {
                newTime[stateKey + 'Text'] = 'dismissed';
            }
            //console.log(newTime)
            
        } catch ({code, message}) {
            console.warn(`Error in example '${stateKey}': `, message);
        }
    };

    showDatePicker = async (stateKey, options, isStart) => {
        try {
            var newState = {};
            const {action, year, month, day} = await DatePickerAndroid.open(options);
            if (action === DatePickerAndroid.dismissedAction) {
                newState[stateKey + 'Text'] = 'dismissed';
            } else if (action === DatePickerAndroid.dateSetAction) {
                var newState = {...this.state };
                var date = new Date(year, month, day);
                this.state.scheduled[0] = this.setDay(date)
                this.setState({
                    scheduled: this.state.scheduled
                });

            }
            this.setState(newState);
        } catch ({code, message}) {
            console.warn(`Error in example '${stateKey}': `, message);
        }
    };

   

    render() {
       //console.log(this.state.inSequence)
        return (
            <View style={styles.container}>
                <View style = {{flex: 1}}>
                    <Text style ={styles.text}>{this.props.data.EventName}</Text>
                </View>

                <View style={{flex: 1, flexDirection: 'row', paddingHorizontal: 15, borderLeftColor: '#D6D6D6', borderLeftWidth: 2}}> 
                     <Ripple text=""  style={styles.rippleStyle} color='rgba(255,255,255,.2)' onPress={ () => {
                            this.showDatePicker('scheduledDate', {
                                            date: this.state.scheduled != [] ? new Date(this.state.scheduled[0]): new Date()
                                        }, false);
                     }} >                     
                        <Text style = {[styles.text]} >
                            {this.getDay(new Date(this.state.scheduled[0]))}
                        </Text>
                        <Icon name="arrow-drop-down" size={20} color="#000000" />
                    </Ripple>
                </View>

                <View style={{flex: 1, flexDirection: 'row', paddingHorizontal: 15, borderLeftColor: '#D6D6D6', borderLeftWidth: 2}}> 
                     <Ripple text=""  style={styles.rippleStyle} color='rgba(255,255,255,.2)' onPress={ () => { }} >                     
                        <Text style = {[styles.text]} >
                            {this.state.inSequence[0] == null ? 'N/A' :this.getDay(new Date(this.state.inSequence[0]))}
                        </Text>
                    </Ripple>
                </View>

                <View style={{flex: 1, flexDirection: 'row', paddingHorizontal: 15, borderLeftColor: '#D6D6D6', borderLeftWidth: 2}}> 
                     <Ripple text=""  style={styles.rippleStyle} color='rgba(255,255,255,.2)' onPress={ () => {}} >                     
                        <Text style = {[styles.text]} >
                            {this.state.inProduction[0] == null ? 'N/A' :this.getDay(new Date(this.state.inProduction[0]))}
                        </Text>
                    </Ripple>
                </View>
                        
                <View style={{flex: 1, flexDirection: 'row', paddingHorizontal: 15, borderLeftColor: '#D6D6D6', borderLeftWidth: 2}}> 
                     <Ripple text=""  style={styles.rippleStyle} color='rgba(255,255,255,.2)' onPress={ () => {}} >                     
                        <Text style = {[styles.text]} >
                            {this.state.inCompletion[0] == null ? 'N/A' :this.getDay(new Date(this.state.inCompletion[0]))}
                        </Text>
                    </Ripple>
                </View>                      
            </View>
        );
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        margin: 5,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        backgroundColor: 'white',
        borderWidth: 2,
        borderColor: '#D6D6D6',
        height: 60,
        borderTopWidth: 0
    },
    text: {
        marginLeft: 15,
        marginTop: 0,
        fontSize: 16,
    },
    photo: {
        height: 60,
        width: 60,
        borderRadius: 60,
    },
    rippleContainer: {
        flex: 1,
        flexDirection: 'row',
        height: 80,
        padding: 12,
    },
    pickerBox:{
        flex:1,
        marginLeft: 60,
        marginVertical: 10,
        borderColor: '#D6D6D6', 
        borderWidth: 1,
        flexDirection:'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        width: 170,
        
    },
    dropdown:{        
        flex: 1, 
        width: Dimensions.get('window').width/8,
        flexDirection: 'row', 
        justifyContent: 'space-between', 
        alignItems: 'center',
    },
    rippleStyle:{
        flex:1, 
        flexDirection: 'row',
        justifyContent: 'center'
    },
});



/**
 * 
 * 
 * 
 * 
 */