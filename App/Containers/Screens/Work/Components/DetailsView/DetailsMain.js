import React, { Component } from 'react';
import { View, StyleSheet, ScrollView, Text, TextInput, ListView } from 'react-native';
import EventsRow from './EventsRow'

const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });

export default class DetailsMain extends Component {

    constructor(){
        super()        
        this.state = {
            note: '',
            events: [],
            dataSource:  ds.cloneWithRows([]),
        }
    }

    componentDidMount(){
        //const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
        this.state.events = this.props.events
        this.setState({
            events: this.props.events,
            dataSource:  ds.cloneWithRows(this.state.events),
        })
    }

    render(){
        return (
            <View style= {styles.container}>
                <View style= {{flex: 3, flexDirection: 'row'}}>
                    <View style={[styles.jobSection, {marginRight: 0}]} >
                            <View style={{padding: 10}}>
                                <Text style={styles.titleText} >JOB INFO:</Text>
                            </View>
                            <ScrollView style={{paddingLeft: 15, paddingTop: 15}}>
                                <View style={{flexDirection: 'row'}}>
                                    <Text style={styles.text}>Job ID:</Text>
                                    <Text style={styles.text}></Text>
                                </View>

                                <View style={{flexDirection: 'row'}}>
                                    <Text style={styles.text}>Estimate ID:</Text>
                                    <Text style={styles.text}></Text>
                                </View>

                                <View style={{flexDirection: 'row'}}>
                                    <Text style={styles.text}>Claim ID:</Text>
                                    <Text style={styles.text}></Text>
                                </View>

                                <View style={{flexDirection: 'row'}}>
                                    <Text style={styles.text}>BMS ID:</Text>
                                    <Text style={styles.text}></Text>
                                </View>
                                
                            </ScrollView>
                        
                    </View>
                    <View style={[styles.jobSection, {marginLeft: 0}]} >
                            <View style={{padding: 10}}>
                                <Text style={styles.titleText} >NOTES:</Text>
                            </View>
                            <View style={{flex: 1, padding: 15}}>
                                <TextInput
                                    style={{flex: 1, borderColor: 'gray', borderWidth: 1, textAlignVertical: 'top', padding: 10}}
                                    onChangeText={(text) => this.setState({note: text})}
                                    value={this.state.note}
                                    multiline= {true}
                                    underlineColorAndroid = 'transparent'
                                />
                            </View>
                        
                    </View>
                </View>
                <View style={{flex: 2}}>
                    <View style = {{padding: 20, paddingLeft:5, flexDirection: 'row',paddingBottom:10, borderBottomWidth: 2, borderBottomColor: '#D6D6D6', backgroundColor: '#F6F6F6'}}>
                         <View style={{flex:1}}>
                            <Text style={styles.tableTitle}>Event</Text>
                        </View>
                        <View style={{flex:1}}>
                            <Text style={styles.tableTitle}>Scheduled</Text>
                        </View>
                         <View style={{flex:1}}>
                            <Text style={styles.tableTitle}>In Sequence</Text>
                        </View>
                         <View style={{flex:1}}>
                            <Text style={styles.tableTitle}>In Production</Text>
                        </View>
                         <View style={{flex:1}}>
                            <Text style={styles.tableTitle}>In completion</Text>
                        </View>
                    </View>

                    <ScrollView style={styles.scrollstyle} >
                        <ListView style={{ backgroundColor: 'transparent' }}
                            dataSource={this.state.dataSource}
                            enableEmptySections={true}
                            renderRow={
                                (rowData) => {
                                    return (
                                    <EventsRow data = {rowData} />
                                    ) 
                                }   
                            }
                            //renderSeparator={(sectionId, rowId) => <View key={rowId} style={styles.separator} />}
                            >   
                        </ListView>
                    </ScrollView>
                </View>                

            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: '#ffffff',
        //alignItems: 'center',
    },
    jobSection:{
        flex: 1,
        borderBottomWidth: 1,
        borderColor: '#dddddd',
        margin: 10,
    },
    titleText:{
        padding: 10,
        fontSize: 16,
        color: '#000000',
        borderBottomWidth: 2,
        borderColor: '#eeeeee'
    },
    text: {
        paddingBottom: 5
    },
    scrollstyle:{
        flex: 1,
        backgroundColor: '#F6F6F6',
    },
    tableTitle: { 
        paddingBottom: 10,  
        color: 'black', 
        fontSize: 16,
        textAlign: 'center'
    }

    
});