import React, { Component } from 'react';
import { TouchableOpacity, DatePickerAndroid, Alert, View, Text, StyleSheet, Image, ScrollView, TextInput, Switch, Picker, ListView, Dimensions} from 'react-native';
import dismissKeyboard from 'react-native-dismiss-keyboard'
import { connect } from 'react-redux'
import LoginActions from '../../../Redux/LoginRedux'
import { Avatar, Drawer, Divider, PRIMARY_COLORS, TYPO, Ripple } from 'react-native-material-design';
import TextField from 'react-native-md-textinput';
import Button from '../../../Components/Button'
import Icon from 'react-native-vector-icons/Ionicons';
import SelectionModal from '../Setup/User/Modals/SelectionModal';
import ImagePicker from 'react-native-image-picker';
import {api} from '../../../Lib/helpers';

 const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });

//const setUserImage = (userId, Imgtype, data) => api.post('upload', {userID: userId, ImageType: Imgtype, ImageData: data})


type UserViewProps = {
    data: Object,
    authToken: string,
    changeURL: () => void
}

export class ProfileScreen extends Component {
    props: UserViewProps;
    constructor(props) {
        super(props);
        this.state = {
            userData: {},
            roleModalList: [],
            rawSkillList: [],
            rawRoleList: [],
            qualificationDS: ds.cloneWithRows([]),
            skillsDS: ds.cloneWithRows([]),
            rolesDS: ds.cloneWithRows([]),
        }
    }

    changeAvatar(file){
        let image = new FormData()
        image.append("file",file)
        //console.log(image)
        api().post('upload', image)
        .then((response)=>{
            console.log(response)
            this.setState({

            })
        })
    }

    getDirection(){
        return Dimensions.get('window').width< 500 ? 'column' : 'row'
    }

    componentDidMount(){

        api().get('user').then((response) => {
            //console.log(response);
           let UserData = response.data.find((user)=>{
              
               return user.authToken == this.props.authToken ? user: null;
           })
           //console.log(this.props)
           this.setState({
                userData: UserData,
                authToken: this.props.authToken,
                isQModelOpen: false,
                isSModelOpen: false,
                isRModelOpen: false,
                
                
                qualificationDS: ds.cloneWithRows(UserData.qualifications),
                skillsDS: ds.cloneWithRows(UserData.skill),
                rolesDS: ds.cloneWithRows(UserData.roles),
            });
        //     console.log('************************DATASOURCES*********')
        });
    }

    toggleModal(modalName, openStatus) {
        this.state[modalName] = openStatus;
        this.updateState();
    }

    getRoleList() {
        var $promise = {
            then: (func) => {
                $promise.callback = func;
            }
        };
        api().get('role').then((response) => {
            var modelData = [];
            this.state.rawRoleList = response.data;
            for (var key in response.data) {
                modelData.push({ id: response.data[key].roleId, value: response.data[key].roleName });
            }
            $promise.callback(modelData);
        });
        return $promise;
    }

    getSkillList() {
        var $promise = {
            then: (func) => {
                $promise.callback = func;
            }
        };
        api().get('skill').then((response) => {
            var modelData = [];
            this.state.rawSkillList = response.data;
            for (var key in response.data) {
                modelData.push({ id: response.data[key].skillId, value: response.data[key].skillName });
            }
            $promise.callback(modelData);
        });
        return $promise;
    }

     updateState() {
        const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
        this.setState({
            ...this.state,
            qualificationDS: ds.cloneWithRows(this.state.userData.qualifications),
            skillsDS: ds.cloneWithRows(this.state.userData.skill),
            rolesDS: ds.cloneWithRows(this.state.userData.roles),
        });
    }

    setImage(){

        const options = {
            quality: 1.0,
            maxWidth: 500,
            maxHeight: 500,
            storageOptions: {
                skipBackup: true
            }
            };
            
            ImagePicker.showImagePicker(options, (response) => {

            if (response.didCancel) {
                console.log('User cancelled photo picker');
            }
            else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            }
            else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            }
            else { 
                //console.log('content:/'+response.path)
                //this.changeAvatar('content:/'+response.path)
                this.state.userData.profileImg = response.uri
                this.props.changeURL(this.state.userData.username, this.props.authToken, this.state.userData.profileImg)
                this.setState({
                    userData: this.state.userData
                })

            }
        });
    
    }

    showPicker = async (stateKey, options) => {
        try {
            var newState = {};
            const {action, year, month, day} = await DatePickerAndroid.open(options);
            if (action === DatePickerAndroid.dismissedAction) {
                newState[stateKey + 'Text'] = 'dismissed';
            } else {
                var newState = {...this.state };

                var date = new Date(year, month, day);
                newState.userData[stateKey] = date.toLocaleDateString();
                this.setState(newState);

            }
            this.setState(newState);
        } catch ({code, message}) {
            console.warn(`Error in example '${stateKey}': `, message);
        }
    };



    render(){
        return (
            <View style={{ flex: 1 }} >
                <ScrollView style={styles.parent}>

                    <View style={[styles.topContainer, { flexDirection: this.getDirection() }]}>

                        <View style={{ flexDirection: 'column', alignItems: 'center', justifyContent: 'center', paddingBottom: -10 }}>
                            <Image source={{ uri: this.state.userData.profileImg }} style={styles.photo} />
                            <Icon name = {'ios-camera'} size={55} color='#dddddd' style={{ position: 'absolute', bottom: 20, right:40}} onPress= {()=>{
                                this.setImage()
                            }}/>
                        </View>

                        <View style={{ flex: 1, flexDirection: 'column', padding: 50, height: 200, backgroundColor: 'white' }}>
                            {(!this.props.create) ? (<View>
                                <TextField
                                    label={'User ID'}
                                    highlightColor={'#00BCD4'}
                                    value={this.state.userData.userId}
                                    dense={true}
                                    editable={false}

                                    />
                            </View>) : null}
                            <View style={{ flexDirection: 'row' }}>
                                <View style={{ flex: 1 }}>
                                    <TextField
                                        label={'First Name'}
                                        highlightColor={'#00BCD4'}
                                        value={this.state.userData.firstName}
                                        dense={true}
                                        editable={false}
                                    />
                                </View>
                                <View style={{ flex: 1, marginLeft: 50 }}>
                                    <TextField
                                        label={'Surname'}
                                        highlightColor={'#00BCD4'}
                                        value={this.state.userData.surName}
                                        dense={true}
                                        editable={false}
                                    />
                            </View>
                            </View>
                        </View>
                    </View>
                    <View style={[styles.topContainer, { marginTop: 20, flexDirection: this.getDirection() }]}>
                        <View style={{ paddingRight: 0 }}>
                            <Text style={{ fontSize: 16, fontWeight: 'bold', margin: 30, paddingRight: 20, color: 'black' }}> Login Details </Text>
                        </View>
                        <View style={{ flex: 1, flexDirection: 'column', padding: 30, paddingLeft: 15, backgroundColor: 'white' }}>
                            <View style={{ flexDirection: 'row' }}>
                                <ScrollView style={{ flex: 1 }}>
                                    <TextField
                                        label={'Username'}
                                        highlightColor={'#00BCD4'}
                                        value={this.state.userData.username}
                                        editable={false}
                                        dense={true}
                                    />
                            </ScrollView>
                                <ScrollView style={{ flex: 1, marginLeft: 50 }}>
                                    <TextField
                                        label={'Password'}
                                        highlightColor={'#00BCD4'}
                                        secureTextEntry={true}
                                        value={this.state.userData.password}
                                        editable={false}
                                        dense={true}
                                    />
                                </ScrollView>
                            </View>
                            <View style={{ flexDirection: 'row', marginTop: 20 }}>
                                <View style={{ flex: 1, flexDirection: 'column', alignItems: 'flex-start' }}>
                                    <Text style={{ color: 'black', margin: 15, marginLeft: 0 }}>Active</Text>
                                    <Switch style={{ margin: 10, marginLeft: 0 }} value={this.state.userData.active}></Switch>
                            </View>
                            <View style={{ flex: 1, flexDirection: 'column', alignItems: 'flex-start' }}>
                                <Text style={{ color: 'black', margin: 15, marginLeft: 0 }}>Temporary Staff</Text>
                                <Switch style={{ margin: 10, marginLeft: 0 }} value={this.state.userData.temporaryStaff}></Switch>
                        </View>
                    </View>
                    <View style={{ flexDirection: 'row', marginTop: 20 }}>

                        <ScrollView style={{ flex: 1 }}>
                            <TouchableOpacity style={{ flex: 1 }} onPressIn={() => { dismissKeyboard(); } } onPressOut={() => { dismissKeyboard(); } } onPress={() => { dismissKeyboard(); } }>
                                <TextField
                                    label={'Contract Start Date'}
                                    highlightColor={'#00BCD4'}
                                    ref="contractStartDate"
                                    value={this.state.userData.contractStartDate}
                                    editable={false}
                                    dense={true}
                                />


                            </TouchableOpacity>
                        </ScrollView>

                        <ScrollView style={{ flex: 1, marginLeft: 50 }}>
                            <TextField
                                label={'Contract End Date'}
                                highlightColor={'#00BCD4'}
                                value={this.state.userData.contractEndDate}
                                editable={false}
                                dense={true}
                            />
                            </ScrollView>
                    </View>
                    <View style={{ flexDirection: 'row', marginTop: 20 }}>
                        <ScrollView style={{ flex: 1 }}>
                            <Text style={{ color: 'black', fontSize: 14, marginTop: 15 }}>User Level</Text>
                            <Picker style={{ height: 25 }} selectedValue={this.state.userData.userLevel} enabled = {false}>
                                <Picker.Item label="Technician" value="technician" />
                                <Picker.Item label="Floater" value="floater" />
                            </Picker>
                        <Divider></Divider>
                        </ScrollView>
                    <ScrollView style={{ flex: 1, marginLeft: 50 }}>
                        <Text style={{ color: 'black', fontSize: 14, marginTop: 15 }}>Associated Entity</Text>
                        <Picker style={{ height: 25 }} selectedValue={this.state.userData.associatedEntity} enabled={false} >
                                <Picker.Item label="Technician" value="Technician" />
                                <Picker.Item label="Floater" value="Floater" />
                            </Picker>
                    <Divider></Divider>
                </ScrollView>
            </View >
                        </View >
            </View >
            <View style={[styles.topContainer, { marginTop: 20, flexDirection: this.getDirection() }]}>
                <View style={{ paddingRight: 0 }}>
                    <Text style={{ fontSize: 16, fontWeight: 'bold', margin: 30, color: 'black' }}> Qualifications </Text>
                </View>
                <View style={{ flex: 1, flexDirection: 'column', padding: 30, backgroundColor: 'white' }}>
                    <ListView style={{ backgroundColor: 'white' }}
                        dataSource={this.state.qualificationDS}
                        enableEmptySections={true}
                        renderRow={(rowData) => {
                            return (
                                <View style={{ flex: 1, flexDirection: 'row', marginTop: 5 }}>
                                    <Text style={{ flex: 1 }}>{rowData.qualificationDescription}</Text>
                                </View>)
                        } }
                        >
                    </ListView>
                </View>

            </View>
            <View style={[styles.topContainer, { marginTop: 20, flexDirection: this.getDirection() }]}>
                <View style={{ paddingRight: 0 }}>
                    <Text style={{ fontSize: 16, fontWeight: 'bold', margin: 30, paddingRight: 60, color: 'black' }}> Skills </Text>
                </View>
                <View style={{ flex: 1, flexDirection: 'column', padding: 30, backgroundColor: 'white' }}>
                    <View style={{ flex: 1, flexDirection: 'row' }}>
                        <Text style={{ flex: 2 }}>Skill ID</Text>
                        <Text style={{ flex: 3 }}>Skill Name </Text>
                        <Text style={{ flex: 4 }}>Skill Description</Text>
                        
                    </View>
                    <ListView style={{ backgroundColor: 'white' }}
                        dataSource={this.state.skillsDS}
                        enableEmptySections={true}
                        renderRow={(rowData) => {
                            return (
                                <View style={{ flex: 1, flexDirection: 'row', marginTop: 15 }}>
                                    <Text style={{ flex: 2 }}>{rowData.skillId}</Text>
                                    <Text style={{ flex: 3 }}>{rowData.skillName} </Text>
                                    <Text style={{ flex: 4 }}>{rowData.skillDescription}</Text>
                                </View>)
                        } }
                        >
                    </ListView>
                </View>
            </View>
            <View style={[styles.topContainer, { marginTop: 20, flexDirection: this.getDirection() }]}>
                <View style={{ paddingRight: 0 }}>
                    <Text style={{ fontSize: 16, fontWeight: 'bold', margin: 30, paddingRight: 60, color: 'black' }}> Roles </Text>
                </View>
                <View style={{ flex: 1, flexDirection: 'column', padding: 30, backgroundColor: 'white' }}>
                    <View style={{ flex: 1, flexDirection: 'row' }}>
                        <Text style={{ flex: 2 }}>Role ID</Text>
                        <Text style={{ flex: 3 }}>Role Name </Text>
                        <Text style={{ flex: 4 }}>Role Description</Text>
                    </View>
                    <ListView style={{ backgroundColor: 'white' }}
                        dataSource={this.state.rolesDS}
                        enableEmptySections={true}
                        renderRow={(rowData) => {
                            return (
                                <View style={{ flex: 1, flexDirection: 'row', marginTop: 15 }}>
                                    <Text style={{ flex: 2 }}>{rowData.roleId}</Text>
                                    <Text style={{ flex: 3 }}>{rowData.roleName} </Text>
                                    <Text style={{ flex: 4 }}>{rowData.roleDescription}</Text>
                                </View>)
                        } }
                        >
                    </ListView>
                </View>
            </View>
            <View style={{ marginTop: 50, height: 1 }}>
            </View>

                </ScrollView >
           
            </View >
        );
    }
}

const mapStateToProps = (state) => {
    return {
        authToken: state.login.authToken,
    }
}
 
const mapDispachtoProps = (dispatch) => {
   return {
       changeURL: (username, token, ImageURL) => dispatch(LoginActions.loginSuccess(username, token, ImageURL))
   }
}

export default connect(mapStateToProps, mapDispachtoProps)(ProfileScreen)

const styles = StyleSheet.create({
    parent: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: '#eeeeee',
        margin: 0,
        marginTop: 50,
        padding: 10,
    },
    topContainer: {
        flex: 1,
        flexDirection: 'row',
        //flexWrap: 'wrap',
        backgroundColor: 'white',
        borderRadius: 2,
        elevation: 2
    },

    photo: {
        flex: 1,
        margin: 40,
        height: 160,
        width: 160,
        borderRadius: 180,
    },
    separator: {
        marginTop: 10,
        height: 1,
        backgroundColor: '#8E8E8E',
    },
    profileName: {
        marginTop: 10,
        fontSize: 30,
        color: 'black',
        fontWeight: '400'
    },
    profileDesignation: {
        fontSize: 25,
        color: 'black',
        fontWeight: '400'
    }
});