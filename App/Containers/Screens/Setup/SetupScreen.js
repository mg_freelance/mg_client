import React, { Component } from 'react';
import { View, StyleSheet, Alert } from 'react-native';
import { TabViewAnimated, TabBarTop } from 'react-native-tab-view';
import UserList from './User/UserList';
import SkillList from './Skill/SkillList';
import RoleList from './Role/RoleList';
import DepartmentList from './Department/DepartmentList'
import WorkflowList from './Workflow/WorkflowList'
import ChecklistList from './Checklist/ChecklistList'
import { Colors, Metrics } from '../../../Themes'
import REDUX_PERSIST from '../../../Config/ReduxPersist'
export default class TabViewExample extends Component {
  state = {
    index: 0,
    routes: [
      { key: '1', title: 'Users' },
      { key: '2', title: 'Skills' },
      { key: '3', title: 'Roles' },
      { key: '4', title: 'Department'},
      { key: '5', title: 'Workflow' },
      { key: '6', title: 'Checklist' },
    ],
  };

  _handleChangeTab = (index) => {
    this.setState({ index });
  };

  _renderHeader = (props) => {
    return <TabBarTop {...props}
     indicatorStyle={styles.indicator}
        style={styles.tabbar}
        labelStyle={styles.label}
     />;
  };

  _renderScene = ({ route }) => {
    switch (route.key) {
    case '1':
      return (<UserList />);
    case '2':
      return <SkillList />;
    case '3':
      return <RoleList />;
    case '4':
      return <DepartmentList/>
    case '5':
      return <WorkflowList/>
    case '6':
      return <ChecklistList/>
    default:
      return null;
    }
  };

  render() {
    return (
      
      <TabViewAnimated
        style={styles.container}
        navigationState={this.state}
        renderScene={this._renderScene}
        renderHeader={this._renderHeader}
        onRequestChangeTab={this._handleChangeTab}
      />
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop:60,
    backgroundColor: Colors.background
  },
  page: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  tabbar: {
    backgroundColor:  Colors.background,
  },
  indicator: {
    backgroundColor: '#408fff',
  },
  label: {
    color: '#222',
    fontWeight: '400',
  },
});