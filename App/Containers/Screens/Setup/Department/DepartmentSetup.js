import React, { Component } from 'react';
import { View, StyleSheet, Dimensions, Text, TouchableHighlight, TextInput, Alert } from 'react-native';
import { TabViewAnimated, TabBarTop } from 'react-native-tab-view';
import {Colors} from '../../../../Themes'
import ModalDropdown from 'react-native-modal-dropdown';
import Icon from 'react-native-vector-icons/MaterialIcons';
import SetupView from './tabViews/SetupView';
import BufferView from './tabViews/BufferView';
import CheckpointsView from './tabViews/CheckpointsView'
import ChecklistModel from './tabViews/ChecklistModel'
import { Actions as NavigationActions } from 'react-native-router-flux'
import R from 'ramda';
import {api} from '../../../../Lib/helpers';

let setupData = {}
let checkpointData = {}

export default class DepartmentSetup extends Component {
    
    state = {
        index: 0,
        routes: [
            { key: '1', title: 'setup' },
            { key: '2', title: 'Checkpoints' },
            ],
        workshop: 'Select',
        DepartmentName: '',
        setupdata: [],
        checklistData: [],
        checkpoints: {1: [ 'CHK001', 'Clocking Off'],
                      2: [ 'CHK002', 'Clocking On' ]                                  
                                                    },
        checkpointOrder:['1', '2'],
        isOpen: false
    };

    componentDidMount(){
       api().get('checklists').then((response) => {
                //console.log(response.data)
                this.setState({
                    checklistData: response.data
                })
        });
    }

    componentWillMount(){
         
         

         if(!this.props.create){
            api().get('department/'+ this.props.id).then((response) => {
                //console.log(response.data)
                this.setState({
                    DepartmentName: response.data[0].departmentName,
                    setupdata: response.data[0].setup,
                    checkpoints: response.data[0].checkpoints,
                    checkpointOrder: response.data[0].checkpointsOrder
                });
            });
        }

        this.props.setActionHandlers('confirm', () => {
            //console.log(checkpointData)
            if(this.state.DepartmentName == ''){
                Alert.alert(
                    'Error',
                    'Checklist Name cannot be blank'
                 );
            }else{
                let departmentData = {
                    departmentName: this.state.DepartmentName, 
                    setup: setupData, 
                    checkpoints: this.state.checkpoints,
                    checkpointOrder: this.state.checkpointOrder
                }

                 if(this.props.create){
                    //console.log(JSON.stringify(departmentData))
                    api().post('department', JSON.stringify(departmentData))
                    .then((response) => {
                            //console.log(response.data)
                            Alert.alert(
                                'Success',
                                'Saved Successfully',
                                [
                                    { text: 'OK', onPress: () => NavigationActions.pop() }
                                ]);
                        });

                }else{
                    api().put('department/' + this.props.id, departmentData)
                    .then((response) => {
                        Alert.alert(
                            'Success',
                            'User Updated Successfully',
                            [
                                {
                                    text: 'OK', onPress: () => {
                                        NavigationActions.pop();
                                    }
                                },
                            ]
                        );
                    });

                }
            }
        });
        this.props.setActionHandlers('delete', () => {
            Alert.alert(
                'Delete',
                'Are you sure you want to Delete?',
                [
                    { text: 'Cancel', onPress: () => { } },
                    {
                        text: 'OK', onPress: () => {
                            NavigationActions.pop();
                        }
                    },
                ]
            )
        });
    }

    _handleChangeTab = (index) => {
        this.setState({ index });
    };

    _renderHeader = (props) => {
        return <TabBarTop {...props}
        indicatorStyle={styles.indicator}
            style={styles.tabbar}
            labelStyle={styles.label}
        />;
    };

    toggleModal(openStatus) {
        this.setState({
         
          isOpen: openStatus,
        });
    }

    _renderScene = ({ route }) => {
        switch (route.key) {
        case '1':
        return <SetupView data={this.state.setupdata} setupData= {(data)=>{setupData = data}}/>
        case '2':
        return <CheckpointsView 
                    
                    modelState = {(isOpen)=>{this.toggleModal(isOpen)}}
                    checklistData = {this.state.checklistData}
                    data = {[this.state.checkpoints, this.state.checkpointOrder]}
                    checkpoints = {(data, order)=>{
                        checkpointData = {data, order}
                    }} 
                    onDelete = {(list, order)=>{
                        //console.log(order)
                        this.setState({
                            checkpoints: list,
                            checkpointOrder: order
                        })
                        //console.log(this.state.checkpointOrder)
                    }}
                />
        default:
        return null;
        }
    };
    

    dropDown(rowData) {
        return (
            <TouchableHighlight style={{ flex: 1}} >
                <View style={{ width: 200, flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center', padding: 10 }}>
                    <Text style={{fontSize: 16 }}>{rowData}</Text>
                </View>
            </TouchableHighlight>
        );
    }

    render() {
       //console.log(this.state.checkpoints)
        return (
            <View style = {{flex: 1}}>
                <View style = {{flexDirection: 'row',paddingBottom:10, marginTop: 65, borderBottomWidth: 2, borderBottomColor: '#D6D6D6'}}>
                    <Text style={{paddingLeft:20, paddingTop: 10,  color: 'black', fontSize: 16 }}>Department Name: </Text>
                     <TextInput
                        style={{marginLeft:20, height: 40, width: 200, borderColor: 'gray', borderWidth: 1, textAlign: 'center'}}
                        onChangeText={(DepartmentName) => this.setState({DepartmentName})}
                        value={this.state.DepartmentName}
                        underlineColorAndroid = 'transparent'
                    />
                </View>

                <TabViewAnimated
                    style={styles.container}
                    navigationState={this.state}
                    renderScene={this._renderScene}
                    renderHeader={this._renderHeader}
                    onRequestChangeTab={this._handleChangeTab}
                />


                <ChecklistModel
                    //width = {3*Dimensions.get('window').width/4}
                    //modelType={'Approve'}
                    title={''}
                    Checklists = {this.state.checklistData}
                    isOpen={this.state.isOpen}
                    onClosed={() => { this.toggleModal(false) } }
                    onApply={(listData)=>{
                            let lastKey = !R.isEmpty(this.state.checkpoints) ? Math.max(...this.state.checkpointOrder.map(Number)) :0 
                            this.state.checkpoints[lastKey+1] = listData
                            this.state.checkpointOrder.push(''+(lastKey+1))
                            this.toggleModal(false)
                        
                    }} 
                />
            </View>
      
        );
    }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ffffff',
    marginTop: 10
  },
  page: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  tabbar: {
    backgroundColor:  '#fefefe',
  },
  indicator: {
    backgroundColor: '#408fff',
  },
  label: {
    color: '#222',
    fontWeight: '400',
  },
  pickerBox:{
        height: 50,
        marginHorizontal: 10,
        borderColor: '#D6D6D6', 
        width :200,
        borderWidth: 1,
        flexDirection:'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    dropdown:{
        marginLeft: 5, 
        flex: 1, 
        width:Dimensions.get('window').width/8,
        flexDirection: 'row', 
        justifyContent: 'space-between', 
        alignItems: 'center',
    }
});