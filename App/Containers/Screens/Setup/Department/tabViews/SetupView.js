import React, { Component } from 'react';
import { View, StyleSheet, Dimensions, Text, TextInput, Switch } from 'react-native';

export default class SetupView extends Component {
    constructor(){
        super()
        this.state = {
            DeptBufferTime: '',
            DeptBufferPercent: '',
            otherBufferTime: '',
            otherBufferPercent: '',
            isProduction: false,
            isOutwork: false,
        }
    }

    componentWillReceiveProps(newProps){
        
        if(newProps.data != this.props.data){
            //console.log(newProps.data)
           this.setState({
               DeptBufferTime: newProps.data.deptBufferTime,
               DeptBufferPercent: newProps.data.DeptBufferPercent,
               otherBufferTime: newProps.data.otherBufferTime,
               otherBufferPercent: newProps.data.otherBufferPercent,
               isProduction: newProps.data.isProduction,
               isOutwork: newProps.data.isOutwork
           });
        }
    }
    
    render(){
        //console.log(this.state)
        this.props.setupData(this.state)
        return (
            <View style= {styles.container}>

                <View style = {{flexDirection: 'row', marginTop: 30, paddingLeft: 20}}>
                    <Text style={[styles.text,{paddingRight: 40}]}>Production Department ?  </Text>
                   <Switch
                        onValueChange={(value) =>{ this.setState({isProduction: value})}}
                        style={{paddingTop: 15, paddingLeft: 10}}
                        value={this.state.isProduction} />
                </View>

                <View style = {{flexDirection: 'row', marginTop: 30, paddingLeft: 20}}>
                    <Text style={[styles.text,{paddingRight: 60}]}>Outwork Department ?  </Text>
                   <Switch
                        onValueChange={(value) =>{ this.setState({isOutwork: value})}}
                        style={{paddingTop: 15, paddingLeft: 10}}
                        value={this.state.isOutwork} />
                </View>

                <View style = {{flexDirection: 'row', marginTop: 60, paddingLeft: 20}}>
                    <Text style={[styles.text,{paddingRight: 60}]}>Buffers :</Text>                    
                </View>

                
                <View style = {{flexDirection: 'row', marginTop: 30, paddingLeft: 20}}>
                    <View style ={{flex: 1}}>
                        <Text style={[styles.text,{paddingRight: 60}]}>Department Buffer :</Text>
                    </View>
                    <View style={{flex: 2, flexDirection: 'row'}}>
                        <Text style={[styles.text]}>Fixed time : </Text>
                        <TextInput
                            style={{height: 40, width: 100, borderColor: 'gray', borderWidth: 1, textAlign: 'center'}}
                            onChangeText={(time) => this.setState({
                                DeptBufferTime: time,
                                DeptBufferPercent: ''
                            })}
                            value={this.state.DeptBufferTime}
                            underlineColorAndroid = 'transparent'
                            keyboardType = 'numeric'
                        />
                        <Text style={[styles.text,{paddingLeft: 40}]}>  OR </Text>
                        <Text style={[styles.text,{paddingLeft: 40}]}>% of Dept. Efficiency : </Text>
                        <TextInput
                            style={{height: 40, width: 100, borderColor: 'gray', borderWidth: 1, textAlign: 'center'}}
                            onChangeText={(time) => this.setState({
                                DeptBufferTime: '',
                                DeptBufferPercent: time
                            })}
                            value={this.state.DeptBufferPercent}
                            underlineColorAndroid = 'transparent'
                            keyboardType =  'numeric'
                        />
                    </View>               
                </View>

                <View style = {{flexDirection: 'row', marginTop: 30, paddingLeft: 20}}>
                    <View style ={{flex: 1}}>
                        <Text style={[styles.text,{paddingRight: 60}]}>Inside another Department Buffer :</Text>
                    </View>

                    <View style={{flex: 2, flexDirection: 'row'}}>
                        <Text style={[styles.text]}>Fixed time : </Text>
                        <TextInput
                            style={{height: 40, width: 100, borderColor: 'gray', borderWidth: 1, textAlign: 'center'}}
                            onChangeText={(time) => this.setState({
                                otherBufferTime: time,
                                otherBufferPercent: ''
                            })}
                            value={this.state.otherBufferTime}
                            underlineColorAndroid = 'transparent'
                            keyboardType = 'numeric'
                        />
                        <Text style={[styles.text,{paddingLeft: 40}]}>  OR </Text>
                        <Text style={[styles.text,{paddingLeft: 40}]}>% of Dept. Efficiency : </Text>
                        <TextInput
                            style={{height: 40, width: 100, borderColor: 'gray', borderWidth: 1, textAlign: 'center'}}
                            onChangeText={(time) => this.setState({
                                otherBufferTime: '',
                                otherBufferPercent: time
                            })}
                            value={this.state.otherBufferPercent}
                            underlineColorAndroid = 'transparent'
                            keyboardType =  'numeric'
                        />
                    </View>                        
                </View>
                

            </View>
        );
    }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ffffff',
  },
  tabbar: {
    backgroundColor:  '#efefef',
  },
  label: {
    color: '#222',
    fontWeight: '400',
  },
  pickerBox:{
        height: 50,
        marginHorizontal: 10,
        borderColor: '#D6D6D6', 
        width :200,
        borderWidth: 1,
        flexDirection:'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    text:{
        paddingLeft:10, 
        paddingTop: 8,  
        color: 'black', 
        fontSize: 16, 
        paddingRight: 10
    }
});