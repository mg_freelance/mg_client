// @flow
import React, { Component } from 'react';
import { Colors, Metrics } from '../../../../../Themes'
import {
    StyleSheet,
    ListView,
    Text,
    ScrollView,
    View,
    Dimensions,
    TextInput,
    Image,
    TouchableHighlight,
    DatePickerAndroid,
    TouchableOpacity
} from 'react-native';
import { Avatar, Checkbox, Drawer, Divider, PRIMARY_COLORS, TYPO, Ripple } from 'react-native-material-design';
import ActionButton from 'react-native-action-button';
import Icon from 'react-native-vector-icons/MaterialIcons';
import Modal from 'react-native-modalbox';
import TextField from 'react-native-md-textinput';
//import Button from '../../../../Components/Button';
import ModalDropdown from 'react-native-modal-dropdown';
import dismissKeyboard from 'react-native-dismiss-keyboard';



const approveIcon = (<Icon name="done" size={30} color="#ffffff" />)

export default class ChecklistModel extends Component {

    constructor() {
        super();
        this.state = {
           enables: 'clocking off',
           checklistName: '',
           checklistId: '',
           key: 0,
        };
    }

    
    getModelWidth(){
        let modelWidth = Dimensions.get('window').width

        return Dimensions.get('window').width>500 ? modelWidth/3: 7* modelWidth/8
    }

    componentWillMount() {
         //console.log( this.props.itemData[1])
         this.setState({ 
               
                //enables: this.props.enablesStae[1],
                // checklistName: this.props.Checklists[1],
                // checklistId: this.props.Checklists[0]

         })
    }
    componentDidMount(){
       
    }

    componentWillReceiveProps(nextProps) {
        if(nextProps.Checklists != this.props.Checklists){
            //console.log(nextProps.Checklists)
            this.setState({ 
               
               checklistName: nextProps.Checklists[0].checkListName,
               checklistId: nextProps.Checklists[0].checkListId

            })
        }
    }

    updateState() {
        this.setState({ 
            ...this.state,
        });
    }

     dropDown(rowData, isEnables) {
        return (
            <TouchableHighlight style={{ flex: 1}} >
                <View style={{ width: 200, flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center', padding: 10 }}>
                    {isEnables? <Text style={{fontSize: 16 }}>{rowData}</Text> : <Text style={{fontSize: 16 }}>{rowData.checklistName}</Text>}
                </View>
            </TouchableHighlight>
        );
    }

render() {
        return (
                <Modal onClosed={() => { this.props.onClosed(), this.state.textField = '' } } position="top" isOpen={this.props.isOpen} style={{ zIndex: 1000, marginTop: 80, padding: 20, width: this.getModelWidth() , height: 300, backgroundColor: '#ffffff' }}>
                    <View style= {{flexDirection: 'row', justifyContent:'center', alignItems: 'center' ,backgroundColor:'dodgerblue', height: 60, margin: -20, marginBottom: 10}}>
                        <View style = {{flex: 1,justifyContent: 'flex-start', alignItems:'flex-start', paddingLeft: 15}}>
                            <Text style = {{ fontSize: 18, color: 'white'}}>Add Checklist</Text>
                        </View>
                        <View style = {{flex: 1,justifyContent: 'flex-end', alignItems:'flex-end', paddingRight:15}}>
                            <Ripple color='rgba(255,255,255,.5)' onPress={()=>{this.props.onClosed()}}>
                                <Icon name="close" size={25} color="#ffffff" />
                            </Ripple>
                        </View>    
                    </View>

                    <View>
                        <View style = {{flexDirection: 'row', marginTop: 20}}>
                            <Text style={styles.text}>Title: </Text>
                             <ModalDropdown
                                style={styles.pickerBox}
                                options={
                                    this.props.Checklists.reduce((names, data)=>{
                                        names.push([data.checkListName])
                                        return names
                                    },[])
                                }
                                onSelect={(idx, value) => {
                                    let chklist = this.props.Checklists.find((checklist)=>{
                                                    return value == checklist.checkListName
                                                  })
                                    this.setState({
                                        checklistName: value,
                                        checklistId: chklist.checkListId
                                    })
                                }}
                                renderRow ={this.dropDown.bind(this)}
                                >
                                <View style={styles.dropdown}>
                                    <Text style={{paddingLeft:20,  color: 'black', fontSize: 16 }}>
                                        {this.state.checklistName}
                                    </Text>
                                    <Icon name="arrow-drop-down" size={20} color="#000000" />
                                </View>
                            </ModalDropdown>
                        </View>
                        <View style={{justifyContent:'flex-start', flexDirection :'row', marginTop: 20}}>
                            <Text style={styles.text}>Type: </Text>
                             <ModalDropdown
                                style={styles.pickerBox}
                                options={['clocking off', 'clocking on']}
                                onSelect={(idx, value) => {
                                    this.setState({
                                        enables: value 
                                    })
                                }}
                                renderRow ={this.dropDown.bind(this)}
                                >
                                <View style={styles.dropdown}>
                                    <Text style={{paddingLeft:20,  color: 'black', fontSize: 16 }}>
                                        {this.state.enables}
                                    </Text>
                                    <Icon name="arrow-drop-down" size={20} color="#000000" />
                                </View>
                            </ModalDropdown>
                        </View>

                        <View style={{ marginTop: 20, justifyContent:'flex-end', flexDirection: 'row'}}>

                            <Ripple style={[styles.button, {backgroundColor: '#4fab6e',}]} color='rgba(255,255,255,.5)' 
                              onPress={()=>{
                                  this.props.onApply([this.state.checklistId, this.state.enables], this.state.key)}}>
                                <Icon name="done" size={25} color="#ffffff" />
                                <Text style={[styles.text,{color: 'white'}]}>ADD</Text>
                            </Ripple>                       
                            
                        </View>
                                          
                            
                    </View>

                    
                </Modal>
            );
    }

}

const styles = StyleSheet.create({
     photo: {
        height: 60,
        width: 60,
        borderRadius: 60,
    },
    text:{
        paddingLeft:10, 
        paddingTop: 5,  
        color: 'black', 
        fontSize: 16 
    },
    button: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems:'center',
        padding:10,
        marginHorizontal: 20,
        elevation: 2
    },
    image: {
        height: 40,
        width: 40,
        borderRadius: 60
    },
    pickerBox:{
        marginHorizontal: 20,
        borderColor: '#D6D6D6', 
        borderWidth: 1,
        flexDirection:'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        height:40
    },
    dropdown:{
        
        flex: 1, 
        width:200,
        flexDirection: 'row', 
        justifyContent: 'space-between', 
        alignItems: 'center',
    },
})