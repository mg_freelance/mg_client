// @flow
import React, { Component } from 'react';
import { Colors, Metrics } from '../../../../Themes'
import {
  StyleSheet,
  ListView,
  Text,
  ScrollView,
  View
} from 'react-native';
import UserRow from './UserRow';
import ActionButton from 'react-native-action-button';
import Icon from 'react-native-vector-icons/Ionicons';
import { Actions as NavigationActions } from 'react-native-router-flux'
import {api} from '../../../../Lib/helpers';

//const getUsers = () => api.get('user');

const styles = StyleSheet.create({
  container: {
    flex: 1,

  },
  separator: {
    flex: 1,
    height: StyleSheet.hairlineWidth,
    backgroundColor: '#8E8E8E',
  },
});

export default class UserList extends Component {
  constructor() {
    super();

    const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
    this.state = {
      dataSource: ds.cloneWithRows([]),
    };

  }
  componentDidMount(){
   
  }
  render() {
     api().get('user').then((response) => {
      //console.log(response);
      const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
      this.setState({
        newUserTemplate:{
            firstName: '',
            surName: '',
            username: '',
            siteEmail: 'test@bback.com',
            roles: [
               
            ],
            active: false,
            temporaryStaff: false,
            contractStartDate: '',
            contractEndDate: '',
            qualifications: [],
            skill: [],
            profileImg: 'https://www.outsystems.com/PortalTheme/img/UserImage.png?24860',
            password: '',
        },
        dataSource: ds.cloneWithRows(response.data),
      });
    });
    return (
      <View style={styles.container}>
        <ScrollView style={styles.container}>
          <ListView style={{ backgroundColor: Colors.background }}
            dataSource={this.state.dataSource}
            enableEmptySections={true}
            renderRow={(rowData) => { return (<UserRow data={rowData} />) } }
            renderSeparator={(sectionId, rowId) => <View key={rowId} style={styles.separator} />}
            >
          </ListView>
        </ScrollView>

        <ActionButton style={styles.container} buttonColor="#408fff" onPress={()=>{NavigationActions.userView({data:this.state.newUserTemplate,create:true})}}>
        </ActionButton>
      </View>
    );
  }
}