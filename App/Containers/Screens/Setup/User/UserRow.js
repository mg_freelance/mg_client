import React, { Component } from 'react';
import { View, Text, StyleSheet, Image } from 'react-native';
import { Avatar, Drawer, Divider, PRIMARY_COLORS, TYPO, Ripple } from 'react-native-material-design';
import { Actions as NavigationActions } from 'react-native-router-flux'
const styles = StyleSheet.create({
    container: {
        flex: 1,

        flexDirection: 'row',
        alignItems: 'center',
    },
    text: {
        marginLeft: 12,
        marginTop: 12,
        fontSize: 16,
    },
    photo: {
        height: 60,
        width: 60,
        borderRadius: 60,
    },
    rippleContainer: {
        flex: 1,
        flexDirection: 'row',
        height: 90,
        padding: 12,
    }
});

export default class UserRow extends React.Component {
    constructor(props) {
        super(props)
    }
    render() {
        return (
            <View style={styles.container}>
                <Ripple text="" style={styles.rippleContainer} color='rgba(0,0,0,.5)' onPress={() => { NavigationActions.userView({ data: this.props.data,create:false }) } }>
                    <Image source={{ uri: this.props.data.profileImg }} style={styles.photo} />
                    <View style={{ flex: 1, flexDirection: 'column' }}>
                        <Text style={styles.text}>
                            {this.props.data.firstName} {this.props.data.surName}
                        </Text>
                        <Text style={[styles.text, { fontSize: 14,color:'#101010' }]}>
                            {this.props.data.roles[0].roleName}
                        </Text>
                    </View>
                </Ripple>
            </View>
        );
    }
}
