// @flow
import React, { Component } from 'react';
import { TouchableOpacity, DatePickerAndroid, Alert, View, Text, StyleSheet, Image, ScrollView, TextInput, Switch, Picker, ListView, Dimensions } from 'react-native';
import { Avatar, Drawer, Divider, PRIMARY_COLORS, TYPO, Ripple } from 'react-native-material-design';
import TextField from 'react-native-md-textinput';
import Button from '../../../../Components/Button'
import Icon from 'react-native-vector-icons/Ionicons';
import SelectionModal from './Modals/SelectionModal';
import {api} from '../../../../Lib/helpers';
import { Actions as NavigationActions } from 'react-native-router-flux'
import dismissKeyboard from 'react-native-dismiss-keyboard'
import { connect } from 'react-redux'
import ImagePicker from 'react-native-image-picker';


type UserViewProps = {
    data: Object,
    authToken: string
}
class UserView extends Component {
    props: UserViewProps;
    constructor(props) {
        super(props);
        this.state = {
            
        }
    }

    // aligncomponents = event => {
    //     this.setState({
    //         flexDir: event.nativeEvent.layout.width < 500 ? 'column' : 'row'
    //     })
    // }


    getDirection(){
        return Dimensions.get('window').width< 500 ? 'column' : 'row'
    }

    componentWillMount() {
        const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
        this.state = {
            userData: {...this.props.data },
            authToken: this.props.authToken,
            isQModelOpen: false,
            isSModelOpen: false,
            isRModelOpen: false,
            skillModalList: [],
            roleModalList: [],
            rawSkillList: [],
            rawRoleList: [],
            qualificationDS: ds.cloneWithRows(this.props.data.qualifications),
            skillsDS: ds.cloneWithRows(this.props.data.skill),
            rolesDS: ds.cloneWithRows(this.props.data.roles),

        }
        api().setHeader('authorization',this.state.authToken);
        this.props.setActionHandlers('confirm', () => {
            // if (this.props.create) {
            var isValid = this.validateObject(this.state.userData);
            if (!isValid) {
                Alert.alert(
                    'Validation Failure',
                    'Please complete all fields to save',
                    [
                        { text: 'OK', onPress: () => { } },
                    ]
                );
            } else {
                if (!this.props.create) {
                    
                    api().put('user/' + this.state.userData.userId, this.state.userData, {
                        headers:
                        { "content-type": 'application/json', 'authorization': this.state.authToken }
                    }).then((response) => {
                        Alert.alert(
                            'Success',
                            'User Updated Successfully',
                            [
                                {
                                    text: 'OK', onPress: () => {
                                        NavigationActions.pop();
                                    }
                                },
                            ]
                        );
                    });
                } else {

                    api().post('user', this.state.userData, {
                        headers:
                        { "content-type": 'application/json','authorization': this.state.authToken  }
                    }).then((response) => {
                        Alert.alert(
                            'Success',
                            'User Created Successfully',
                            [
                                {
                                    text: 'OK', onPress: () => {
                                        NavigationActions.pop();
                                    }
                                },
                            ]
                        );
                    });
                }
            }
            //} else {
            //     console.log(this.state);
            //}
        });

        this.props.setActionHandlers('delete', () => {
            Alert.alert(
                'Delete',
                'Are you sure you want to Delete?',
                [
                    { text: 'Cancel', onPress: () => { } },
                    {
                        text: 'OK', onPress: () => {
                            NavigationActions.pop();
                        }
                    },
                ]
            )
        });
    }


    toggleModal(modalName, openStatus) {
        this.state[modalName] = openStatus;
        this.updateState();
    }

    getRoleList() {
        var $promise = {
            then: (func) => {
                $promise.callback = func;
            }
        };
        api().get('role').then((response) => {
            var modelData = [];
            this.state.rawRoleList = response.data;
            for (var key in response.data) {
                modelData.push({ id: response.data[key].roleId, value: response.data[key].roleName });
            }
            $promise.callback(modelData);
        });
        return $promise;
    }

    getSkillList() {
        var $promise = {
            then: (func) => {
                $promise.callback = func;
            }
        };
        api().get('skill').then((response) => {
            var modelData = [];
            this.state.rawSkillList = response.data;
            for (var key in response.data) {
                modelData.push({ id: response.data[key].skillId, value: response.data[key].skillName });
            }
            $promise.callback(modelData);
        });
        return $promise;
    }

    validateObject(userObj) {
        var validationStatus = true;
        var exclusions = ["qualifications", "skill"];
        for (var i in userObj) {
            var excluded = false;

            for (var exclusion in exclusions) {
                if (i == exclusions[exclusion]) {
                    excluded = true;
                    validationStatus = validationStatus && true;
                    break;
                }
            }
            if (!excluded) {
                if (Array.isArray(userObj[i])) {
                    // Is an Array
                    validationStatus = validationStatus && userObj[i].length > 0;
                    console.log("Array " + validationStatus);
                } else if (typeof userObj[i] === 'boolean') {
                    validationStatus = validationStatus && true;
                } else {
                    validationStatus = validationStatus && userObj[i].length > 0;
                }
            }
            // Validation false then break out...
            if (!validationStatus) {
                break;
            }

        }
        return validationStatus;
    }

    showPicker = async (stateKey, options) => {
        try {
            var newState = {};
            const {action, year, month, day} = await DatePickerAndroid.open(options);
            if (action === DatePickerAndroid.dismissedAction) {
                newState[stateKey + 'Text'] = 'dismissed';
            } else {
                var newState = {...this.state };

                var date = new Date(year, month, day);
                newState.userData[stateKey] = date.toLocaleDateString();
                this.setState(newState);

            }
            this.setState(newState);
        } catch ({code, message}) {
            console.warn(`Error in example '${stateKey}': `, message);
        }
    };

    removeFromDataSource(key, id, datasource) {
        this.state[datasource] = this.state[datasource].filter((col) => {
            return col[key] != id
        });
        this.updateState();
    }
    updateState() {
        const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
        this.setState({
            ...this.state,
            qualificationDS: ds.cloneWithRows(this.state.userData.qualifications),
            skillsDS: ds.cloneWithRows(this.state.userData.skill),
            rolesDS: ds.cloneWithRows(this.state.userData.roles),
        });
    }

    setImage(){

        const options = {
            quality: 1.0,
            maxWidth: 500,
            maxHeight: 500,
            storageOptions: {
                skipBackup: true
            }
            };
            
            ImagePicker.showImagePicker(options, (response) => {

            if (response.didCancel) {
                console.log('User cancelled photo picker');
            }
            else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            }
            else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            }
            else {
                this.changeAvatar(this.state.userData.userId, response.type, response.data)
                this.state.userData.profileImg = response.uri
                this.setState({
                    userData: this.state.userData
                })
            }
        });
    
    }

     changeAvatar(userId, Imgtype, data){
        api().post('upload', {userID: userId, ImageType: Imgtype, ImageData: data})
        .then((response)=>{
            //console.log(response)
            this.setState({

            })
        })
    }

render() {

    

    return (
        <View style={{ flex: 1 }} >
            <ScrollView style={styles.parent}>

                <View style={[styles.topContainer, { flexDirection: this.getDirection() }]}>

                    <View style={{ flexDirection: 'column', alignItems: 'center', justifyContent: 'center', paddingBottom: -10 }}>
                        <Image source={{ uri: this.state.userData.profileImg }} style={styles.photo} />
                        <Icon name = {'ios-camera'} size={55} color='#dddddd' style={{ position: 'absolute', bottom: 20, right:40}} onPress= {()=>{
                                this.setImage()
                        }}/>
                    </View>

                    <View style={{ flex: 1, flexDirection: 'column', padding: 50, height: 200, backgroundColor: 'white' }}>
                        {(!this.props.create) ? (<View>
                            <TextField
                                label={'User ID'}
                                highlightColor={'#00BCD4'}
                                value={this.state.userData.userId}
                                dense={true}
                                editable={false}

                                />
                        </View>) : null}
                        <View style={{ flexDirection: 'row' }}>
                            <View style={{ flex: 1 }}>
                                <TextField
                                    label={'First Name'}
                                    highlightColor={'#00BCD4'}
                                    value={this.state.userData.firstName}
                                    dense={true}
                                    onChangeText={(text) => {
                                        var newState = {...this.state};
                                        newState.userData.firstName=text;
                                        this.setState(newState)}}
                                />
                             </View>
                            <View style={{ flex: 1, marginLeft: 50 }}>
                                <TextField
                                    label={'Surname'}
                                    highlightColor={'#00BCD4'}
                                    value={this.state.userData.surName}
                                    dense={true}
                                    onChangeText={(text) => {
                                        var newState = {...this.state};
                                        newState.userData.surName=text;
                                        this.setState(newState)
                                        }}
                                />
                        </View>
                        </View>
                    </View>
                </View>
                <View style={[styles.topContainer, { marginTop: 20, flexDirection: this.getDirection() }]}>
                    <View style={{ paddingRight: 0 }}>
                        <Text style={{ fontSize: 16, fontWeight: 'bold', margin: 30, paddingRight: 20, color: 'black' }}> Login Details </Text>
                    </View>
                    <View style={{ flex: 1, flexDirection: 'column', padding: 30, paddingLeft: 15, backgroundColor: 'white' }}>
                        <View style={{ flexDirection: 'row' }}>
                            <ScrollView style={{ flex: 1 }}>
                                <TextField
                                    label={'Username'}
                                    highlightColor={'#00BCD4'}
                                    value={this.state.userData.username}
                                    onChangeText={(text) => {
                                        var newState = {...this.state}
                                        newState.userData.username=text;
                                        this.setState(newState);
                                    }

                                    }
                                dense={true}
                                />
                        </ScrollView>
                            <ScrollView style={{ flex: 1, marginLeft: 50 }}>
                                <TextField
                                    label={'Password'}
                                    highlightColor={'#00BCD4'}
                                    secureTextEntry={true}
                                    value={this.state.userData.password}
                                    onChangeText={(text) => {
                                        var newState = {...this.state};
                                        newState.userData.password=text;
                                        this.setState(newState)}}
                                    dense={true}
                                />
                            </ScrollView>
                        </View>
                        <View style={{ flexDirection: 'row', marginTop: 20 }}>
                            <View style={{ flex: 1, flexDirection: 'column', alignItems: 'flex-start' }}>
                                <Text style={{ color: 'black', margin: 15, marginLeft: 0 }}>Active</Text>
                                <Switch style={{ margin: 10, marginLeft: 0 }} value={this.state.userData.active} onValueChange={(value) => {
                                    var newState = {...this.state};
                                        newState.userData.active=value;
                                        this.setState(newState) 
                                     } }></Switch>
                        </View>
                        <View style={{ flex: 1, flexDirection: 'column', alignItems: 'flex-start' }}>
                            <Text style={{ color: 'black', margin: 15, marginLeft: 0 }}>Temporary Staff</Text>
                            <Switch style={{ margin: 10, marginLeft: 0 }} value={this.state.userData.temporaryStaff} onValueChange={(value) => {
                                var newState = {...this.state};
                                        newState.userData.temporaryStaff=value;
                                        this.setState(newState); 
                                } }></Switch>
                    </View>
                </View>
                <View style={{ flexDirection: 'row', marginTop: 20 }}>

                    <ScrollView style={{ flex: 1 }}>
                        <TouchableOpacity style={{ flex: 1 }} onPressIn={() => { dismissKeyboard(); } } onPressOut={() => { dismissKeyboard(); } } onPress={() => { dismissKeyboard(); } }>
                            <TextField
                                label={'Contract Start Date'}
                                highlightColor={'#00BCD4'}
                                ref="contractStartDate"
                                value={this.state.userData.contractStartDate}
                                onFocus={() => {
                                    // dismissKeyboard();
                                    this.showPicker('contractStartDate', {
                                        date: new Date(this.state.userData.contractStartDate)
                                    });
                                } }
                                onChangeText={(text) => {
                                    var newState = {...this.state};
                                        newState.userData.contractStartDate=text;
                                        this.setState(newState);
                                      }
                                    }
                                dense={true}
                            />


                        </TouchableOpacity>
                    </ScrollView>

                    <ScrollView style={{ flex: 1, marginLeft: 50 }}>
                        <TextField
                            label={'Contract End Date'}
                            highlightColor={'#00BCD4'}
                            value={this.state.userData.contractEndDate}
                            onFocus={() => {
                                dismissKeyboard();
                                this.showPicker('contractEndDate', { date: new Date(this.state.userData.contractEndDate) })
                            } }
                            onChangeText={(text) => {
                                var newState = {...this.state};
                                        newState.userData.contractEndDate=text;
                                        this.setState(newState);
                                     }
                                    }
                                dense={true}
                        />
                        </ScrollView>
                </View>
                <View style={{ flexDirection: 'row', marginTop: 20 }}>
                    <ScrollView style={{ flex: 1 }}>
                        <Text style={{ color: 'black', fontSize: 14, marginTop: 15 }}>User Level</Text>
                        <Picker style={{ height: 25 }} selectedValue={this.state.userData.userLevel} onValueChange={(userLevel) => {
                            var newState = {...this.state};
                                        newState.userData.userLevel=userLevel;
                                        this.setState(newState);
                        }}>
                            <Picker.Item label="Technician" value="technician" />
                        <Picker.Item label="Floater" value="floater" />
                        </Picker>
                    <Divider></Divider>
                    </ScrollView>
                <ScrollView style={{ flex: 1, marginLeft: 50 }}>
                    <Text style={{ color: 'black', fontSize: 14, marginTop: 15 }}>Associated Entity</Text>
                    <Picker style={{ height: 25 }} selectedValue={this.state.userData.associatedEntity} onValueChange={(associatedEntity) => {
                        var newState = {...this.state};
                                        newState.userData.associatedEntity=associatedEntity;
                                        this.setState(newState);
                        }} >
                            <Picker.Item label="Technician" value="Technician" />
                    <Picker.Item label="Floater" value="Floater" />
                        </Picker>
                <Divider></Divider>
            </ScrollView>
        </View >
                    </View >
        </View >
        <View style={[styles.topContainer, { marginTop: 20, flexDirection: this.getDirection() }]}>
            <View style={{ paddingRight: 0 }}>
                <Text style={{ fontSize: 16, fontWeight: 'bold', margin: 30, color: 'black' }}> Qualifications </Text>
            </View>
            <View style={{ flex: 1, flexDirection: 'column', padding: 30, backgroundColor: 'white' }}>
                <ListView style={{ backgroundColor: 'white' }}
                    dataSource={this.state.qualificationDS}
                    enableEmptySections={true}
                    renderRow={(rowData) => {
                        return (
                            <View style={{ flex: 1, flexDirection: 'row', marginTop: 15 }}>
                                <Text style={{ flex: 1 }}>{rowData.qualificationDescription}</Text>
                                <View style={{ flex: 1, marginTop: -5, alignItems: 'flex-end' }}>
                                    <Icon name="ios-close" size={30} onPress={() => { this.removeFromDataSource("qualificationDescription", rowData.qualificationDescription, "qualifications") } } ></Icon>
                                </View>
                            </View>)
                    } }
                    >
                </ListView>
                <View style={{ flex: 1, width: 250, marginTop: 50 }}>
                    <Button overrides={{ backgroundColor: '#5fa54a', textColor: '#ffffff', rippleColor: '#a5f48d', height: 100 }} text="ADD QUALIFICATIONS" raised={true} onPress={() => { this.toggleModal("isQModelOpen", true); } } />
                </View>
            </View>

        </View>
        <View style={[styles.topContainer, { marginTop: 20, flexDirection: this.getDirection() }]}>
            <View style={{ paddingRight: 0 }}>
                <Text style={{ fontSize: 16, fontWeight: 'bold', margin: 30, paddingRight: 60, color: 'black' }}> Skills </Text>
            </View>
            <View style={{ flex: 1, flexDirection: 'column', padding: 30, backgroundColor: 'white' }}>
                <View style={{ flex: 1, flexDirection: 'row' }}>
                    <Text style={{ flex: 2 }}>Skill ID</Text>
                    <Text style={{ flex: 3 }}>Skill Name </Text>
                    <Text style={{ flex: 4 }}>Skill Description</Text>
                    <Text style={{ flex: 1 }}></Text>
                </View>
                <ListView style={{ backgroundColor: 'white' }}
                    dataSource={this.state.skillsDS}
                    enableEmptySections={true}
                    renderRow={(rowData) => {
                        return (
                            <View style={{ flex: 1, flexDirection: 'row', marginTop: 15 }}>
                                <Text style={{ flex: 2 }}>{rowData.skillId}</Text>
                                <Text style={{ flex: 3 }}>{rowData.skillName} </Text>
                                <Text style={{ flex: 4 }}>{rowData.skillDescription}</Text>
                                <View style={{ flex: 1, marginTop: -5, alignItems: 'flex-end' }}>
                                    <Icon name="ios-close" size={30} onPress={() => { this.removeFromDataSource("skillId", rowData.skillId, "skill") } }  ></Icon>
                                </View>
                            </View>)
                    } }
                    >
                </ListView>
                <View style={{ flex: 1, width: 250, marginTop: 50, justifyContent: 'flex-start' }}>
                    <Button overrides={{ backgroundColor: '#5fa54a', textColor: '#ffffff', rippleColor: '#a5f48d' }} text={'ADD SKILLS'} raised={true}
                        onPress={
                            () => {
                                this.getSkillList().then(
                                    (modalData) => {
                                        this.state.skillModalList = modalData;
                                        this.updateState();

                                    });
                                this.toggleModal("isSModelOpen", true);
                            }
                        } />
                </View>
            </View>
        </View>
        <View style={[styles.topContainer, { marginTop: 20, flexDirection: this.getDirection() }]}>
            <View style={{ paddingRight: 0 }}>
                <Text style={{ fontSize: 16, fontWeight: 'bold', margin: 30, paddingRight: 60, color: 'black' }}> Roles </Text>
            </View>
            <View style={{ flex: 1, flexDirection: 'column', padding: 30, backgroundColor: 'white' }}>
                <View style={{ flex: 1, flexDirection: 'row' }}>
                    <Text style={{ flex: 2 }}>Role ID</Text>
                    <Text style={{ flex: 3 }}>Role Name </Text>
                    <Text style={{ flex: 4 }}>Role Description</Text>
                    <Text style={{ flex: 1 }}></Text>
                </View>
                <ListView style={{ backgroundColor: 'white' }}
                    dataSource={this.state.rolesDS}
                    enableEmptySections={true}
                    renderRow={(rowData) => {
                        return (
                            <View style={{ flex: 1, flexDirection: 'row', marginTop: 15 }}>
                                <Text style={{ flex: 2 }}>{rowData.roleId}</Text>
                                <Text style={{ flex: 3 }}>{rowData.roleName} </Text>
                                <Text style={{ flex: 4 }}>{rowData.roleDescription}</Text>
                                <View style={{ flex: 1, marginTop: -5, alignItems: 'flex-end' }}>
                                    <Icon name="ios-close" size={30} onPress={() => { this.removeFromDataSource("roleId", rowData.roleId, "roles") } }  ></Icon>
                                </View>
                            </View>)
                    } }
                    >
                </ListView>
                <View style={{ flex: 1, width: 250, marginTop: 50 }}>
                    <Button overrides={{ backgroundColor: '#5fa54a', textColor: '#ffffff', rippleColor: '#a5f48d' }} text="ADD ROLES" raised={true}
                        onPress={() => {
                            this.getRoleList().then(
                                (modalData) => {
                                    this.state.roleModalList = modalData;
                                    this.updateState();

                                });
                            this.toggleModal("isRModelOpen", true);
                        } } />
                </View>
            </View>
        </View>
        <View style={{ marginTop: 50, height: 1 }}>
        </View>

            </ScrollView >
        <SelectionModal
            modalType="textModal"
            title="ADD QUALIFICATION"
            isOpen={this.state.isQModelOpen}
            label="Qualification"
            onClosed={() => { this.toggleModal("isQModelOpen", false);} }
            onApply={(text) => { this.state.userData.qualifications.push({ qualificationDescription: text }); this.toggleModal("isQModelOpen", false); this.updateState(); } } />
        <SelectionModal
            modalType="listModal"
            title="ADD SKILL"
            isOpen={this.state.isSModelOpen}
            onClosed={() => { this.toggleModal("isSModelOpen", false);} }
            listData={this.state.skillModalList}
            onApply={(selectedList) => {
                var selectedSkills = [];
                console.log(this.state.rawSkillList);
                for (var i in selectedList) {
                    for (var j in this.state.rawSkillList) {
                        if (this.state.rawSkillList[j].skillId == selectedList[i]) {
                            selectedSkills.push(this.state.rawSkillList[j])
                        }
                    }
                }
                this.state.userData.skill = selectedSkills;
                console.log("SKILL LIST");
                console.log(selectedList);
                this.toggleModal("isSModelOpen", false);
                this.updateState();
            } } />

        <SelectionModal
            modalType="listModal"
            title="ADD ROLES"
            isOpen={this.state.isRModelOpen}
            listData={this.state.roleModalList}
            onClosed={() => { this.toggleModal("isRModelOpen", false); this.updateState(); } }
            onApply={(selectedList) => {
                var selectedRoles = [];
                console.log(this.state.rawRoleList);
                for (var i in selectedList) {
                    for (var j in this.state.rawRoleList) {
                        if (this.state.rawRoleList[j].roleId == selectedList[i]) {
                            selectedRoles.push(this.state.rawRoleList[j])
                        }
                    }
                }
                this.state.userData.roles = selectedRoles;
                this.toggleModal("isRModelOpen", false);
                this.updateState();
            } } />
        </View >
    );
}
}

const mapStateToProps = (state) => {
    return {
        authToken: state.login.authToken,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(UserView)

const styles = StyleSheet.create({
    parent: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: '#eeeeee',
        margin: 0,
        marginTop: 50,
        padding: 10,
    },
    topContainer: {
        flex: 1,
        flexDirection: 'row',
        //flexWrap: 'wrap',
        backgroundColor: 'white',
        borderRadius: 2,
        elevation: 2
    },

    photo: {
        flex: 1,
        margin: 40,
        height: 160,
        width: 160,
        borderRadius: 180,
    },
    separator: {
        marginTop: 10,
        height: 1,
        backgroundColor: '#8E8E8E',
    },
    profileName: {
        marginTop: 10,
        fontSize: 30,
        color: 'black',
        fontWeight: '400'
    },
    profileDesignation: {
        fontSize: 25,
        color: 'black',
        fontWeight: '400'
    }
});