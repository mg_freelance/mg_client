// @flow
import React, { Component } from 'react';
import { Colors, Metrics } from '../../../../../Themes'
import {
    StyleSheet,
    ListView,
    Text,
    ScrollView,
    View,
    Dimensions
} from 'react-native';
import { Avatar, Checkbox, Drawer, Divider, PRIMARY_COLORS, TYPO, Ripple } from 'react-native-material-design';
import ActionButton from 'react-native-action-button';
import Icon from 'react-native-vector-icons/Ionicons';
import Modal from 'react-native-modalbox';
import TextField from 'react-native-md-textinput';
import Button from '../../../../../Components/Button'



export default class SelectionModal extends Component {

    constructor() {
        super();
        this.state = {
            initial: true,
            checkbox: {},
            selectionList: [],
            //width: modelWidth
        };
    }

    getModelWidth(){
        let modelWidth = Dimensions.get('window').width

        return Dimensions.get('window').width>500 ? modelWidth/3: 7* modelWidth/8
    }
    componentWillMount() {
        if (this.props.modalType == 'textModal' || this.props.modalType == 'loginModel') {
            this.state = { 
                ...this.state,
                textField: ''

            }
        } else {
            const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });

            this.state = { 
                ...this.state,
                textField: ''
            }
            this.state.sourceListData = this.props.listData;
            this.state.listData = ds.cloneWithRows(this.props.listData);
        }
    }

    updateState() {
        this.setState({ 
            ...this.state,
        });
}

componentWillReceiveProps(nextProps) {
    if (this.state.sourceListData != nextProps.listData) {
        const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
        this.state.sourceListData = nextProps.listData;
        this.state.listData = ds.cloneWithRows(nextProps.listData);
        for (var k in this.props.listData) {
            this.state.checkbox[this.props.listData[k]] = false;

        }
        this.updateState();
    }
}

render() {

    if (this.props.modalType == 'textModal') {
        return (
            <Modal onClosed={() => { this.props.onClosed() } } position="top" isOpen={this.props.isOpen} style={{ zIndex: 1000, marginTop: 80, padding: 20, width: this.getModelWidth() , height: 200, backgroundColor: '#ffffff' }}>
                <View>
                    <Text>{this.props.title}</Text>
                    <Divider />
                    <TextField
                        label={this.props.label}
                        labelStyle={{ color: 'black' }}
                        highlightColor={'#00BCD4'}
                        value={this.state.textField}
                        onChangeText={(text) => this.setState({ ...this.state, textField: text })}
                                dense={true}
                    />
                        <View style={{ marginTop: 25 }}>
                        <Button overrides={{ backgroundColor: '#5fa54a', textColor: '#ffffff', rippleColor: '#a5f48d' }} text="APPLY" raised={true} onPress={() => {
                            if (this.state.textField.trim().length == 0) {
                                window.alert(this.props.label + " cannot be empty");
                            } else {

                                this.props.onApply(this.state.textField);
                            }
                        } } />
                    </View>
                </View>
            </Modal>
        );
    } else if (this.props.modalType == 'listModal') {

        return (
            <Modal onClosed={() => { this.props.onClosed() } } position="top" isOpen={this.props.isOpen} style={{ zIndex: 1000, marginTop: 80, padding: 20, width: this.getModelWidth(), height: 250, backgroundColor: '#ffffff' }}>
                <Text>{this.props.title}</Text>
                <Divider />
                <ScrollView>
                    <ListView style={{ backgroundColor: 'white' }}
                        dataSource={this.state.listData}
                        enableEmptySections={true}
                        renderRow={(rowData) => {
                            return (
                                <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center', marginTop: 15 }}>
                                    <Checkbox checked={this.state.checkbox[rowData.id]} value={rowData.id} onCheck={
                                        (status, item) => {
                                            if (status) {
                                                this.state.selectionList.push(item);
                                            } else {
                                                this.state.selectionList = this.state.selectionList.filter((col) => {
                                                    return (col != item);
                                                });
                                            }
                                            this.state.checkbox[rowData.id] = status;
                                            this.updateState();
                                        } } />
                                    <Text style={{ color: 'black' }}>{rowData.value}</Text>

                                </View>)
                        } }
                        >
                    </ListView>
                </ScrollView>
                <Button overrides={{ backgroundColor: '#5fa54a', textColor: '#ffffff', rippleColor: '#a5f48d' }} text="APPLY" raised={true} onPress={() => { this.props.onApply(this.state.selectionList) } } />
            </Modal>
        );
    } else if (this.props.modalType == 'loginModel') {
        return (
            <Modal onClosed={() => { this.props.onClosed() } } position="top" isOpen={this.props.isOpen} style={{ zIndex: 1000, marginTop: 100, padding: 30, width:this.props.width, height: 200, backgroundColor: '#ffffff' }}>
                <View>
                    <Text>{this.props.title}</Text>

                    <TextField
                        label={this.props.label}
                        labelStyle={{ color: 'black' }}
                        highlightColor={'#00BCD4'}
                        value={this.state.textField}
                        onChangeText={(text) => this.setState({ ...this.state, textField: text })}
                                dense={true}
                    />
                        <View style={{ marginTop: 25, width: 150, marginLeft: -10 }}>
                        <Button overrides={{ backgroundColor: '#5fa54a', textColor: '#ffffff', rippleColor: '#a5f48d' }} text="OK" raised={true} onPress={() => { this.props.onApply(this.state.textField) } } />
                    </View>
                </View>
            </Modal>
        );
    }

}

}