import React, { Component } from 'react';
import { Colors, Metrics } from '../../../../Themes'
import {
  StyleSheet,
  ListView,
  Text,
  ScrollView,
  View
} from 'react-native';
import SkillRow from './SkillRow';
import ActionButton from 'react-native-action-button';
import Icon from 'react-native-vector-icons/Ionicons';
import {api} from '../../../../Lib/helpers';
import { Actions as NavigationActions } from 'react-native-router-flux'

export default class SkillList extends Component {
  constructor() {
    super();
    const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
    this.state = {
      dataSource: ds.cloneWithRows([]),
    };

  

  }

  render() {
      api().get('skill').then((response) => {
      const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
      this.setState({
        dataSource: ds.cloneWithRows(response.data),
      });
    });
    return (
      <View style={styles.container}>
        <ScrollView style={styles.container}>
          <ListView style={{ backgroundColor: Colors.background }}
            enableEmptySections={true}
            dataSource={this.state.dataSource}
            renderRow={(rowData) => { return (<SkillRow data={rowData} />) } }
            renderSeparator={(sectionId, rowId) => <View key={rowId} style={styles.separator} />}
            >
          </ListView>
        </ScrollView>
        <ActionButton style={styles.container} buttonColor="#408fff" onPress={()=>{NavigationActions.addSkillView({data:{roleName:'',roleDescription:''}})}}>
         
        </ActionButton>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,

  },
  separator: {
    flex: 1,
    height: StyleSheet.hairlineWidth,
    backgroundColor: '#8E8E8E',
  },
});