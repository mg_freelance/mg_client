import React, { Component } from 'react';
import { Colors, Metrics } from '../../../../Themes'
import {
    StyleSheet,
    ListView,
    Text,
    ScrollView,
    Alert,
    View,
    Dimensions,
    TextInput
} from 'react-native';
import SkillRow from './SkillRow';
import ActionButton from 'react-native-action-button';
import Icon from 'react-native-vector-icons/Ionicons';
import TextField from 'react-native-md-textinput';
import { Actions as NavigationActions } from 'react-native-router-flux'
import { connect } from 'react-redux'
import {api} from '../../../../Lib/helpers';

var flexDir = ''

class SkillView extends Component {
   
    constructor(props) {
        super(props);
        this.state = {
            flexDir: Dimensions.get('window').width< 500 ? 'column' : 'row',
            authToken: this.props.authToken,
            efficiencyPercent: '',
            efficiencyHrs:  '',
            //flexDir: 'column'
        }
    }

    componentWillReceiveProps(nextProps) {
        if (this.state.authToken != nextProps.authToken) {
            this.setState({ authToken: nextProps.authToken });
        }
    }
 
    componentWillMount() {

        this.setState({
            skillId: this.props.data.skillId,
            skillName: this.props.data.skillName,
            authToken: this.props.authToken,
            skillDescription: this.props.data.skillDescription,
            efficiencyPercent: this.props.data.efficiencyPercent,
            efficiencyHrs: this.props.data.efficiencyHrs
        });

        this.props.setActionHandlers('confirm', () => {
            if (this.props.create) {
                api().setHeader('authorization',this.state.authToken);
                api().post('skill', {
                    skillName: this.state.skillName,
                    skillDescription: this.state.skillDescription,
                    efficiencyPercent: this.state.efficiencyPercent,
                    efficiencyHrs: this.state.efficiencyHrs
                }, {
                        headers:
                        { "content-type": 'application/json', "authorization": this.state.authToken }
                    }).then((response) => {
                        Alert.alert(
                            'Success',
                            'Saved Successfully',
                            [
                                { text: 'OK', onPress: () => NavigationActions.pop() }
                            ]);
                    });
            } else {
                api().setHeader('authorization',this.state.authToken);
                console.log(api());
                api().put('skill/' + this.state.skillId, {
                    skillId: this.state.skillId,
                    skillName: this.state.skillName,
                    skillDescription: this.state.skillDescription,
                    efficiencyPercent: this.state.efficiencyPercent,
                    efficiencyHrs: this.state.efficiencyHrs
                }, {
                        headers:
                        { "content-type": 'application/json', 'authorization': this.state.authToken }
                    }).then((response) => {
                        Alert.alert(
                            'Success',
                            'Updated Successfully',
                            [
                                { text: 'OK', onPress: () => NavigationActions.pop() }
                            ]);
                    });
            }
        });

        this.props.setActionHandlers('delete', () => {
            Alert.alert(
                'Delete',
                'Are you sure you want to Delete this Skill?',
                [
                    { text: 'Cancel', onPress: () => { } },
                    {
                        text: 'OK', onPress: () => {
                           api().delete('skill/' + this.state.skillId).then((response) => {
                                Alert.alert(
                                    'Success',
                                    'Successfully Deleted',
                                    [
                                        { text: 'OK', onPress: () => NavigationActions.pop() }
                                    ]);
                            });
                        }
                    },
                ]
            )
        });
    }
    render() {
        return (
            <ScrollView style={styles.parent}>
                <View style={[styles.topContainer, { flexDirection: this.state.flexDir }]}>
                    <View style={{ flex: 1, flexDirection: 'column', padding: 30, backgroundColor: 'white' }}>
                        {(!this.props.create) ?
                            <ScrollView>
                                <TextField
                                    label={'Skill ID'}
                                    labelStyle={{ color: 'black' }}
                                    highlightColor={'#00BCD4'}
                                    value={this.state.skillId}
                                    dense={true}
                                    />
                            </ScrollView>
                            : null}
                        <View style={{ flexDirection: 'row' }}>
                            <ScrollView style={{ flex: 1 }}>
                                <TextField
                                    label={'Skill Name'}
                                    labelStyle={{ color: 'black' }}
                                    highlightColor={'#00BCD4'}
                                    value={this.state.skillName}
                                    onChangeText={(text) => this.setState({ ...this.state, skillName: text })}
                                    dense={true}
                                />
                            </ScrollView>
                            <ScrollView style={{ flex: 1, marginLeft: 50 }}>
                                <TextField
                                    label={'Skill Description'}
                                    labelStyle={{ color: 'black' }}
                                    highlightColor={'#00BCD4'}
                                    value={this.state.skillDescription}
                                    onChangeText={(text) => this.setState({ ...this.state, skillDescription: text })}
                                    dense={true}
                                />
                            </ScrollView>
                        </View>
                         <View style={{ flexDirection: 'row' , paddingTop: 20}}>
                            <Text style={[styles.text,{paddingRight: 40, paddingLeft: 0}]}>Efficiency : </Text>
                            <TextInput
                                style={{height: 40, width: 100, borderColor: 'gray', borderWidth: 1, textAlign: 'center'}}
                                onChangeText={(efficiency) => this.setState({
                                    efficiencyPercent: efficiency,
                                    efficiencyHrs: ''
                                })}
                                value={this.state.efficiencyPercent}
                                underlineColorAndroid = 'transparent'
                                keyboardType = 'numeric'
                            />
                            <Text style={styles.text}>%</Text>

                            <Text style={[styles.text,{paddingRight: 40}]}>  OR </Text>
                            <TextInput
                                style={{height: 40, width: 100, borderColor: 'gray', borderWidth: 1, textAlign: 'center'}}
                                onChangeText={(efficiency) => this.setState({
                                    efficiencyHrs: efficiency,
                                    efficiencyPercent: ''
                                })}
                                value={this.state.efficiencyHrs}
                                underlineColorAndroid = 'transparent'
                                keyboardType =  'numeric'
                            />
                            <Text style={styles.text}>Hrs</Text>
                         </View>
                    </View>
                </View>
            </ScrollView>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        authToken: state.login.authToken,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(SkillView)


const styles = StyleSheet.create({
    parent: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: '#eeeeee',
        margin: 0,
        marginTop: 50,
        padding: 20,
    },
    photo: {
        margin: 40,
        height: 160,
        width: 160,
        borderRadius: 180,
        backgroundColor: '#01579b',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center'
    },
    topContainer: {
        flexDirection: 'row',
        backgroundColor: 'white',
        borderRadius: 5,
        elevation: 2
    },
    text:{
        paddingLeft:10, 
        paddingTop: 8,  
        color: 'black', 
        fontSize: 14, 
        paddingRight: 10
    }
});


