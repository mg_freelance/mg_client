import React, { Component } from 'react';
import { View, Text, StyleSheet, Image } from 'react-native';
import { Avatar, Drawer, Divider, PRIMARY_COLORS, TYPO, Ripple } from 'react-native-material-design';
import Icon from 'react-native-vector-icons/Ionicons';
import { Actions as NavigationActions } from 'react-native-router-flux'

export default class SkillRow extends React.Component {
    constructor(props) {
        super(props)
    }
    render() {
        return (
            <View style={styles.container}>
                <Ripple text="" style={styles.rippleContainer} color='rgba(0,0,0,.5)' onPress={()=>{NavigationActions.skillView({data:this.props.data})}}>
                    <Icon name="md-build" style={styles.photo} />
                    <Text style={styles.text}>
                        {this.props.data.skillName}
                    </Text>
                </Ripple>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
    },
    text: {
        marginLeft: 12,
        marginTop: 12,
        fontSize: 16,
    },
    photo: {
        
        height: 60,
        width: 60,
        paddingTop: 15,
        paddingLeft: 15,
        borderRadius: 60,
        backgroundColor: '#01579b',
        color: 'white',
        fontSize: 30
    },
    rippleContainer: {
        flex: 1,
        flexDirection: 'row',
        height: 90,
        padding: 12,
    }
});

