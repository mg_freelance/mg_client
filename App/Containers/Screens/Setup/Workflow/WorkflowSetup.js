import React, { Component } from 'react';
import { View, StyleSheet, Dimensions, Text, TouchableHighlight, TextInput, Alert } from 'react-native';
import { TabViewAnimated, TabBarTop } from 'react-native-tab-view';
import {Colors} from '../../../../Themes'
import ModalDropdown from 'react-native-modal-dropdown';
import Icon from 'react-native-vector-icons/MaterialIcons';
import SkillDeptView from './tabViews/SkillDeptView';
import BufferView from './tabViews/BufferView';
import CheckpointsView from './tabViews/CheckpointsView'
import KeptOpenView from './tabViews/KeptOpenView'
import { Actions as NavigationActions } from 'react-native-router-flux'
import {api} from '../../../../Lib/helpers'
import ChecklistModel from './tabViews/ChecklistModel'
import R from 'ramda';


let checkpointsData = {}

export default class WorkflowSetup extends Component {
    
    state = {
        index: 0,
        routes: [
            { key: '1', title: 'Skill-Dept. Mapping' },
            { key: '2', title: 'Tech kept open' },
            { key: '3', title: 'Buffers' },
            { key: '4', title: 'Checkpoints' }
            ],
        workshop: 'Select',
        WorkflowName: '',
        SkillDept: [],
        TechOpen: [],
        buffers:{},
        checklistData: [],
        departments:[],
        skillData:[],
        checkpoints: {},
        checkpointOrder:[],
        isOpen: false
        //departmentList:[]

    };

    componentDidMount(){
       api().get('checklists').then((response) => {
               
            api().get('departments').then((response2) => {

                api().get('skill').then((skillResponse)=>{
                     //const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
                    this.setState({
                        departments: response2.data,
                        checklistData: response.data,
                        skillData: skillResponse.data
                        //dataSource: ds.cloneWithRows(response.data),
                    })
                });
            });
                
        });
    }

    componentWillMount(){
        if(!this.props.create){
             api().get('workflow/'+ this.props.id).then((response) => {
                console.log(response.data[0].TechOpen)
                this.setState({
                    WorkflowName: response.data[0].workflowName,
                    SkillDept: response.data[0].SkillDept,
                    TechOpen: response.data[0].TechOpen,
                    buffers: response.data[0].Buffers,
                    checkpoints: response.data[0].checkpoints,
                    checkpointOrder: response.data[0].checkpointOrder
                });
            });
        }
        
        this.props.setActionHandlers('confirm', () => {

            if(this.state.WorkflowName == ''){
                    Alert.alert(
                        'Error',
                        'Wrokflow Name cannot be blank'
                    );
            }else{
                //console.log(this.state.TechOpen)
                let WorkFlow = {
                        workflowName: this.state.WorkflowName,
                        SkillDept: this.state.SkillDept,
                        TechOpen: this.state.TechOpen,
                        Buffers: this.state.buffers,
                        checkpoints: this.state.checkpoints,
                        checkpointOrder: this.state.checkpointOrder
                }
                
                if(this.props.create){
                        //console.log(JSON.stringify(Checklist))
                        api().post('workflow', JSON.stringify(WorkFlow))
                        .then((response) => {
                                //console.log(response.data)
                                Alert.alert(
                                    'Success',
                                    'Saved Successfully',
                                    [
                                        { text: 'OK', onPress: () => NavigationActions.pop() }
                                    ]);
                            });

                    }else{
                        api().put('workflow/' + this.props.id, JSON.stringify(WorkFlow))
                        .then((response) => {
                            //console.log(response.data)
                            Alert.alert(
                                'Success',
                                'Checklist Updated Successfully',
                                [
                                    {
                                        text: 'OK', onPress: () => {
                                            NavigationActions.pop();
                                        }
                                    },
                                ]
                            );
                        });
                    }

            }
        });
        
        this.props.setActionHandlers('delete', () => {
                 Alert.alert(
                    'Delete',
                    'Are you sure you want to Delete?',
                    [
                        { text: 'Cancel', onPress: () => { } },
                        {
                            text: 'OK', onPress: () => {
                                if(this.props.create){
                                    NavigationActions.pop();
                                }else{
                                    api().delete('workflow/'+ this.props.id).then((response) => {
                                        //console.log(response.data[0])
                                         NavigationActions.pop();
                                    });
                                }                               
                            }
                        },
                    ]
                )         
        });
    }

    _handleChangeTab = (index) => {
        this.setState({ index });
    };

    _renderHeader = (props) => {
        return <TabBarTop {...props}
        indicatorStyle={styles.indicator}
            style={styles.tabbar}
            labelStyle={styles.label}
        />;
    };

    _renderScene = ({ route }) => {
        switch (route.key) {
        case '1':
        return <SkillDeptView 
                    deptData ={this.state.departments} 
                    config = {this.state.SkillDept}
                    skills = {this.state.skillData}
                    setMap = {(mapSkill, mapDept)=>{
                        let mappedId = this.state.SkillDept.findIndex((skill)=>{
                            return skill.skillId == mapSkill
                        })
                        if(mappedId >= 0){
                            this.state.SkillDept[mappedId].departmentId = mapDept
                            this.forceUpdate()
                        }else{
                            this.state.SkillDept.push({skillId: mapSkill, departmentId: mapDept})
                            this.forceUpdate()
                        }
                        //console.log(this.state.SkillDept)
                    }}
                />
        case '2':
        return <KeptOpenView 
                    deptData ={this.state.departments} 
                    keptOpen= {this.state.TechOpen}
                    newData = {(data)=>{
                        console.log(data)
                        this.setState({
                            TechOpen: data
                        })
                    }}
                />
        case '3':
        return <BufferView bufferData={(buffers)=>{this.setState({buffers: buffers})}} data = {this.state.buffers}/>
        case '4':
        return <CheckpointsView 
                    data = {[this.state.checkpoints, this.state.checkpointOrder]}
                    checklistData = {this.state.checklistData}
                    modelState = {(isOpen)=>{this.toggleModal(isOpen)}}
                    onDelete = {(list, order)=>{
                        this.setState({
                            checkpoints: list,
                            checkpointOrder: order
                        })
                    }}
                />
        default:
        return null;
        }
    };
    

    dropDown(rowData) {
        return (
            <TouchableHighlight style={{ flex: 1}} >
                <View style={{ width: 200, flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center', padding: 10 }}>
                    <Text style={{fontSize: 16 }}>{rowData}</Text>
                </View>
            </TouchableHighlight>
        );
    }

    toggleModal(openStatus) {
        this.setState({
         
          isOpen: openStatus,
        });
    }

    render() {
        
        return (
            <View style = {{flex: 1}}>
                <View style = {{flexDirection: 'row',paddingBottom:10, marginTop: 65, borderBottomWidth: 2, borderBottomColor: '#D6D6D6'}}>
                    <Text style={{paddingLeft:20, paddingTop: 10,  color: 'black', fontSize: 16 }}>Workflow Name: </Text>
                     <TextInput
                        style={{marginLeft:20, height: 40, width: 200, borderColor: 'gray', borderWidth: 1, textAlign: 'center'}}
                        onChangeText={(WorkflowName) => this.setState({WorkflowName})}
                        value={this.state.WorkflowName}
                        underlineColorAndroid = 'transparent'
                    />
                </View>

                <TabViewAnimated
                    style={styles.container}
                    navigationState={this.state}
                    renderScene={this._renderScene}
                    renderHeader={this._renderHeader}
                    onRequestChangeTab={this._handleChangeTab}
                />

                <ChecklistModel
                    //width = {3*Dimensions.get('window').width/4}
                    //modelType={'Approve'}
                    title={''}
                    Checklists = {this.state.checklistData}
                    isOpen={this.state.isOpen}
                    onClosed={() => { this.toggleModal(false) } }
                    onApply={(listData)=>{
                            let lastKey = !R.isEmpty(this.state.checkpoints) ? Math.max(...this.state.checkpointOrder.map(Number)) :0 
                            this.state.checkpoints[lastKey+1] = listData
                            this.state.checkpointOrder.push(''+(lastKey+1))
                            this.toggleModal(false)
                        
                    }} 
                />
            </View>
      
        );
    }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ffffff',
    marginTop: 10
  },
  page: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  tabbar: {
    backgroundColor:  '#fefefe',
  },
  indicator: {
    backgroundColor: '#408fff',
  },
  label: {
    color: '#222',
    fontWeight: '400',
  },
  pickerBox:{
        height: 50,
        marginHorizontal: 10,
        borderColor: '#D6D6D6', 
        width :200,
        borderWidth: 1,
        flexDirection:'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    dropdown:{
        marginLeft: 5, 
        flex: 1, 
        width:Dimensions.get('window').width/8,
        flexDirection: 'row', 
        justifyContent: 'space-between', 
        alignItems: 'center',
    }
});