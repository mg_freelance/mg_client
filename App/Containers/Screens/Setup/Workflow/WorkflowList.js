import React, { Component } from 'react';
import { Colors, Metrics } from '../../../../Themes'
import {
    StyleSheet,
    ListView,
    Text,
    ScrollView,
    Alert,
    View,
    Dimensions
} from 'react-native';
import ActionButton from 'react-native-action-button';
import Icon from 'react-native-vector-icons/Ionicons';
import TextField from 'react-native-md-textinput';
import { Actions as NavigationActions } from 'react-native-router-flux'
import { connect } from 'react-redux'
import {api} from '../../../../Lib/helpers'
import WorkflowRow from './WorkflowRow'

export default class WorkFlowList extends React.Component{
    constructor(){
        super()
        const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
        this.state = {
            dataSource: ds.cloneWithRows([]),
        };
    }

    componentDidMount(){
        api().get('workflows').then((response) => {
            const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
            this.setState({
                dataSource: ds.cloneWithRows(response.data),
            });
        });
    }

    componentWillReceiveProps(newProps){
        console.log(newProps)
        this.componentDidMount()
    }

    render(){
        console.log(this.props)
        return(
            <View style={styles.container}>

                <ScrollView style={styles.container}>
                <ListView style={{ backgroundColor: Colors.background }}
                    enableEmptySections={true}
                    dataSource={this.state.dataSource}
                    renderRow={(rowData) => { return (<WorkflowRow data={rowData} />) } }
                >
                </ListView>
                </ScrollView>

                <ActionButton buttonColor="#408fff" onPress={()=>{NavigationActions.WorkflowSetup({create: true})}}>
            
                </ActionButton>
            
            </View>
        )
    }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,

  },
  separator: {
    flex: 1,
    height: StyleSheet.hairlineWidth,
    backgroundColor: '#8E8E8E',
  },
});