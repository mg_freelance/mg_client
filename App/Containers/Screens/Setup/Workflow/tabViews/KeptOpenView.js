import React, { Component } from 'react';
import { View, StyleSheet, Text, ScrollView, ListView } from 'react-native';
import {api} from '../../../../../Lib/helpers'
import DepartmentRow from './DepartmentRow'


export default class KeptOpenView extends Component {
    constructor(props){
        super(props)
        const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
        this.state = {
            data: [],
            selectedDept: '',
            deptData: [],
            dataSource: ds.cloneWithRows([])
        }
    };
    
    componentDidMount(){
        const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
        this.setState({
            dataSource: ds.cloneWithRows(this.state.deptData)
        })
    }

    componentWillReceiveProps(nextProps){
         //console.log(nextProps)
       
        if(nextProps != this.props){
            const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
            this.state.deptData = nextProps.deptData
            this.setState({
                deptData: this.state.deptData,
                data: nextProps.keptOpen,
                dataSource: ds.cloneWithRows(this.state.deptData),
            });
        }
        //this.render()
    };   
   
    render(){
        return (
             <View style = {styles.container}>

                <View style={{flex: 1, flexDirection: 'column', margin: 5, borderWidth: 2, borderColor: '#D6D6D6', marginRight: 0}}>
                    <View style = {{padding: 20, flexDirection: 'row',paddingBottom:10, borderBottomWidth: 2, borderBottomColor: '#D6D6D6', backgroundColor: '#F6F6F6', justifyContent:'center'}}>
                        <View>
                            <Text style={{ paddingBottom: 10,  color: 'black', fontSize: 16 }}>Tech Department Kept Open </Text>
                        </View>
                    </View>
                     <ScrollView style={styles.list}>
                       
                        {<ListView 
                            style={styles.list}
                            enableEmptySections={true}
                            dataSource={this.state.dataSource}
                            
                            renderRow={
                                (rowData) =>                               
                                    <DepartmentRow
                                    //detData={console.log(this.state.departments)} 
                                        data={[rowData, this.state.data]} 
                                        isDuring={false} 
                                        selected={(state)=>{
                                            if(state){
                                                const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
                                                this.setState({
                                                    selectedDept : rowData,
                                                    dataSource: ds.cloneWithRows(this.state.deptData)
                                                })
                                            }
                                        }}
                                    
                                    />
                             } 
                        />}                            
                       
                    </ScrollView>
                </View>

                <View style={{flex: 1, flexDirection: 'column', margin: 5, borderWidth: 2, borderColor: '#D6D6D6', marginLeft: 0}}>
                    <View style = {{padding: 20, flexDirection: 'row',paddingBottom:10, borderBottomWidth: 2, borderBottomColor: '#D6D6D6', backgroundColor: '#F6F6F6', justifyContent:'center'}}>
                        <View>
                            <Text style={{ paddingBottom: 10,  color: 'black', fontSize: 16 }}>During </Text>
                        </View>
                    </View>
                     <ScrollView style={styles.list}>
                        <ListView style={styles.list}
                            enableEmptySections={true}
                            dataSource={this.state.dataSource}
                            renderRow={(rowData) => { return (
                                <DepartmentRow 
                                    //departmentData={this.state.departments}
                                    data={[rowData, this.state.data]} 
                                    isDuring={true} 
                                    exclude = {this.state.selectedDept}
                                    onOpenChange = {(value, duringId, dept)=>{
                                        const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
                                        let index = this.state.data.findIndex((row)=>{
                                                       return row.departmentId == dept.departmentId
                                                    })
                                       
                                        if(value){

                                            if(index == -1){
                                                this.state.data.push({
                                                    departmentId: dept.departmentId, 
                                                    open: [duringId]
                                                })
                                            }
                                            else{
                                                 this.state.data[index].open.push(duringId)
                                            }
                                           
                                        }else{
                                            let newOpen = this.state.data[index].open.filter((id)=>{
                                                return (id != duringId)
                                            })
                                            
                                            if(newOpen == []){
                                                this.state.data.splice(index,1)
                                            }else{
                                                this.state.data[index].open = newOpen
                                            }
                                        }
                                        //console.log(this.state.data)
                                        this.props.newData(this.state.data)
                                    }}
                                    
                            />) } }
                            >
                        </ListView>
                    </ScrollView>
                </View>


            </View>
        );
    }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row'
  },
  list: {
    backgroundColor:  'transparent',
    //flexDirection: 'row'
  },
  label: {
    color: '#222',
    fontWeight: '400',
  },
  pickerBox:{
        height: 50,
        marginHorizontal: 10,
        borderColor: '#D6D6D6', 
        width :200,
        borderWidth: 1,
        flexDirection:'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    text:{
        paddingLeft:10, 
        paddingTop: 8,  
        color: 'black', 
        fontSize: 16, 
        paddingRight: 10
    }
});