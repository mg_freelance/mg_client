import React, { Component } from 'react';
import { View, StyleSheet, Text, ScrollView, ListView, TouchableHighlight, Dimensions} from 'react-native';
import {Ripple} from 'react-native-material-design'
import ModalDropdown from 'react-native-modal-dropdown';
import Icon from 'react-native-vector-icons/MaterialIcons';
import { Actions as NavigationActions } from 'react-native-router-flux'

export default class CheckPointsRow extends React.Component{

    constructor(){
        super()
        this.state = {
            enablesAt: 'Starting Job',
            checklistnames: [],
            checklistId: ''
        }
    }

    componentDidMount(){
        //console.log(this.props.checklists)
        this.props.checklists.forEach((data)=>{
            this.state.checklistnames[data.checkListId] = data.checkListName
               
        })
        //console.log(this.state.checklistnames)
        this.setState({
            enablesAt: this.props.data[1],
            checklistnames: this.state.checklistnames,
            checklistId: this.props.data[0]
        })
         
    }

    componentDidReceiveProps(nextProps) {
        console.log(nextProps)
    }



    dropDown(rowData) {
        return (
            <TouchableHighlight style={{ flex: 1}} >
                <View style={{ width: 200, flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center', padding: 10 }}>
                    <Text style={{fontSize: 16 }}>{rowData}</Text>
                </View>
            </TouchableHighlight>
        );
    }

    render(){
        return(
            <TouchableHighlight
                underlayColor={'#eee'}
                delayLongPress={500} 
                style={styles.container}
                onPress = {()=>{
                     NavigationActions.ChecklistSetup({create: false, id: this.state.checklistId})
                }} 
                {...this.props.sortHandlers}
            >
                <View style = {{flexDirection: 'row'}}>
                    <View style={{flex:3}}>
                         <Text style = {styles.text}>{this.state.checklistnames[this.state.checklistId]}</Text>
                    </View>
                    <View style={{flex:1}}>
                         <ModalDropdown
                            style={styles.pickerBox}
                            options={['Starting Job', 'Completing Job']}
                            onSelect={(idx, value) => {
                                this.props.enablesAt(value)
                                this.setState({
                                    enablesAt: value 
                                })
                            }}
                            renderRow ={this.dropDown.bind(this)}
                            >
                            <View style={styles.dropdown}>
                                <Text style={{paddingLeft:20,  color: 'black', fontSize: 16 }}>
                                    {this.state.enablesAt}
                                </Text>
                                <Icon name="arrow-drop-down" size={20} color="#000000" />
                            </View>
                        </ModalDropdown>
                    </View>
                    
                    <View style={{width: 100, alignItems: 'flex-end', paddingBottom: -10}}>
                        <Ripple style={styles.RippleStyle} color='rgba(0 ,0 ,0 ,.5)' onPress={()=>{
                            this.props.onIconPress(this.props.data[0], this.state.enablesAt)}}>
                            <Icon name="close" size={20} color="#000000" />
                        </Ripple>
                    </View>
                </View>
               
            </TouchableHighlight>
        )
    }
}

const styles = StyleSheet.create({
  container: {
      backgroundColor: '#fdfdfd', 
      height: 60, 
      padding: 10, 
      margin:5,
      borderWidth: 3, 
      borderTopWidth: 0, 
      borderColor:'#dddddd'
    },
    text:{
        paddingLeft:10, 
        paddingTop: 8,  
        color: 'black', 
        fontSize: 16, 
        paddingRight: 10
    },
    pickerBox:{
        flex:1,
        marginHorizontal: 10,
        borderColor: '#D6D6D6', 
        borderWidth: 1,
        flexDirection:'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        width: 170,
       height:35
    },
    dropdown:{
        
        flex: 1, 
        width: Dimensions.get('window').width/8,
        flexDirection: 'row', 
        justifyContent: 'space-between', 
        alignItems: 'center',
    },
    RippleStyle:{
        flex:1, 
        width: 50, 
        alignItems: 'center', 
        justifyContent: 'center', 
        paddingBottom: 10,
    }
  
});