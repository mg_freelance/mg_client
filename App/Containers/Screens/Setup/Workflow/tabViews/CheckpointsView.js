import React, { Component } from 'react';
import { View, StyleSheet, Text, ScrollView, ListView } from 'react-native';
import CheckpointsRow from './CheckpointsRow'
import SortableListView from 'react-native-sortable-listview'
import ActionButton from 'react-native-action-button';

export default class CheckpointsView extends Component {

    constructor(props){
        super(props);
        //const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
        this.state = {
            checkListData: {},
            order : []
        };
    }
    
    componentDidMount(){
        this.setState({
            order : Object.keys(this.state.checkListData)
        })
        
    }

    componentWillReceiveProps(nextProps) {
        if(nextProps != this.props){
           //console.log(nextProps)
            
            this.setState({ 
                checkListData: nextProps.data[0],
                order: nextProps.data[1],
                ChecklistList: nextProps.checklistData
            })

        }
    }


    render(){
        return (
            
            <View style = {styles.container}>
                 <View style = {{padding: 20, flexDirection: 'row',paddingBottom:10, borderBottomWidth: 2, borderBottomColor: '#D6D6D6', backgroundColor: '#F6F6F6'}}>
                    <View style={{flex:3}}>
                        <Text style={{ paddingBottom: 10,  color: 'black', fontSize: 16 }}>CheckList(s) </Text>
                    </View>
                    <View style={{flex:1}}>
                        <Text style={{ paddingBottom: 10,  color: 'black', fontSize: 16 }}>Enables </Text>
                    </View>
                </View>

                <SortableListView
                    style={[styles.container, {paddingTop: 5}]}
                    data={this.state.checkListData}
                    order={this.state.order}
                    onRowMoved={(e) => {
                        this.state.order.splice(e.to, 0, this.state.order.splice(e.from, 1)[0]);
                        this.forceUpdate();
                    }}
                    renderRow={(row) =>  
                        <CheckpointsRow 
                            checklists = {this.state.ChecklistList}
                            data = {row}
                            onIconPress = {(id, enablePos)=>{
                                for([key, value] of Object.entries(this.state.checkListData)){
                                   
                                   if(value[0] == id && value[1] == enablePos){
                                       delete this.state.checkListData[key]
                                       let order = this.state.order.reduce((newOrder, data)=>{
                                           if(data != key){
                                               newOrder.push(data)
                                           }
                                           return newOrder
                                       },[])//Object.keys(this.state.listData)
                                       //console.log(order)
                                      
                                       //this.forceUpdate()
                                       this.props.onDelete(this.state.checkListData, order)
                                    }
                                }
                            }}
                            enablesAt ={(enabled)=>{
                                for([key, value] of Object.entries(this.state.checkListData)){
                                   if(value[0].title == row[0].title){
                                       let val = enabled
                                       //console.log(val)
                                      this.state.checkListData[key][1] = enabled
                                       //console.log(this.state.checkListData)
                                       this.forceUpdate()
                                    }
                                }
                            }} 
                        />}
                    
                />

                <ActionButton buttonColor="#408fff" onPress={()=>{this.props.modelState(true)}}>
            
                </ActionButton>

                
                
            </View>
        );
    }
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F6F6F6',
  },
  tabbar: {
    backgroundColor:  '#efefef',
  },
  label: {
    color: '#222',
    fontWeight: '400',
  },
  pickerBox:{
        height: 50,
        marginHorizontal: 10,
        borderColor: '#D6D6D6', 
        width :200,
        borderWidth: 1,
        flexDirection:'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    text:{
        paddingLeft:10, 
        paddingTop: 8,  
        color: 'black', 
        fontSize: 16, 
        paddingRight: 10
    }
});

/**
 * 
 * <ScrollView style={[styles.container, {paddingTop: 5}]}  onScroll= {()=>{}}>
                    <ListView style={{ backgroundColor: 'transparent' }}
                        dataSource={this.state.dataSource}
                        enableEmptySections={true}
                        renderRow={
                            (rowData) => { 
                                return (
                                    <CheckpointsRow 
                                        
                                    />
                                ) 
                            }   
                        }
                        
                        //renderSeparator={(sectionId, rowId) => <View key={rowId} style={styles.separator} />}
                        >
                    </ListView>
                </ScrollView>
 */