import React, { Component } from 'react';
import {  
    StyleSheet,
    ListView,
    Text,
    ScrollView,
    View,
    Dimensions
} from 'react-native';
import {api} from '../../../../../Lib/helpers'
import SkillDeptRow from './SkillDeptRow'


export default class SkillDeptView extends Component {
    constructor(props){
        super(props)
        const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
        this.state = {
            dataSource: ds.cloneWithRows([]),
            departmentData : [],
            SkillDeptMap: []
        };
    }

    // componentDidMount(){
    //     api().get('skill').then((response) => {
    //         const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });

    //         api().get('departments').then((deptResponse)=>{
    //             this.setState({
    //                 departmentData: deptResponse.data,
    //                 dataSource: ds.cloneWithRows(response.data),
    //             });
    //         })
            
    //     });
    // }
    
    componentWillReceiveProps(newProps){
       if(newProps != this.props){
           const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
           this.setState({
               SkillDeptMap: newProps.config,
               departmentData: newProps.deptData,
               dataSource: ds.cloneWithRows(newProps.skills)
           })
        }
    }
    
    render(){
        return (
            <View style= {styles.container}>
                <View style = {{padding: 20, flexDirection: 'row',paddingBottom:10, borderBottomWidth: 2, borderBottomColor: '#D6D6D6', backgroundColor: '#F6F6F6'}}>
                    <View style={{flex:3}}>
                        <Text style={styles.text}>Skill </Text>
                    </View>
                    <View style={{flex:1}}>
                        <Text style={{ paddingBottom: 10,  color: 'black', fontSize: 16 }}>Department </Text>
                    </View>
                </View>
                 <ScrollView style={styles.container} >
                    <ListView style={{ backgroundColor: 'transparent' }}
                        dataSource={this.state.dataSource}
                        enableEmptySections={true}
                        renderRow={
                            (rowData) => { 
                                return (
                                   <SkillDeptRow 
                                        data = {rowData} 
                                        deptData = {this.state.departmentData} 
                                        mapData= {this.state.SkillDeptMap}
                                        enablesAt = {(deptId)=>{
                                            this.props.setMap(rowData.skillId, deptId)
                                        }}
                                    />
                                ) 
                            }   
                        }
                        //renderSeparator={(sectionId, rowId) => <View key={rowId} style={styles.separator} />}
                        >   
                    </ListView>
                </ScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F6F6F6',
  },
  tabbar: {
    backgroundColor:  '#efefef',
  },
  label: {
    color: '#222',
    fontWeight: '400',
  },
  pickerBox:{
        height: 50,
        marginHorizontal: 10,
        borderColor: '#D6D6D6', 
        width :200,
        borderWidth: 1,
        flexDirection:'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    text:{ 
        paddingBottom: 10,  
        color: 'black', 
        fontSize: 16,
        paddingLeft: 30 
    }
});