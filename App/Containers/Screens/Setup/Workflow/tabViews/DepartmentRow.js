import React, { Component } from 'react';
import { View, Text, StyleSheet, Image, Switch } from 'react-native';
import { Avatar, Drawer, Divider, PRIMARY_COLORS, TYPO, Ripple } from 'react-native-material-design';
import Icon from 'react-native-vector-icons/Ionicons';
import { Actions as NavigationActions } from 'react-native-router-flux'



export default class DepartmentRow extends React.Component {
    constructor(props) {
        super(props)
       
        this.state = {
            during: false,
            department: {},
            openDepts:[],

        }
    }

    componentDidMount(){
        this.setState({
            during: this.getOpenState()
        })
       
        //  this.props.data[1].forEach((row)=>{
        //         this.state.departmentNames[row.departmentId] = row.departmentName                
        //     })
      
        // this.setState({
        //     departmentNames: this.state.departmentNames,
        //     openDepts: this.props.data.open,
        //     during: this.getDuringState()
        // })
    }

    componentWillReceiveProps(nextProps){
        //console.log(nextProps)
        if(nextProps != this.props){
            this.setState({
                //enablesAt: this.props.data[1],
                department: nextProps.data[0],
                openDepts: nextProps.data[1],
                during: this.getOpenState()
            })
        }         
    }
    
    // getDuringState(){
    //     if(this.state.openDepts == [] || this.state.openDepts == undefined){
    //         return false
    //     }else{
    //         this.state.deprtment

    //         this.state.openDepts.find((value)=>{
    //             if(value == this.props.data[0].departmentId){
    //                 return true
    //             }
    //         })
    //     }
    // }

    getOpenState(){
        if(this.props.data != null && this.props.exclude != null){
            //console.log(this.props.data)
            let isOpen = this.props.data[1].find((dept)=>{
                                //console.log(dept)
                                return this.props.exclude.departmentId == dept.departmentId 
                            })
            
            if(isOpen != null){
               
                let value = isOpen.open.find((dpt)=>{
                                return this.state.department.departmentId == dpt
                            })
                return (value != null) ? true: false
             }  
        }
        return false
    }

    getRow(){
        
        if(!this.props.isDuring){
        
           return ( 
                <View style={styles.container}>
                    <Ripple text="" style={styles.rippleContainer} color='rgba(0,0,0,.5)' onPress={()=>{
                        this.props.selected(true)
                       
                        }}>
                            <Text style={styles.text}>
                                {this.state.department.departmentName}
                            </Text>
                    </Ripple>
                </View>
            )
        }else{
            
            if(this.props.exclude == '' || this.props.data[0].departmentId == this.props.exclude.departmentId){
                return <View></View>
            }else{
                //console.log(this.props)
                return (
                    <View style={styles.container}>
                        <View style={styles.rippleContainer} color='rgba(0,0,0,.5)'>
                            <View style={{flex : 4, flexDirection: 'row'}}>
                                <Text style={styles.text}>
                                   {this.state.department.departmentName}
                                </Text>
                            </View>
                            <View style = {{flex: 1}}>
                                <Switch
                                    onValueChange={(value) =>{ 
                                        //console.log(this.props.exclude)
                                        this.props.onOpenChange(value, this.props.data[0].departmentId, this.props.exclude)
                                        this.setState({during: value})
                                    }}
                                    style={{paddingTop: 30, marginRight: 30}}
                                    value={this.state.during} 
                                />
                            </View>
                        </View>
                    </View>
                )
            }
            
        }
    }
    render() {
        this.state.during = this.getOpenState()
        return(
             this.getRow()
        )
            
    }
}

// {this.state.departmentNames[this.props.data[0].departmentId]}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        borderWidth: 2, 
        borderColor: '#D6D6D6',
        borderTopWidth: 0,
        margin: 5,
        marginVertical: 2
    },
    text: {
        marginLeft: 12,
        marginTop: 20,
        fontSize: 16,
        color: 'black',
    
    },
    // photo: {
    //     height: 60,
    //     width: 60,
    //     paddingTop: 10,
    //     paddingLeft: 15,
    //     borderRadius: 60,
    //     backgroundColor: '#01579b',
    //     color: 'white',
    //     //fontSize: 50
    // },
    rippleContainer: {
        flex: 1,
        flexDirection: 'row',
        height: 70,
        padding: 5,
        paddingLeft: 30,
        
    }
});