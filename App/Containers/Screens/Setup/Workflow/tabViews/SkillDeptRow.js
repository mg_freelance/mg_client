import React, { Component } from 'react';
import { View, Text, StyleSheet, Image, Dimensions, TouchableHighlight } from 'react-native';
import { Avatar, Drawer, Divider, PRIMARY_COLORS, TYPO, Ripple } from 'react-native-material-design';
import { Actions as NavigationActions } from 'react-native-router-flux';
import ActionButton from 'react-native-action-button';
import Icon from 'react-native-vector-icons/MaterialIcons';
import ModalDropdown from 'react-native-modal-dropdown';


export default class SkillDeptRow extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            department: 'not allowed'
        }
    }

    componentDidMount(){
        this.getEnabled()
    }

    componentWillReceiveProps(newProps){
        if(newProps != this.props){
            this.getEnabled()
        }
    }

    getEnabled(){
       
        let skillMapped = this.props.mapData.find((mapping)=>{
            return (mapping.skillId == this.props.data.skillId)
        })
      

        if(skillMapped != null){
            let dept = this.props.deptData.find((deptRow)=>{
                return deptRow.departmentId == skillMapped.departmentId
            })
            
            if(dept != null){
                
                this.setState({
                    department: dept.departmentName
                })
            }
        }
    }

    dropDown(rowData) {
        return (
            <TouchableHighlight style={{ flex: 1}} >
                <View style={{ width: 200, flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center', padding: 10 }}>
                    <Text style={{fontSize: 16 }}>{rowData.departmentName}</Text>
                </View>
            </TouchableHighlight>
        );
    }

   

    render() {
        return (
            <View style={styles.container}>
                <View style = {{flex: 2}}>
                    <Text style ={styles.text}>{this.props.data.skillName}</Text>
                </View>
                <View style={{flex: 1}}> 
                    <ModalDropdown
                        style={styles.pickerBox}
                        options={this.props.deptData}
                        onSelect={(idx, value) => {
                            this.props.enablesAt(value.departmentId)
                            this.setState({
                                department: value.departmentName
                            })
                        }}
                        renderRow ={this.dropDown.bind(this)}
                    >
                    <View style={styles.dropdown}>
                        <Text style={{paddingLeft:20,  color: 'black', fontSize: 16 }}>
                            {this.state.department}
                        </Text>
                            <Icon name="arrow-drop-down" size={20} color="#000000" />
                        </View>
                    </ModalDropdown>
                </View>              
            </View>
        );
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        margin: 5,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        backgroundColor: 'white',
        borderWidth: 2,
        borderColor: '#D6D6D6',
        height: 60,
        borderTopWidth: 0
    },
    text: {
        marginLeft: 40,
        marginTop: 0,
        fontSize: 16,
    },
    photo: {
        height: 60,
        width: 60,
        borderRadius: 60,
    },
    rippleContainer: {
        flex: 1,
        flexDirection: 'row',
        height: 80,
        padding: 12,
    },
    pickerBox:{
        flex:1,
        marginLeft: 60,
        marginVertical: 10,
        borderColor: '#D6D6D6', 
        borderWidth: 1,
        flexDirection:'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        width: 170,
        
    },
    dropdown:{        
        flex: 1, 
        width: Dimensions.get('window').width/8,
        flexDirection: 'row', 
        justifyContent: 'space-between', 
        alignItems: 'center',
    },
});



/**
 * 
 * <Ripple style={styles.rippleIconcontainer} color='rgba(0,0,0,.5)'>
                             <Image source={Images.ApproveIcon} style={[styles.photo]} />
                        </Ripple>
                         <Ripple style={styles.rippleIconcontainer} color='rgba(0,0,0,.5)'>
                             <Image source={Images.RejectIcon} style={[styles.photo]} />
                        </Ripple>
 */