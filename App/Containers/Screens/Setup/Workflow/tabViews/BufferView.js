import React, { Component } from 'react';
import { View, StyleSheet, TextInput,Text  } from 'react-native';

export default class BufferView extends Component {
    constructor(){
        super()
        this.state ={
            fixedTime: '',
            Percentage:  '',
        }
    }

    componentWillReceiveProps(newProps){
        if(newProps != this.props){
            this.setState({
                fixedTime: newProps.data.fixedTime,
                Percentage: newProps.data.Efficiency
            })
        }
    }

    render(){
        //
        return (
            <View style= {styles.container}>
                <View style = {{flexDirection: 'row', marginTop: 30, paddingLeft: 20}}>
                    <View style={{flex: 2, flexDirection: 'row'}}>
                        <Text style={[styles.text]}>Fixed time : </Text>
                        <TextInput
                            style={{height: 40, width: 100, borderColor: 'gray', borderWidth: 1, textAlign: 'center'}}
                            onChangeText={(time) =>{ 
                                //this.setState()
                                this.props.bufferData({
                                    fixedTime: time,
                                    Efficiency: '',
                                })
                            }}
                            value={this.state.fixedTime}
                            underlineColorAndroid = 'transparent'
                            keyboardType = 'numeric'
                        />
                        <Text style={[styles.text,{paddingLeft: 40}]}>  OR </Text>
                        <Text style={[styles.text,{paddingLeft: 40}]}>% of Dept. Efficiency : </Text>
                        <TextInput
                            style={{height: 40, width: 100, borderColor: 'gray', borderWidth: 1, textAlign: 'center'}}
                            onChangeText={(time) => {
                                //this.setState()
                                this.props.bufferData({
                                    fixedTime: '',
                                    Efficiency: time
                                })
                            }}
                            value={this.state.Percentage}
                            underlineColorAndroid = 'transparent'
                            keyboardType =  'numeric'
                        />
                    </View>               
                </View>

            </View>
            );
    }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ffffff',
  },
    text:{
        paddingLeft:10, 
        paddingTop: 8,  
        color: 'black', 
        fontSize: 16, 
        paddingRight: 10
    }
});