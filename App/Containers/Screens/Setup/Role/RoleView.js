import React, { Component } from 'react';
import { Colors, Metrics } from '../../../../Themes'
import {
    StyleSheet,
    ListView,
    Text,
    ScrollView,
    View,
    Picker,
    Alert,
    Dimensions
} from 'react-native';
import ActionButton from 'react-native-action-button';
import Icon from 'react-native-vector-icons/Ionicons';
import TextField from 'react-native-md-textinput';
import {api} from '../../../../Lib/helpers';
import { Actions as NavigationActions } from 'react-native-router-flux'
var flexDir = ''

export default class RoleView extends Component {
     constructor(props) {
        super(props);
        this.state = {
            flexDir: Dimensions.get('window').width< 500 ? 'column' : 'row'
        }
    }

    // aligncomponents = event => {
    //          this.setState({
    //              flexDir:  event.nativeEvent.layout.width<500? 'column': 'row'
    //          })
    // }

    componentWillMount() {
        this.setState({
            roleId: this.props.data.roleId,
            roleName: this.props.data.roleName,
            roleDescription: this.props.data.roleDescription
        });

        this.props.setActionHandlers('delete', () => {
            api().delete('role/' + this.state.roleId).then((response) => {
                Alert.alert(
                    'Success',
                    'Successfully Deleted',
                    [
                        { text: 'OK', onPress: () => NavigationActions.pop() }
                    ]);
            });
        });

        this.props.setActionHandlers('confirm', () => {
            if (this.props.create) {

                api().post('role', {
                    roleName: this.state.roleName,
                    roleDescription: this.state.roleDescription
                },
                    {
                        headers:
                        { "content-type": 'application/json' }
                    }).then((response) => {
                        Alert.alert(
                            'Success',
                            'Saved Successfully',
                            [
                                { text: 'OK', onPress: () => NavigationActions.pop() }
                            ]);
                    });
            } else {
                api().put('role/' + this.state.roleId, {
                    roleId: this.state.roleId,
                    roleName: this.state.roleName,
                    roleDescription: this.state.roleDescription
                },
                    {
                        headers:
                        { "content-type": 'application/json' }
                    }).then((response) => {
                        Alert.alert(
                            'Success',
                            'Updated Successfully',
                            [
                                { text: 'OK', onPress: () => NavigationActions.pop() }
                            ]);
                    });
            }
        });
        //  
    }
    render() {
        return (
            <ScrollView style={styles.parent }>
                <View style={[styles.topContainer, {flexDirection: this.state.flexDir}]}>
                    <View style={{ flex: 1, flexDirection: 'column', padding: 30, backgroundColor: 'white' }}>
                        {(!this.props.create) ?
                            <ScrollView>
                                <TextField
                                    label={'Role ID'}
                                    labelStyle={{ color: 'black' }}
                                    highlightColor={'#00BCD4'}

                                    value={this.state.roleId}
                                    dense={true}
                                    editable={false}
                                    />
                            </ScrollView> : null
                        }
                        <View style={{ flexDirection: 'row' }}>
                            <ScrollView style={{ flex: 1 }}>
                                <TextField
                                    label={'Role Name'}
                                    labelStyle={{ color: 'black' }}
                                    highlightColor={'#00BCD4'}
                                    onChangeText={(text) => this.setState({ ...this.state, roleName: text })}
                                    value={this.state.roleName}
                                dense={true}
                                />
                            </ScrollView>
                            <ScrollView style={{ flex: 1 }}>
                                <TextField
                                    label={'Role Description'}
                                    labelStyle={{ color: 'black' }}
                                    highlightColor={'#00BCD4'}
                                    onChangeText={(text) => this.setState({ ...this.state, roleDescription: text})}
                                    value={this.state.roleDescription}
                                dense={true}
                                />
                            </ScrollView>
                        </View>
                    </View>
                </View>
            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    parent: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: '#eeeeee',
        margin: 0,
        marginTop: 50,
        padding: 20,
    },
    photo: {
        margin: 40,
        height: 160,
        width: 160,
        borderRadius: 180,
        backgroundColor: '#01579b',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center'
    },
    topContainer: {
        flexDirection: 'row',
        backgroundColor: 'white',
        borderRadius: 5,
        elevation: 2
    }
});