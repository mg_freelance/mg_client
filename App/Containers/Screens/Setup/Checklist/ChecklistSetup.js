import React, { Component } from 'react';
import { View, StyleSheet, Dimensions, Text, TouchableHighlight, TextInput, Alert } from 'react-native';
import { TabViewAnimated, TabBarTop } from 'react-native-tab-view';
import {Colors} from '../../../../Themes'
import ModalDropdown from 'react-native-modal-dropdown';
import Icon from 'react-native-vector-icons/MaterialIcons';
import SortableListView from 'react-native-sortable-listview'
import ActionButton from 'react-native-action-button';
import ChecklistItem from './ChecklistItem'
import ItemModel from './ItemModel'
import { Actions as NavigationActions } from 'react-native-router-flux'
import {api} from '../../../../Lib/helpers';
import R from 'ramda';

let setupData = {}
let checklistItemData = {}

export default class ChecklistSetup extends Component {
    
    state = {
        index: 0,
        ChecklistName: '',
        itemList : {},
        order : [],
        itemId: 0,
        itemName: '',
        itemtype: '',
        isOpen: false
    };

    componentDidMount(){
        this.setState({
            order : Object.keys(this.state.itemList)
        })
        
    }
    

    componentWillMount(){

        if(!this.props.create){
            api().get('checklist/'+ this.props.id).then((response) => {
                //console.log(response.data[0])
                this.setState({
                    ChecklistName: response.data[0].checkListName,
                    itemList: response.data[0].items,
                    order: response.data[0].checkListOrder

                });
            });
        }
        
        this.props.setActionHandlers('confirm', () => {

            if(this.state.ChecklistName == ''){
                Alert.alert(
                    'Error',
                    'Checklist Name cannot be blank'
                );
            }else{
                let Checklist = { checkListName: this.state.ChecklistName, items: this.state.itemList, checkListOrder: this.state.order}
                if(this.props.create){
                    //console.log(JSON.stringify(Checklist))
                    api().post('checklist', JSON.stringify(Checklist))
                    .then((response) => {
                            //console.log(response.data)
                            Alert.alert(
                                'Success',
                                'Saved Successfully',
                                [
                                    { text: 'OK', onPress: () => NavigationActions.pop() }
                                ]);
                        });

                }else{
                    //console.log(this.props.id)
                    api().put('checklist/' + this.props.id, JSON.stringify(Checklist))
                    .then((response) => {
                        //console.log(response.data)
                        Alert.alert(
                            'Success',
                            'Checklist Updated Successfully',
                            [
                                {
                                    text: 'OK', onPress: () => {
                                        NavigationActions.pop();
                                    }
                                },
                            ]
                        );
                    });

                }
            }
        });
        this.props.setActionHandlers('delete', () => {
                 Alert.alert(
                    'Delete',
                    'Are you sure you want to Delete?',
                    [
                        { text: 'Cancel', onPress: () => { } },
                        {
                            text: 'OK', onPress: () => {
                                if(this.props.create){
                                    NavigationActions.pop();
                                }else{
                                    api().delete('checklist/'+ this.props.id).then((response) => {
                                        //console.log(response.data[0])
                                         NavigationActions.pop();
                                    });
                                }                               
                            }
                        },
                    ]
                )

            
           
        });
    }

    toggleModal(openStatus) {
        this.setState({
          itemId: 0,
          itemName: '',
          itemtype: 'checkbox',
          isOpen: openStatus,
        });
    }
    

    render() {
        //console.log(setupData)
        return (
            <View style = {{flex: 1}}>
                <View style = {{flexDirection: 'row',paddingBottom:10, marginTop: 65, borderBottomWidth: 2, borderBottomColor: '#D6D6D6'}}>
                    <Text style={{paddingLeft:20, paddingTop: 10,  color: 'black', fontSize: 16 }}>Checklist Name: </Text>
                     <TextInput
                        style={{marginLeft:20, height: 40, width: 200, borderColor: 'gray', borderWidth: 1, textAlign: 'center'}}
                        onChangeText={(ChecklistName) => this.setState({ChecklistName})}
                        value={this.state.ChecklistName}
                        underlineColorAndroid = 'transparent'
                    />
                </View>

                <View style = {{padding: 20, flexDirection: 'row',paddingBottom:10, borderBottomWidth: 2, borderBottomColor: '#D6D6D6', backgroundColor: '#F6F6F6'}}>
                    <View style={{flex:3}}>
                        <Text style={{ paddingBottom: 10, paddingLeft: 30,  color: 'black', fontSize: 18 }}>Items</Text>
                    </View>
                    <View style={{flex:1}}>
                        <Text style={{ paddingBottom: 10,  color: 'black', fontSize: 18 }}>Type </Text>
                    </View>
                </View>

                <SortableListView
                    style={[styles.container, {paddingTop: 5}]}
                    data={this.state.itemList}
                    order={this.state.order}
                    onRowMoved={(e) => {
                        this.state.order.splice(e.to, 0, this.state.order.splice(e.from, 1)[0]);
                        this.forceUpdate();
                    }}
                    renderRow={(row) =>  
                        
                        <ChecklistItem 
                            data = {row}
                            onIconPress = {(idx)=>{
                                for([key, value] of Object.entries(this.state.itemList)){
                                   if(value.itemId == idx){
                                       delete this.state.itemList[key]
                                       this.state.order = this.state.order.reduce((newOrder, data)=>{
                                           if(data != key){
                                               newOrder.push(data)
                                           }
                                           return newOrder
                                       },[])//Object.keys(this.state.listData)
                                       this.forceUpdate()
                                    }
                                }
                            }}
                            itemType ={(selected)=>{
                                for([key, value] of Object.entries(this.state.itemList)){
                                   if(value.itemId == row.itemId){
                                       //let val = selected
                                       //console.log(val)
                                      this.state.itemList[key].type = selected
                                       //console.log(this.state.checkListData)
                                       this.forceUpdate()
                                    }
                                }
                            }}
                            onPress={(IName, IType, id)=>{
                                //console.log(IType)
                                this.setState({
                                    itemId: id,
                                    itemName: IName,
                                    itemtype: IType,
                                    isOpen: true
                                })
                                //this.toggleModal(true)
                            }}
                            listItems = {(data, order)=>{checklistItemData = {data, order}}} 
                        />}
                    
                />

                <ActionButton buttonColor="#408fff" onPress={()=>{this.toggleModal(true)}}>
            
                </ActionButton>

               <ItemModel
                    //width = {3*Dimensions.get('window').width/4}
                    //modelType={'Approve'}
                    title={''}
                    itemData = {[this.state.itemName, this.state.itemtype, this.state.itemId]}
                    isOpen={this.state.isOpen}
                    onClosed={() => { this.toggleModal(false) } }
                    onApply={(Name, Type, id) => { 
                        //console.log(id)
                        if(Name == ''){
                             Alert.alert(
                                'Error',
                                'Name Cannot be Empty'
                            );
                        }else{
                            if(id == 0 ){
                                //console.log()
                                //console.log(Math.max(this.state.order.map(Number)))
                                let lastKey = !R.isEmpty(this.state.itemList) ? Math.max(...this.state.order.map(Number)) :0 
                                this.state.itemList[parseInt(lastKey)+1] = {itemId: lastKey+1, itemName: Name, type: Type}
                                this.state.order.push(''+(lastKey+1))
                                //console.log(...this.state.order.map(Number))
                                
                                this.toggleModal(false)
                            }else{
                                for([key, value] of Object.entries(this.state.itemList)){
                                   if(value.itemId == id){
                                      
                                       this.state.itemList[id] = {itemId: id, itemName: Name, type: Type}
                                       
                                       //this.toggleModal(false)
                                       this.toggleModal(false)
                                       this.forceUpdate()
                                      
                                      
                                    }
                                }
                            }  
                            //console.log(this.state.itemList)
                           
                        }
                    }} 
                />

            </View>
      
        );
    }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F6F6F6',
  },
  page: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  tabbar: {
    backgroundColor:  '#fefefe',
  },
  indicator: {
    backgroundColor: '#408fff',
  },
  label: {
    color: '#222',
    fontWeight: '400',
  },
  pickerBox:{
        height: 50,
        marginHorizontal: 10,
        borderColor: '#D6D6D6', 
        width :200,
        borderWidth: 1,
        flexDirection:'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    dropdown:{
        marginLeft: 5, 
        flex: 1, 
        width:Dimensions.get('window').width/8,
        flexDirection: 'row', 
        justifyContent: 'space-between', 
        alignItems: 'center',
    }
});