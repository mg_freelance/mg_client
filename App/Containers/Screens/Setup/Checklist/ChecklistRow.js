import React, { Component } from 'react';
import { View, Text, StyleSheet, Image } from 'react-native';
import { Avatar, Drawer, Divider, PRIMARY_COLORS, TYPO, Ripple } from 'react-native-material-design';
import Icon from 'react-native-vector-icons/Ionicons';
import { Actions as NavigationActions } from 'react-native-router-flux'

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
    },
    text: {
        marginLeft: 12,
        marginTop: 12,
        fontSize: 16,
    },
    photo: {
        height: 60,
        width: 60,
        paddingTop: 5,
        paddingLeft: 20,
        borderRadius: 60,
        backgroundColor: '#01579b',
        color: 'white',
      
    },
    rippleContainer: {
        flex: 1,
        flexDirection: 'row',
        height: 90,
        padding: 12,
    }
});

export default class ChecklistRow extends React.Component {
    constructor(props) {
        super(props)
    }
    render() {
        return (
            <View style={styles.container}>
                <Ripple text="" style={styles.rippleContainer} color='rgba(0,0,0,.5)' onPress={()=>{
                    NavigationActions.ChecklistSetup({create: false, id: this.props.data.checkListId})
                }}>
                    <Icon name="ios-checkmark" style={styles.photo} size= {50}/>
                    <Text style={styles.text}>
                        {this.props.data.checkListName}
                    </Text>
                </Ripple>
            </View>
        );
    }
}
