// @flow
import React, { Component } from 'react';
import { Colors, Metrics } from '../../../../Themes'
import {
    StyleSheet,
    ListView,
    Text,
    ScrollView,
    View,
    Dimensions,
    TextInput,
    Image,
    TouchableHighlight,
    DatePickerAndroid,
    TouchableOpacity
} from 'react-native';
import { Avatar, Checkbox, Drawer, Divider, PRIMARY_COLORS, TYPO, Ripple } from 'react-native-material-design';
import ActionButton from 'react-native-action-button';
import Icon from 'react-native-vector-icons/MaterialIcons';
import Modal from 'react-native-modalbox';
import TextField from 'react-native-md-textinput';
import Button from '../../../../Components/Button';
import ModalDropdown from 'react-native-modal-dropdown';
import dismissKeyboard from 'react-native-dismiss-keyboard';



const approveIcon = (<Icon name="done" size={30} color="#ffffff" />)

export default class ItemModel extends Component {

    constructor() {
        super();
        this.state = {
           itemType: 'checkbox',
           itemName: ''
        };
    }

    
    getModelWidth(){
        let modelWidth = Dimensions.get('window').width

        return Dimensions.get('window').width>500 ? modelWidth/3: 7* modelWidth/8
    }

    componentWillMount() {
         //console.log( this.props.itemData[1])
         this.setState({ 
               
                itemType: this.props.itemData[1],
                itemName: this.props.itemData[0]

         })
    }
    componentDidMount(){
       
    }

    componentWillReceiveProps(nextProps) {
        if(nextProps != this.props){
                 this.setState({ 
               
                itemType: nextProps.itemData[1],
                itemName: nextProps.itemData[0]

         })
        }
    }

    updateState() {
        this.setState({ 
            ...this.state,
        });
    }

     dropDown(rowData) {
        return (
            <TouchableHighlight style={{ flex: 1}} >
                <View style={{ width: 200, flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center', padding: 10 }}>
                    <Text style={{fontSize: 16 }}>{rowData}</Text>
                </View>
            </TouchableHighlight>
        );
    }

render() {
        return (
                <Modal onClosed={() => { this.props.onClosed(), this.state.textField = '' } } position="top" isOpen={this.props.isOpen} style={{ zIndex: 1000, marginTop: 80, padding: 20, width: this.getModelWidth() , height: 300, backgroundColor: '#ffffff' }}>
                    <View style= {{flexDirection: 'row', justifyContent:'center', alignItems: 'center' ,backgroundColor:'dodgerblue', height: 60, margin: -20, marginBottom: 10}}>
                        <View style = {{flex: 1,justifyContent: 'flex-start', alignItems:'flex-start', paddingLeft: 15}}>
                            <Text style = {{ fontSize: 18, color: 'white'}}>Add Checklist Item</Text>
                        </View>
                        <View style = {{flex: 1,justifyContent: 'flex-end', alignItems:'flex-end', paddingRight:15}}>
                            <Ripple color='rgba(255,255,255,.5)' onPress={()=>{this.props.onClosed()}}>
                                <Icon name="close" size={25} color="#ffffff" />
                            </Ripple>
                        </View>    
                    </View>

                    <View>
                        <View style = {{flexDirection: 'row', marginTop: 20}}>
                            <Text style={styles.text}>Title: </Text>
                            <TextInput
                                style={{marginLeft:20, width: 300, textAlign: 'left', paddingTop: 0}}
                                onChangeText={(itemName) => this.setState({itemName})}
                                value={this.state.itemName}
                                underlineColorAndroid = 'gray'
                            />
                        </View>
                        <View style={{justifyContent:'flex-start', flexDirection :'row', marginTop: 20}}>
                            <Text style={styles.text}>Type: </Text>
                             <ModalDropdown
                                style={styles.pickerBox}
                                options={['checkbox', 'photo']}
                                onSelect={(idx, value) => {
                                    this.setState({
                                        itemType: value 
                                    })
                                }}
                                renderRow ={this.dropDown.bind(this)}
                                >
                                <View style={styles.dropdown}>
                                    <Text style={{paddingLeft:20,  color: 'black', fontSize: 16 }}>
                                        {this.state.itemType}
                                    </Text>
                                    <Icon name="arrow-drop-down" size={20} color="#000000" />
                                </View>
                            </ModalDropdown>
                        </View>

                        <View style={{ marginTop: 20, justifyContent:'flex-end', flexDirection: 'row'}}>

                            <Ripple style={[styles.button, {backgroundColor: '#4fab6e',}]} color='rgba(255,255,255,.5)' 
                              onPress={()=>{
                                  this.props.onApply(this.state.itemName, this.state.itemType, this.props.itemData[2])}}>
                                <Icon name="done" size={25} color="#ffffff" />
                                <Text style={[styles.text,{color: 'white'}]}>ADD</Text>
                            </Ripple>                       
                            
                        </View>
                                          
                            
                    </View>

                    
                </Modal>
            );
    }

}

const styles = StyleSheet.create({
     photo: {
        height: 60,
        width: 60,
        borderRadius: 60,
    },
    text:{
        paddingLeft:10, 
        paddingTop: 5,  
        color: 'black', 
        fontSize: 16 
    },
    button: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems:'center',
        padding:10,
        marginHorizontal: 20,
        elevation: 2
    },
    image: {
        height: 40,
        width: 40,
        borderRadius: 60
    },
    pickerBox:{
        marginHorizontal: 20,
        borderColor: '#D6D6D6', 
        borderWidth: 1,
        flexDirection:'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        height:35
    },
    dropdown:{
        
        flex: 1, 
        width: 150,
        flexDirection: 'row', 
        justifyContent: 'space-between', 
        alignItems: 'center',
    },
})