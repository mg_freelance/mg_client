import React, { Component } from 'react';
import { View, StyleSheet, Text, ScrollView, ListView, TouchableHighlight, Dimensions} from 'react-native';
import {Ripple} from 'react-native-material-design'
import ModalDropdown from 'react-native-modal-dropdown';
import Icon from 'react-native-vector-icons/MaterialIcons';

export default class ChecklistItem extends React.Component{

    constructor(){
        super()
        this.state = {
            itemType: ''
        }
    }

    componentDidMount(){
        this.setState({
            itemType: this.props.data.type
        })
    }

    dropDown(rowData) {
        return (
            <TouchableHighlight style={{ flex: 1}} >
                <View style={{ width: 200, flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center', padding: 10 }}>
                    <Text style={{fontSize: 16 }}>{rowData}</Text>
                </View>
            </TouchableHighlight>
        );
    }

    render(){
        //console.log(this.props.data)
        return(
            <TouchableHighlight
                underlayColor={'#eee'}
                delayLongPress={500} 
                style={styles.container}
                onPress = {()=>{
                    this.props.onPress(this.props.data.itemName, this.state.itemType, this.props.data.itemId)
                }} 
                {...this.props.sortHandlers}
            >
                <View style = {{flexDirection: 'row'}}>
                    <View style={{flex:3}}>
                         <Text style = {[styles.text,{paddingLeft: 30}]}>{this.props.data.itemName}</Text>
                    </View>
                    <View style={{flex:1}}>
                         <Text style = {[styles.text,{paddingLeft: 70}]}>{this.props.data.type}</Text>
                         
                    </View>
                    
                    <View style={{width: 100, alignItems: 'flex-end', paddingBottom: -10}}>
                        <Ripple style={styles.RippleStyle} color='rgba(0 ,0 ,0 ,.5)' onPress={()=>{this.props.onIconPress(this.props.data.itemId)}}>
                            <Icon name="close" size={20} color="#000000" />
                        </Ripple>
                    </View>
                </View>
               
            </TouchableHighlight>
        )
    }
}

const styles = StyleSheet.create({
  container: {
      backgroundColor: '#fdfdfd', 
      height: 60, 
      padding: 10, 
      margin:5,
      borderWidth: 3, 
      borderTopWidth: 0, 
      borderColor:'#dddddd'
    },
    text:{
        paddingLeft:10, 
        paddingTop: 8,  
        color: 'black', 
        fontSize: 16, 
        paddingRight: 10
    },
    pickerBox:{
        flex:1,
        marginHorizontal: 10,
        borderColor: '#D6D6D6', 
        borderWidth: 1,
        flexDirection:'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        width: 170,
       height:35
    },
    dropdown:{
        
        flex: 1, 
        width: Dimensions.get('window').width/8,
        flexDirection: 'row', 
        justifyContent: 'space-between', 
        alignItems: 'center',
    },
    RippleStyle:{
        flex:1, 
        width: 50, 
        alignItems: 'center', 
        justifyContent: 'center', 
    }
  
});


/**
 * 
 * <ModalDropdown
                            style={styles.pickerBox}
                            options={['checkbox', 'upload']}
                            onSelect={(idx, value) => {
                                this.props.itemType(value)
                                this.setState({
                                    itemType: value 
                                })
                            }}
                            renderRow ={this.dropDown.bind(this)}
                            >
                            <View style={styles.dropdown}>
                                <Text style={{paddingLeft:20,  color: 'black', fontSize: 16 }}>
                                    {this.state.itemType}
                                </Text>
                                <Icon name="arrow-drop-down" size={20} color="#000000" />
                            </View>
                        </ModalDropdown>
 */