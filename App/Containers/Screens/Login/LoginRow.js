import React, { Component } from 'react';
import { View, Text, StyleSheet, Image } from 'react-native';
import { Avatar, Drawer, Divider, PRIMARY_COLORS, TYPO, Ripple } from 'react-native-material-design';
import { Actions as NavigationActions } from 'react-native-router-flux'
import {Images} from '../../../Themes'
const styles = StyleSheet.create({
    container: {
        //flex: 1,
        flexDirection: 'row',
        alignItems: 'flex-start',
        backgroundColor: 'transparent'
    },
    text: {
        //marginLeft: 6,// change 12
        //marginTop: 12,
        //fontSize: 14,// change 16
        color: 'white'
    },
    photo: {
        height: 60,
        width: 60,
        borderRadius: 60,
    },
    rippleContainer: {
        flex: 1,
        //flexDirection: 'row',
        //height: 125,// change 85
        padding: 12,
    }
});

export default class LoginRow extends React.Component {
    
    constructor(props) {
        super(props);
        this.state = {
            highlightColor: 'rgba(255,255,255,.85)',
        };
        
    }

    setHighlightColor(){
            this.setState({highlightColor: 'white'})
    }

    componentWillReceiveProps(nextProps){
        
        if (nextProps.highlighted != this.props.indx){
            if (nextProps.indx == nextProps.highlighted){
                console.log('NEW PROPS*********************')
                 this.setState({highlightColor:'rgba(250,250,250,.2)'});   
            }else{           
                this.setState({highlightColor:'rgba(255,255,255,1)'});
            }
        }else{
            this.setState({highlightColor:'rgba(250,250,250,.2)'});   
        }
    }   

    getUserImage(){
       
        if(this.props.userImg == "new"){
            return Images.DefaultProfImg
        }else if(this.props.userImg == "undefined"){
             console.log(this.props.userImg)
            return {uri: 'https://www.outsystems.com/PortalTheme/img/UserImage.png?24860'}
        }else{
            return {uri: this.props.userImg}
        }
    }

    render() {
        //this.setHighlightColor()
        if(this.props.user === null){
           return null
        }else{

            return (
                <View style={[styles.container,{ width:this.props.width}]}>
                    <View style={{position:'absolute',width:this.props.width,height:130,flex:1,opacity:0.3,backgroundColor: this.state.highlightColor}}></View>
                    <Ripple text="" style={[styles.rippleContainer, {height:this.props.height, flexDirection: this.props.direction}]} color='rgba(255,255,255,.2)' onPress={()=>{this.props.onPress(this.props.indx)}}>
                
                        <View style={{flex: 1}}>
                            <Image source={this.getUserImage()} style={[styles.photo]} />
                        </View>
                        <View style={{ flex: 1, flexDirection: 'column', justifyContent: 'center', alignItems: 'center' }}>
                            <Text style={[styles.text, {fontSize: this.props.fontsize, marginLeft: this.props.fontlftMrgn}]}>
                                {this.props.user}
                            </Text>
                            
                        </View>
                    
                
                    </Ripple>
                </View>
            );
        }
    }
}


 //{this.props.data.surName}