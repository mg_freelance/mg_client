import React, { Component } from 'react';
import { connect } from 'react-redux'
import LoginActions from '../../../Redux/LoginRedux'
import { StyleSheet, Text, View, Image } from 'react-native';
import { Images, Colors } from '../../../Themes';
import { Actions as NavigationActions, ActionConst } from 'react-native-router-flux'
import REDUX_PERSIST from '../../../Config/ReduxPersist'
import {Ripple} from  'react-native-material-design';

type pinScreenProps = {
    username: string,
    dispatch: () => any,
    fetching: boolean,
    fetched: boolean,
    attemptLogin: () => void
}

const styles = StyleSheet.create({
    row: {
        //flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center'

    },
    pad: {
        flex: 50,
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
        //margin: 30,
    },
    btn: {
        fontSize: 20,
        color: 'white',
        justifyContent: 'center',
        alignItems: 'center',
        paddingVertical: 20,
        paddingHorizontal: 50,
        textAlign: 'center',
        margin: 0,
        // borderColor: Colors.frost,
        // borderWidth: 1,
        // borderStyle: 'solid',
    },
    pin: {
        fontSize: 30,
        fontWeight: '500',
        color: 'white'
    },
    backgroundImage: {
        flex: 1,
        resizeMode: 'stretch', // or 'stretch'
    },
     rippleContainer: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        //height: 50,
        //padding: 12,
        //backgroundColor: Colors.snow
    },

});


const MAX_LENGTH = 4;




function makeDots(num) {
    let ret = '';
    while (num > 0) {
        ret += ' ○ ';
        num--;
    }
    return ret;
}

export class Pin extends Component {
    props: pinScreenProps
    isAttempting: boolean
    state = { value: '' };

    constructor() {
        super();
        this.isAttempting = false
    }

    componentWillReceiveProps(newProps) {
        this.forceUpdate()
        // Did the login attempt complete?
        this.handleRemove()
        if (newProps.fetched && newProps.error == null) {
            NavigationActions.drawer({ type: ActionConst.RESET });
        } else if (newProps.error != null) {
            //window.alert(newProps.error);
        }
    }

    handleClear() {
        const {value} = this.state;
        this.setState({ value: value.substr(0, value.length - 1) });
    }

    handlePress(num) {
        let {value} = this.state;
        value += String(num);

        

        if (value.length == MAX_LENGTH) {
            this.isAttempting = true;
            let val = value;
            console.log("Logged in User "+this.props.username); 
            this.props.attemptLogin(this.props.username, val, this.response, this.props.exsiting);
            this.handleRemove();
        }else{
            this.setState({ value });
        }
    }

    handleRemove() {
        this.setState({ value: '' });
        //NavigationActions.login()
    }

    renderButton(num) {
        return (
            <Ripple style={[styles.rippleContainer,{borderRadius:10}]} color='rgba(255,255,255,.1)'onPress={() => this.handlePress(num)}>
                <Text  style={styles.btn}>{num}</Text>
            </Ripple>
        );
    }

    render() {
        const {value} = this.state;
        const marks = value.replace(/./g, ' ● ');
        const dots = makeDots(MAX_LENGTH - value.length);


        return (
            <View style={{ flex: 1, backgroundColor: 'transparent' }}>
            
                
                <View style={{ flex: 1 }}>

                    

                    <View style={[styles.pad, { flex: 25 }]} >
                        <Text style={[styles.pin, { fontSize: 20, fontWeight: '100', paddingBottom: 0, marginTop: -40}]} >ENTER PIN</Text>
                        <Text style={styles.pin} >{marks}{dots}</Text>
                    </View>

                    <View style={[styles.pad, { paddingTop: 0 }]} >



                        <View style={styles.row} >
                           
                            {this.renderButton(1)}                    
                            {this.renderButton(2)}
                            {this.renderButton(3)}
                        </View>

                        <View style={styles.row} >
                            {this.renderButton(4)}
                            {this.renderButton(5)}
                            {this.renderButton(6)}
                        </View>

                        <View style={styles.row} >
                            {this.renderButton(7)}
                            {this.renderButton(8)}
                            {this.renderButton(9)}
                        </View>

                        <View style={styles.row} >
                             
                            <Text style={[styles.rippleContainer,{borderRadius:10}]}></Text>
                           
                            {this.renderButton(0)}
                            
                                <Ripple style={[styles.rippleContainer,{borderRadius:10,height: 70 }]} color='rgba(255,255,255,.1)'onPress={() => this.handleClear()} >
                                    
                                        <Image source={Images.clearIcon} style={{ width: 25, height: 25 }} />
                                    
                                </Ripple>
                            
                        </View>
                    </View>
                    <View style={{ flex: 10 }}></View>
                </View>
            </View>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        fetching: state.login.fetching,
        fetched: state.login.fetched,
        error: state.login.error
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        attemptLogin: (username, password, response, isExisting) => dispatch(LoginActions.loginRequest(username, password, response, isExisting))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Pin)

//<Text onPress={()=> this.handleClear()} style={styles.btn}>C</Text>
//<Text onPress={()=> this.handleClear()} style={[styles.btn, { backgroundColor: '#4CAF50'}]}>{'0'}</Text>


// background Image
/**
 * <View>
                    <Image style={[styles.backgroundImage, { position: 'absolute' }]} source={Images.Loginback} />
                </View>
 */