// @flow

import React, { Component } from 'react'
import { View, StatusBar, NetInfo, Alert } from 'react-native'
import NavigationRouter from '../Navigation/NavigationRouter'
import { connect } from 'react-redux'
import StartupActions from '../Redux/StartupRedux'
import deviceProfile from '../Redux/DeviceProfileRedux'
import ReduxPersist from '../Config/ReduxPersist'
import getScreenProfile from '../Config/DeviceProfile'


// Styles
import styles from './Styles/RootContainerStyle'

class RootContainer extends Component {

  constructor(){
    super()
    this.state = {
      isConnected: null
    }
  }

  componentDidMount () {
    // if redux persist is not active fire startup action
    if (!ReduxPersist.active) {
      this.props.startup()
    }
  
    // check for internet connectivity
    NetInfo.isConnected.addEventListener(
        'change',
        this._handleConnectivityChange
    );
    NetInfo.isConnected.fetch().done(
        (isConnected) => { this.setState({isConnected}); }
    );
  }


  _handleConnectivityChange = (isConnected) => {
    this.setState({
      isConnected,
    });
    
    !this.state.isConnected ? Alert.alert(
            'Network Error!',
            'Please check your internet connection',
            [
              {text: 'Retry', onPress: () => {
                NetInfo.isConnected.fetch().done(
                    (isConnected) => { this._handleConnectivityChange(isConnected); }
                );
              }},
            ],
            {
              cancelable: false
            }
          ):{};
  };


  componentWillUnmount() {
    NetInfo.isConnected.removeEventListener(
        'change',
        this._handleConnectivityChange
    );
  }

  render () {
    this.props.screenProfile(getScreenProfile())
    return (
      <View style={styles.applicationView}>
        <StatusBar barStyle='light-content' />
        <NavigationRouter />
      </View>
    )
  }
}

const mapStateToDispatch = (dispatch) => ({
  startup: () => dispatch(StartupActions.startup()),
  screenProfile: (screenType) => dispatch(deviceProfile.orientationProfile(screenType))
})

export default connect(null, mapStateToDispatch)(RootContainer)
