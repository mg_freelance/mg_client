// @flow
import React, { Component } from 'react'
import { PropTypes, View, Text, Image, BackAndroid, Dimensions, TouchableHighlight } from 'react-native';
import { Actions as NavigationActions } from 'react-native-router-flux'
import { Avatar, Drawer, Divider, COLOR, TYPO } from 'react-native-material-design'
import Section from '../Components/Drawer/Section';
import REDUX_PERSIST from '../Config/ReduxPersist'
import {Images} from '../Themes'
import { connect } from 'react-redux'
import LoginActions, {loggedUser} from '../Redux/LoginRedux'
import NavigationRouter from '../Navigation/NavigationRouter'


class DrawerContent extends Component {
    navigation = {
        "profile": NavigationActions.profileScreen,
        "setup": NavigationActions.tabsScreen,
        "work": NavigationActions.workScreen,
        "calendar": NavigationActions.calendarScreen,
        "history": NavigationActions.historyScreen,
        "reporting": NavigationActions.reportingScreen,
        "murphy": NavigationActions.murphyScreen,
        "messages": NavigationActions.messagesScreen,
        "loginPortrait":NavigationActions.Portrait,
        "loginLandscape" : NavigationActions.LandScape,
        "login": NavigationActions.welcome
    }

    componentDidMount() {
        BackAndroid.addEventListener('hardwareBackPress', () => {
            if (this.context.drawer.props.open) {
                this.toggleDrawer()
                return true
            }
            return false
        })
    }

    componentWillReceiveProps (newProps) {
        
    }

    triggerNavigationAction(navigationLevel) {
        this.toggleDrawer();
        this.setState({
            route: navigationLevel
        });
        this.navigation[navigationLevel]();
    }

    toggleDrawer() {
        this.context.drawer.toggle()
    }

    logout(){
        this.clearAndLogout();

    }

    clearAndLogout = (state)=>{
        //NavigationActions.pop();
        Dimensions.get('window').width<500 ?  this.navigation['loginPortrait'](): this.navigation['loginLandscape']()
        this.props.attemptLogout();
    //  REDUX_PERSIST.storeConfig.storage.removeItem("currentUser",()=>{
    //         this.navigation['login']({type:'reset'});
    //         this.props.attemptLogout();
    //  });

    }

    constructor(props) {
        super(props);
        let User = props.username
        ///console.log('LOGGGGGGGGGGEEED')
        //console.log(props.profURL)

        this.state = {
            route: null
        }
    }


    render() {
        const { route } = this.state;
        // http://demo.geekslabs.com/materialize/v2.2/layout03/images/user-profile-bg.jpg require('./../Images/nav.jpg')
        return (
            <Drawer theme='light' style={{ flex: 1 }} >

                <Drawer.Header style={styles.headerContainer} image={<Image source={Images.Drawerback} />}>
                    <View style={styles.header} >
                        <TouchableHighlight onPress={()=>{this.triggerNavigationAction('profile')}}>    
                            <Image source={{ uri: this.props.profURL }} style={styles.photo} />
                        </TouchableHighlight>
                        <Text style={[styles.text, COLOR.paperGrey50, TYPO.paperFontSubhead]}>{this.props.username}</Text>                        
                    </View>
                </Drawer.Header>
                <View style={{ backgroundColor: 'rgba(185,185,185,0.4)', marginTop: -10, paddingTop: -10}} >
                    <Section theme='light'

                        style={styles.topSection}
                        items={[{
                            icon: 'event',
                            value: 'Calendar',
                            active: route === 'calendar',
                            onPress: () => this.triggerNavigationAction('calendar'),
                            onLongPress: () => this.triggerNavigationAction('calendar')
                        }, {
                            icon: 'history',
                            value: 'History',
                            active: route === 'history',
                            onPress: () => this.triggerNavigationAction('history'),
                            onLongPress: () => this.triggerNavigationAction('history')
                        }, {
                            icon: 'mail',
                            value: 'Messages',
                            active: route === 'messages',
                            label: true ? <Text style={styles.notification}>5</Text> : null,
                            onPress: () => this.triggerNavigationAction('messages'),
                            onLongPress: () => this.triggerNavigationAction('messages')
                        }]}
                        />
                </View>
                <View style={{paddingTop: -10}} >
                    <Section
                    theme = 'light'
                    items={[{

                        value: 'Work',
                        active: route === 'work',
                        onPress: () => this.triggerNavigationAction('work'),
                        onLongPress: () => this.triggerNavigationAction('work')
                    }, {

                        value: 'Murphy',
                        active: route === 'murphy',
                        onPress: () => this.triggerNavigationAction('murphy'),
                        onLongPress: () => this.triggerNavigationAction('murphy')
                    }, {
                        value: 'Reporting',
                        active: route === 'reporting',
                        onPress: () => this.triggerNavigationAction('reporting'),
                        onLongPress: () => this.triggerNavigationAction('reporting')
                    }, {
                        value: 'Setup',
                        active: route === 'setup',
                        onPress: () => this.triggerNavigationAction('setup'),
                        onLongPress: () => this.triggerNavigationAction('setup')
                    }]}
                    />
                </View>

                <Divider  />
                
            <Section
                    theme="light"
                    items={[{
                        value: 'Logout',
                        active: route === 'logout',
                        onPress: () => this.logout(),
                        onLongPress: () => this.logout()
                    }]}
                    />
            </Drawer>
        );
    }
}

const styles = {
    header: {
        paddingTop: 16,
        flexDirection: 'row',
        margin: 0
    },
    photo: {
        marginTop: 50,
        height: 60,
        width: 60,
        borderRadius: 70,
    },
    text: {
        marginLeft: 20,
        marginTop: 65
    },
    headerContainer: {
        height: 50
    },
    topSection: {
        backgroundColor: 'rgba(120,120,120,1)'
    },
    notification:{
        color: 'white',
    }
};

DrawerContent.contextTypes = {
    drawer: React.PropTypes.object
}

const mapStateToProps = (state) => {
   return {
       username: state.login.username,
       profURL : state.login.ProfURL
   }
}

const mapDispatchToProps = (dispatch) => {
    return {
        attemptLogout: (username, password, response) => dispatch(LoginActions.logout())
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(DrawerContent)


/**
 * 
 * {
                            icon: 'person',
                            value: 'My Profile',
                            active: route === 'profile',
                            onPress: () => this.triggerNavigationAction('profile'),
                            onLongPress: () => this.triggerNavigationAction('profile')
                        }, 
 */