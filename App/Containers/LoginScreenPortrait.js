// @flow

import React from 'react'
import {
  View,
  ScrollView,
  Text,
  TextInput,
  TouchableOpacity,
  Image,
  Keyboard,
  LayoutAnimation,
  BackAndroid,
  Dimensions,
  StatusBar
} from 'react-native'
import { connect } from 'react-redux'
import Styles from './Styles/LoginScreenStyle'
import {Images, Metrics} from '../Themes'
import LoginActions from '../Redux/LoginRedux'
import { Actions as NavigationActions } from 'react-native-router-flux'
import I18n from 'react-native-i18n'
import dismissKeyboard from 'react-native-dismiss-keyboard'
import TextField from 'react-native-md-textinput'
import Orientation from 'react-native-orientation';

import Pin from './Screens/Login/pinScreenPortrait'
import LoginRow from './Screens/Login/LoginRow'
import {Ripple} from  'react-native-material-design';
import ActionButton from 'react-native-action-button';
import SelectionModal from './Screens/Setup/User/Modals/SelectionModal';
import REDUX_PERSIST from '../Config/ReduxPersist';
import Indicator from '../Components/Indicator';

import FJSON from 'format-json'


//******************* TEMP **********************************

/*import apisauce from 'apisauce';
baseURL = 'http://ec2-54-213-62-94.us-west-2.compute.amazonaws.com';

const api = apisauce.create({
  "baseURL": baseURL,
  "headers": {
    'Cache-Control': 'no-cache'
  },
  "timeout": 10000
});
*/
//***********************************************************

type LoginScreenProps = {
  dispatch: () => any,
  fetching: boolean,
  fetched: boolean,
  attemptLogin: () => void
}

const imageDimensions = {
  height: window.height,
  width: window.width
}

class LoginScreen extends React.Component {

 

  props: LoginScreenProps
  //api: Object

  state: {
    username: string,
    password: string,
    visibleHeight: number,
    visibleWidth: number,
    topLogo: {
      width: number
    },
    show: boolean,
    isQModelOpen: boolean,
    loggedList: Object
  }

  isAttempting: boolean
  keyboardDidShowListener: Object
  keyboardDidHideListener: Object
  response: {
    
  }

  constructor (props: LoginScreenProps) {
    super(props)
 

    this.state = {   
      username: '',
      password: '',
      sitemail: 'lasitha.petthawadu@bback.com',
      visibleHeight: Metrics.screenHeight,
      visibleWidth: Metrics.screenWidth,
      topLogo: { width: Metrics.screenWidth },
      show: false,
      isQModelOpen: false,
      currentHighlightedIndx:1,
      users:[],
      isExisting: false,
      animating: true       
            
    }      
       this.isAttempting = false

  }

  

  componentWillReceiveProps (newProps) {
    this.setState({animating: true})
    this.forceUpdate()
    // Did the login attempt complete?
    if (newProps.fetched && newProps.error==null) {
          NavigationActions.drawer({type:'reset'});
    }else if (newProps.error != null){
          window.alert(newProps.error);
          this.setState({animating: false})
    }
  }


  toggleModal(modalName, openStatus) {
        //this.state[modalName] = openStatus;
        this.setState({
          isQModelOpen: openStatus
        });
    }

  componentWillMount () {

    // Orientation Lock
    Orientation.lockToPortrait()

    // Using keyboardWillShow/Hide looks 1,000 times better, but doesn't work on Android
    // TODO: Revisit this if Android begins to support - https://github.com/facebook/react-native/issues/3468
    this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this.keyboardDidShow)
    this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this.keyboardDidHide)
    
    
  }

  componentWillUnmount () {
    this.keyboardDidShowListener.remove()
    this.keyboardDidHideListener.remove()
  }

  keyboardDidShow = (e) => {
    // Animation types easeInEaseOut/linear/spring
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut)
    
    let newSize = Metrics.screenHeight - e.endCoordinates.height
    this.setState({
      //visibleHeight: newSize,
      //topLogo: {width: 100, height: 70},
      show: true
      
    })
  }

  keyboardDidHide = (e) => {
    // Animation types easeInEaseOut/linear/spring
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut)
    this.setState({
      visibleHeight: Metrics.screenHeight,
      topLogo: {width: Metrics.screenWidth},
      show: false
    })
  }

  handlePressLogin = () => {
    dismissKeyboard() 
    const { username, password, isExisting } = this.state
    this.isAttempting = true
   
    // attempt a login - a saga is listening to pick it up from here.
    this.props.attemptLogin(username, password, this.response, isExisting)
    
  }

  

componentDidMount(){
      let logged = '';
      BackAndroid.addEventListener('hardwareBackPress', ()=>{
        return BackAndroid.exitApp(0);
      });

     REDUX_PERSIST.storeConfig.storage.getItem('loggedUser', (error, result)=>{
          if(result!= null){
            logged = result
          }
      });

      REDUX_PERSIST.storeConfig.storage.multiGet(['User1', 'User2','User3'],(err, value)=>{
             console.log('******************************************************************')
             let userdata = {}
            
             if(err || value[0][1]==null ){
                this.setState({
                  animating: false
                })
                
             }else{
               
                userdata = value.map((usr)=>{
                  return usr[1]
                })
                .reduce((details,usr)=>{
                  
                  if (usr != null) {
                    details.push(JSON.parse(usr))
                    
                  }else{
                    details.push(null)
                  };

                  return details
                }, [])
                .sort((l1, l2)=>{
                  if(l1 == null){
                    return 1
                  }else if(l2 == null){
                    return -1
                  }else{
                    return (new Date(l2.TimeStamp) - new Date(l1.TimeStamp)) < 0 ? -1 : 1 
                  }
                })

               
                this.setState({
                    users: userdata ,
                    password: '',
                    username: userdata[0].username,
                    currentHighlightedIndx: 1, 
                    newUserPresent: false,
                    isExisting:true,
                    animating: false    
                });
            }
      });


  }


addUser(text){

  this.setState({
    isQModelOpen: false,
    newUserPresent: true,
    username : text,
    isExisting: false,
    currentHighlightedIndx: 1,
    tempUsr: text,   

  })
}

userPresent(text){
 
  if(this.state.users == null){
    return false
  }else{
    let present = this.state.users.find((user)=>{
        
      if(user != null ){
        //console.log(user.username.toUpperCase())
        return user.username.toUpperCase() == text.toUpperCase()
      }else{
        return false
      }
    })
    return present? true: false
  }
}

getImage(id){
  if(this.state.users==null){
    return null
  }else{
    if(this.state.users[id]!=null){

      return this.state.users[id].profileImage

    }else{
      return null
    }
  }
}

getuser(id){
  if(this.state.users==null){

    return null
  }else{
    
    if(this.state.users[id]!=null){
      return this.state.users[id].username
    }else{
      return null
    }
  }
}




render () {
    const { username, password } = this.state
    const { fetching } = this.props
    const {fetched} = this.props
    const editable = true//!fetching
    
    const textInputStyle = editable ? Styles.textInput : Styles.textInputReadonly
    const onKeyboardshow = this.state.show ? Styles.onKeyboardShow : Styles.contentContainer

    if(this.state.newUserPresent){

        return(
          <View style={{ flex:1, backgroundColor: 'transparent' }}>
            <StatusBar hidden= {true}/> 
            <View>
              <Image style={[Styles.backgroundImage, {position: 'absolute'}]} source={Images.Loginback} />
            </View>

            <View contentContainerStyle={onKeyboardshow} style={[Styles.container, {flex: 1, flexDirection:'column', alignItems: 'center',height: this.state.visibleHeight}]} keyboardShouldPersistTaps>
                <View style={{flex:2,marginBottom:30}}>
                  <Image source={Images.logo} style={[Styles.topLogo,{ height: 170, width:170}]} />
                </View>  
                
                <View style={[Styles.form,{flex: 1, flexDirection: 'row', justifyContent: 'flex-start',  marginLeft: -40, marginRight: -40, width: 335, marginBottom: 0, height:36}]}>
                       <LoginRow direction={'column'} height={125} width={83} fontsize={13} fontlftMrgn={0} highlighted ={this.state.currentHighlightedIndx} user = {this.state.tempUsr} indx = {1} userImg = {"new"} onPress={
                          (id)=> {this.setState(
                            {username: this.state.tempUsr, currentHighlightedIndx: id}
                          )}
                        }/>

                         <LoginRow direction={'column'} height={125} width={83} fontsize={13} fontlftMrgn={0} highlighted ={this.state.currentHighlightedIndx} user = {this.getuser(0)} indx = {2} userImg = {this.getImage(0)} onPress={
                          (id)=> {this.setState(
                            {username: this.getuser(0), currentHighlightedIndx: id}
                          )}
                        }/>

                        <LoginRow direction={'column'} height={125} width={83} fontsize={13} fontlftMrgn={0} highlighted ={this.state.currentHighlightedIndx} user = {this.getuser(1)} indx = {3} userImg = {this.getImage(1)} onPress={
                          (id)=> {this.setState(
                            {username: this.getuser(1), currentHighlightedIndx: id}
                          )}
                        }/>
                    
                        <View style={{ flexDirection: 'row',margin:2,marginTop:0, marginLeft:0, paddingTop: 5, width: 83 , height: 130, backgroundColor:'rgba(255,255,255,.3)', justifyContent:'flex-start'}}>
                            <Ripple style={[Styles.rippleContainer,{borderRadius:10, height: 100}]} color='rgba(255,255,255,.1)' onPress={()=>{this.toggleModal('isQModelOpen', true)}}>
                                <Image source={Images.FABIcon} style={[Styles.FAB,{marginTop:0}]} />
                            </Ripple>
                        </View>

                </View>
                
                <View style= {[Styles.form, {flex: 4,flexDirection:'column',width: 335,  backgroundColor: 'rgba(255,255,255,.08)',marginLeft: -40, marginRight: -40, marginBottom: 35, paddingTop: 10}]}>
                      <Pin username={this.state.username} exsiting = {this.state.isExisting} index= {this.state.currentHighlightedIndx}/>            
                </View>
            </View>
          
            <Indicator animating={this.state.animating}/> 
             
        
            <SelectionModal
                modalType="loginModel"
                width = {3*Dimensions.get('window').width/4}
                title="NEW USER LOGIN"
                isOpen={this.state.isQModelOpen}
                label="Username"
                onClosed={() => { this.toggleModal('isQModelOpen', false) } }
                onApply={(text) => { 
                  if(text ==''){
                    alert('Username Empty !')
                  }else{
                    if(this.userPresent(text)){
                        alert('User Already Existing !')
                    }else{
                        this.addUser(text)
                    }
                  }
                }} />
        
         
          </View>

        
        )

    }else{
       return(
          
          <View style={{ flex:1, backgroundColor: 'transparent' }}>
            <StatusBar hidden= {true}/>            
            <View>
              <Image style={[Styles.backgroundImage, {position: 'absolute'}]} source={Images.Loginback} />            
            </View>                     
            
            <View contentContainerStyle={onKeyboardshow} style={[Styles.container, {flex: 1, flexDirection:'column', alignItems: 'center',height: this.state.visibleHeight}]} keyboardShouldPersistTaps>
                <View style={{flex:2,marginBottom:30}}>
                  <Image source={Images.logo} style={[Styles.topLogo,{ height: 170, width:170}]} />
                </View>  
                
                <View style={[Styles.form,{flex: 1, flexDirection: 'row', justifyContent: 'flex-start', marginLeft: -40, marginRight: -40, width: 335, marginBottom: 0, height:36}]}>
                       <LoginRow direction={'column'} height={125} width={83} fontsize={13} fontlftMrgn={0} highlighted ={this.state.currentHighlightedIndx} user = {this.getuser(0)} indx = {1} userImg = {this.getImage(0)} onPress={
                          (id)=> {this.setState(
                            {username: this.getuser(0), currentHighlightedIndx: id, isExisting: true}
                          )}
                        }/>

                        <LoginRow direction={'column'} height={125} width={83} fontsize={13} fontlftMrgn={0} highlighted ={this.state.currentHighlightedIndx} user = {this.getuser(1)} indx = {2} userImg = {this.getImage(1)} onPress={
                          (id)=> {this.setState(
                            {username: this.getuser(1), currentHighlightedIndx: id, isExisting: true}
                          )}
                        }/>

                        <LoginRow direction={'column'} height={125} width={83} fontsize={13} fontlftMrgn={0} highlighted ={this.state.currentHighlightedIndx} user = {this.getuser(2)} indx = {3} userImg = {this.getImage(2)} onPress={
                          (id)=> {this.setState(
                            {username: this.getuser(2), currentHighlightedIndx: id, isExisting:true }
                          )}
                        }/>
                    
                        <View style={{ flexDirection: 'row',margin:2,marginTop:0, marginLeft:0, paddingTop: 5, width: 83 , height: 130, backgroundColor:'rgba(255,255,255,.3)', justifyContent:'flex-start'}}>
                            <Ripple style={[Styles.rippleContainer,{borderRadius:10, height: 100}]} color='rgba(255,255,255,.1)' onPress={()=>{this.toggleModal('isQModelOpen', true)}}>
                                <Image source={Images.FABIcon} style={[Styles.FAB,{marginTop:0}]} />
                            </Ripple>
                        </View>

                </View>
                
                <View style= {[Styles.form, {flex: 4,flexDirection:'column',width: 335,  backgroundColor: 'rgba(255,255,255,.08)',marginLeft: -40, marginRight: -40, marginBottom: 35, paddingTop: 10}]}>
                      <Pin username={this.state.username} exsiting = {this.state.isExisting}/>            
                </View>

                 
            </View>

            
            <Indicator animating={this.state.animating}/> 
  
        
            <SelectionModal
                modalType="loginModel"
                width = {3*Dimensions.get('window').width/4}
                title="NEW USER LOGIN"
                isOpen={this.state.isQModelOpen}
                label="Username"
                onClosed={() => { this.toggleModal('isQModelOpen', false) } }
                onApply={(text) => { 
                  if(text ==''){
                    alert('Username Empty !')
                  }else{
                     if(this.userPresent(text)){
                        alert('User Already Existing !')
                    }else{
                        this.addUser(text)
                    }
                  }
                }} />
                
                
              

        
         
          </View>

        
        )
    }

   
  }

}

const mapStateToProps = (state) => {
  return {
    fetching: state.login.fetching,
    fetched : state.login.fetched,
    error: state.login.error 
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    attemptLogin: (username, password, response, isExisting) => dispatch(LoginActions.loginRequest(username, password, response, isExisting))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(LoginScreen)

// <Indicator animating={this.state.animating}/>