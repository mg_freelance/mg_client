// @flow

import { StyleSheet } from 'react-native'
import { Colors, Metrics } from '../../Themes'

export default StyleSheet.create({
  backgroundImage: {
    flex: 1,
    resizeMode: 'stretch', // or 'stretch'
  },
  topbottom: {
    flex: 1,
  

  },
  container: {
    
    
    //backgroundColor: Colors.windowTint
  },
  panel:{
    flex: 3,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    //paddingHorizontal: Metrics.screenHeight/16,
  },
  form: {
    
    flex: 1,
    backgroundColor: Colors.transparent,
    //flexDirection: 'column'
    justifyContent:'center',
    //marginBottom:4* Metrics.doubleBaseMargin,
    //borderRadius: 0
  },

  FAB: {
        paddingTop: -50,
        height: 70,
        width: 70,
        borderRadius: 90,
       // backgroundColor: 'white'
    },

  row: {
    flex: 1,
   // paddingVertical: Metrics.doubleBaseMargin,
    paddingHorizontal: Metrics.doubleBaseMargin,
    height : 60,
    fontSize:20,
    color: 'white',
    //marginBottom: -15
   
    
  },
  // rowLabel: {
  //   flex: 1,
  //   color: Colors.ricePaper,
  //   fontSize: 20,    
  // },
  text:{
    marginLeft:0,
    color: 'white',
    fontSize: 16

  },
  
  textInput: {
    flex:1,
    //color: Colors.snow,
    fontSize: 20,
    // borderColor: Colors.frost,
    // borderWidth: 1,
    // borderStyle: 'solid',
    //height: 60,
    paddingLeft:20,
    //paddingVertical:30
  },
  textInputReadonly: {
    //color: Colors.frost,
    // borderColor: Colors.frost,
    // borderWidth: 1,
    // borderStyle: 'solid',
    height: window.height
    
  },
  loginRow: {
    flex: 1,
    paddingTop: 3*Metrics.doubleBaseMargin,
    //paddingBottom: Metrics.doubleBaseMargin,
    //paddingHorizontal: Metrics.doubleBaseMargin,
    flexDirection: 'row',
      
  },
  loginButtonWrapper: {
    flex: 1,
    height: 60     
  },
  loginButton: {
    flex: 1,
    borderWidth: 1,
    borderColor: '#4CAF50',
    backgroundColor: '#4CAF50',
    padding: 6,
    justifyContent: 'center',
    
  },
  loginText: {
    textAlign: 'center',
    color: Colors.silver,
    fontSize: 20
  },
  topLogo: {
    alignSelf: 'center',
    resizeMode: 'contain',
    width: 260,
    height : 260,
    
    
  },
  contentContainer: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
   
    padding: Metrics.screenHeight/32,
    //paddingBottom: Metrics.screenHeight/32
  },
  rippleContainer: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        height: 90,
        padding: 12,
        //backgroundColor: Colors.snow
  },
  onKeyboardShow: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center',
    paddingTop:-Metrics.screenHeight/10
  }
})
