import React, { Component } from 'react';
import {
   ActivityIndicator,
   View,
   StyleSheet,
   Dimensions,
   Text
} from 'react-native';

export default Indicator = (props) => {
  
   return (
      <View style = {[styles.container,{marginTop: props.animating ?-Dimensions.get('window').height: Dimensions.get('window').height,}]}>
         <ActivityIndicator animating = {props.animating}
           style = {[styles.activityIndicator]} size = "large"
         />
         <Text style={{flex: 1, color:'white', fontSize: 16}}>Please wait..</Text>
      </View>
   );
}

const styles = StyleSheet.create ({
   container: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      width: Dimensions.get('window').width,
      height:Dimensions.get('window').height,
      position: 'absolute',
      backgroundColor: 'rgba(0,0,0,.6)',
      elevation: 5
   },
   activityIndicator: {
      flex: 1,
      justifyContent: 'flex-end',
      alignItems: 'center',
      width: Dimensions.get('window').width
      //height: 80,
   }
});