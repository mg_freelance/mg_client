// @flow

import { Metrics, Colors, Fonts } from '../../Themes'

export default {
  text: {
    ...Fonts.style.small,
    color: Colors.black,
    marginVertical: Metrics.baseMargin
  }
}
