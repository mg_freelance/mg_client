import React, { Component } from 'react';
import { PropTypes, Text, View, StyleSheet, Image, Dimensions } from 'react-native';
import { Toolbar as MaterialToolbar, Ripple } from 'react-native-material-design';
import { Actions as NavigationActions } from 'react-native-router-flux';
import Icon from 'react-native-vector-icons/MaterialIcons';
import {TYPO} from '../Config/config';
import { Images } from '../Themes'


export default class CustomNavBar extends Component {
/*
    static contextTypes = {
        navigator: PropTypes.object
    };
    */
/*
    static propTypes = {
        onIconPress: PropTypes.func.isRequired
    };
*/
    constructor(props) {
        super(props);
        this.state = {
            title: 'Test',
            theme: 'googleBlue',
            counter: 0
        };
    }
    componentDidMount(){
        
    }
    
    getTopBar(){
        //console.log('****************************Nav Bar start')
        //console.log(this.props.navigationState.children[this.props.navigationState.children.length -1].name)
         if(this.props.navigationState.children[this.props.navigationState.children.length -1].name == 'HolidayScreen'){
             return (<View style = {styles.container}>
                    <Ripple style={styles.ripple} onPress={()=>{NavigationActions.calendarScreen()}}>
                        <Image style ={{width: 24, height: 24}} source={Images.levelUp} />
                    </Ripple>
                    <Text style={styles.text}>Manage Leaves</Text>
                    <View style={styles.label}>
                        <Text style={[TYPO.paperFontBody2, { color: 'white'}]}>5</Text>
                    </View>
                </View>)
        }else if(this.props.navigationState.children[this.props.navigationState.children.length -1].name == 'CellExpandView'){
            //console.log(this.props.date)
            return (<View style = {[styles.container, {justifyContent: 'space-between', width: Dimensions.get('window').width - 75}]}>
                        <Text style={styles.text}>Nov 13, 2017</Text>                        
                       
                    </View>)
        }else{
            return (<View style = {styles.container}>
                    <Text style={styles.text}>Messages</Text>
                    <View style={styles.label}>
                        <Text style={[TYPO.paperFontBody2, { color: 'white'}]}>5</Text>
                    </View>
                </View>)
        } 
    }
    render() {
        const { navigator } = this.context;
        const { theme, counter } = this.state;
       // const { onIconPress } = this.props;
       var actions=[];
     
       if (this.props.onDelete){
         
           actions.push({
               icon:'delete',
               onPress: ()=>{this.props.triggerActionHandler('delete')}
           });
       }

       if (this.props.onConfirm){
           actions.push({
               icon:'done',
               onPress: ()=>{this.props.triggerActionHandler('confirm')}
           });
       }
      
        return (
            <MaterialToolbar
                title={this.props.title}
                primary={theme}
                icon={(this.props.leftButton=='hamburger')?'menu':'keyboard-arrow-left'}
                onIconPress={() =>{ if(this.props.leftButton=='hamburger'){
                    NavigationActions.refresh({
                    key: 'drawer',
                    open: true
                    })}else{
                        NavigationActions.pop();
                    }}}
                actions={actions}
                rightIconStyle={{
                    margin: 10
                }}
            >
                {this.getTopBar()}         
            </MaterialToolbar>
        );
    }
}

const styles = StyleSheet.create({
    container:{
        flexDirection: 'row',
        alignItems: 'flex-start',
        justifyContent:'center'
    },
    text: {
        color: 'white',
        fontSize: 20,
        fontWeight: '500'
    },
    icon:{
        //marginRight: 20
    },
    ripple:{
        marginRight: 20,
    },
    label: {
        borderRadius: 100,
        backgroundColor: '#EC5D5D',
        paddingHorizontal: 10,
        marginLeft: 20,
        paddingBottom: 4,
        paddingTop: -1
        //top:-10
    }
})

/*

<Icon 
                            name = 'reply'
                            size = {26}
                            color = 'white'
                            style = {styles.icon}
                        
                        />

import React from 'react'
import { View, Image, Animated, TouchableOpacity } from 'react-native'
import { Images, Colors } from '../Themes'
import Styles from './Styles/CustomNavBarStyle'
import Icon from 'react-native-vector-icons/Ionicons'
import { Actions as NavigationActions } from 'react-native-router-flux'

export default class CustomNavBar extends React.Component {
  render () {
    return (
      <Animated.View style={Styles.container}>
        <TouchableOpacity style={Styles.leftButton} onPress={NavigationActions.pop}>
          <Icon name='ios-arrow-back' size={34} color={Colors.snow} />
        </TouchableOpacity>
        <Image style={Styles.logo} source={Images.clearLogo} />
        <View style={Styles.rightButton} />
      </Animated.View>
    )
  }
}
*/
