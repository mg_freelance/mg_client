import { put, call } from 'redux-saga/effects'
import LoginActions from '../Redux/LoginRedux'
import REDUX_PERSIST from '../Config/ReduxPersist'
import loginApi from '../Services/LoginApi'
// attempts to login
export function* login({ username, password, response, isExisting }) {
  var apiHandler = loginApi.create();
    console.log("*************** LOGIN ATTEMPTING ******************* ");
  if (password === '' || username === '') {
    // dispatch failure
      console.log("*************** LOGIN FAILED ******************* ");
    yield put(LoginActions.loginFailure('Username or PIN cannot be blank'))
  } else {
    var response = yield call(apiHandler.getToken, username, password, 'lasitha.petthawadu@bback.com');

    if (response.ok) {
        console.log("*************** LOGIN SUCCESS ******************* ");
        // console.log(isExisting)

        REDUX_PERSIST.storeConfig.storage.setItem('loggedUser', username,()=>{} );
        if(!isExisting){

          let json = {
              "username": ""+username,
              "profileImage": ""+ response.data.profileImg, //(response.data.profileImg=== undefined ? "new": response.data.profileImg)
              "AuthToken": ""+ response.data.authToken,
              "TimeStamp": new Date()
          }

          let stringJSON = JSON.stringify(json)
           console.log('*******************************************')
           //console.log(stringJSON)

          REDUX_PERSIST.storeConfig.storage.getItem('userCount', (error, result)=>{
            // console.log('*******************************************')
            // console.log(error, result)
            
            if(result == null){
              REDUX_PERSIST.storeConfig.storage.setItem('userCount', '1',()=>{} );
              REDUX_PERSIST.storeConfig.storage.setItem('User1', stringJSON,()=>{} );
            }else{
              let resultInt = parseInt(result);
                    //REDUX_PERSIST.storeConfig.storage.setItem('userCount','0',()=>{});
              //Limit stored users to 3
              if(resultInt<3){
                  let resultString = (resultInt+1).toString()              
                  
                  //Add the next user
                  REDUX_PERSIST.storeConfig.storage.setItem('User'+ resultString, stringJSON, ()=>{
                        REDUX_PERSIST.storeConfig.storage.setItem('userCount', resultString,()=>{})                   
                  } );

                }else{
                      //Replace oldest user
                       REDUX_PERSIST.storeConfig.storage.multiGet(['User1', 'User2','User3'],(err, value)=>{
                          let loggedKey = value.sort((l1,l2)=>{                           
                            return (new Date(JSON.parse(l2[1]).TimeStamp) - new Date(JSON.parse(l1[1]).TimeStamp)) < 0 ? -1 : 1 
                          })

                         REDUX_PERSIST.storeConfig.storage.setItem(loggedKey[2][0] ,stringJSON, ()=>{} );
                       })
                    }                 
            }
          });

        }else{
          
          REDUX_PERSIST.storeConfig.storage.multiGet(['User1', 'User2','User3'],(err, value)=>{
            let loggedKey = value.findIndex((usr)=>{
              if(usr[1] != null){
                return JSON.parse(usr[1]).username == username ? usr[0]: false
                //console.log(JSON.parse(usr[1]).username)
              }
            }) + 1

            newJson = {
              "TimeStamp": new Date()
            };

            REDUX_PERSIST.storeConfig.storage.mergeItem('User'+ loggedKey, JSON.stringify(newJson),()=>{})
            //console.log(loggedKey)
          })
        }
        
        yield put(LoginActions.loginSuccess(username, response.data.authToken, response.data.profileImg))
      }else{
        console.log("*************** LOGIN FAILED ******************* ");
        //console.log(response);
         yield put(LoginActions.loginFailure(response.data.message));
      }
    }
  }
