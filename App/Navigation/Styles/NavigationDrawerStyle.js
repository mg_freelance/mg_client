// @flow

import {Colors} from '../../Themes/'

export default {
  drawer: {
    backgroundColor: Colors.frost,
    shadowColor: '#ffffff', shadowOpacity: 1.0, shadowRadius: 10
  },
  main: {
    backgroundColor: '#ffffff', opacity: 1
  }
}
