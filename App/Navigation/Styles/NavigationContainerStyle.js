// @flow

import {Colors} from '../../Themes/'

export default {
  container: {
    flex: 1
  },
  navBar: {
    backgroundColor: '#4285F4',
    left:0
  },
  title: {
    color: 'white',
    alignSelf:'flex-start',
    marginLeft:20,
    fontWeight:'400'
   },
  leftButton: {
    marginLeft:10,
    tintColor: 'white'
  },
  rightButton: {
    color: 'white'
  }
}
