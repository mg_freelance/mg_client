// @flow

import React, { Component } from 'react'
import {Dimensions, View, Text} from 'react-native'
import { Scene, Router, Actions as NavigationActions } from 'react-native-router-flux'
import Styles from './Styles/NavigationContainerStyle'
import NavigationDrawer from './NavigationDrawer'
import NavItems from './NavItems'
import CustomNavBar from '../Components/CustomNavBar'

// screens identified by the router

import LoginScreenPortrait from '../Containers/LoginScreenPortrait'
import LoginScreenLandScape from '../Containers/LoginScreenLandScape'
import SetupScreen from '../Containers/Screens/Setup/SetupScreen'
import UserView from '../Containers/Screens/Setup/User/UserView'
import SkillView from '../Containers/Screens/Setup/Skill/SkillView'
import RoleView from '../Containers/Screens/Setup/Role/RoleView'
import DepartmentSetup from '../Containers/Screens/Setup/Department/DepartmentSetup'
import WorkflowSetup from '../Containers/Screens/Setup/Workflow/WorkflowSetup'

import ChecklistSetup from '../Containers/Screens/Setup/Checklist/ChecklistSetup'
//import LoginPin from '../Containers/Screens/Login/pinScreen'
import PatternView from '../Containers/Screens/Calendar/Components/Pattern/PatternView'
import CellExpandView from '../Containers/Screens/Calendar/Components/Cells/CellExpandView'
import CalendarScreen from '../Containers/Screens/Calendar/CalendarScreen'
import HolidayScreen from '../Containers/Screens/Calendar/Holidays/HolidayScreen'
import HistoryScreen from '../Containers/Screens/History/HistoryScreen'
import MessagesScreen from '../Containers/Screens/Messages/MessagesScreen'
import MurphyScreen from '../Containers/Screens/Murphy/MurphyScreen'
import ReportingScreen from '../Containers/Screens/Reporting/ReportingScreen'
import WorkScreen from '../Containers/Screens/Work/WorkScreen'
import ProfileScreen from '../Containers/Screens/Profile/ProfileScreen'
import WelcomeScreen from '../Containers/WelcomeScreen'
import REDUX_PERSIST from '../Config/ReduxPersist'
/* **************************
* Documentation: https://github.com/aksonov/react-native-router-flux
***************************/
var actionHandlers =[];



class NavigationRouter extends Component {

  constructor(props){
    super(props);
    this.state={isPortrait:false,username:null};
    this.getPersistenceProps(this.state).done();

  }
  
  getPersistenceProps= async (state)=>{
      var currentUser = await REDUX_PERSIST.storeConfig.storage.getItem("currentUser");
      let modelWidth = Dimensions.get('window').width
      // @lasitha - PinScreen is merged to LoginScreen
      //@anuradha - Implementing 2 login screen views for tab and mobile
     
      if (modelWidth < 500){
        this.setState({isPortrait:true,username:currentUser});
      }else{
        this.setState({isPortrait:false,username:currentUser});
      }
      this.fastLogin();
     
  }

  fastLogin(){
    console.log("FAST LOGIN");
    if(this.state.isPortrait){
     //NavigationActions.pinScreen({type:'reset',username:this.state.username});
     NavigationActions.Portrait();
    }else{
       NavigationActions.LandScape();
    }
  }
  
  
  render () {
    return (
      <Router>
       <Scene initial key='welcome' component={WelcomeScreen} title='Welcome' navBar={CustomNavBar} hideNavBar />
       <Scene key='LandScape' component={LoginScreenLandScape} title='Login' navBar={CustomNavBar} hideNavBar />
       <Scene  key='Portrait' component={LoginScreenPortrait} title='Login' navBar={CustomNavBar} hideNavBar />
        <Scene key='drawer' component={NavigationDrawer} open={false}>
          <Scene key='drawerChildrenWrapper' type="reset" navigationBarStyle={Styles.navBar}  hideNavBar = {false} titleStyle={Styles.title} leftButtonIconStyle={Styles.leftButton} rightButtonTextStyle={Styles.rightButton}>
       
                    
            <Scene initial key='tabsScreen' type="reset" component={SetupScreen} title='Setup' navBar={CustomNavBar} leftButton="hamburger"/>
                       
            <Scene key='userView' component={UserView} title='Setup - Users' navBar={CustomNavBar} onDelete="true" onConfirm="true" leftButton="back" setActionHandlers={(name,handler)=> actionHandlers[name]=handler} triggerActionHandler={(name)=>{actionHandlers[name]()}} />
              <Scene key='addUserView' component={UserView} title='Setup - Add Users' navBar={CustomNavBar} create={true} onConfirm="true" leftButton="back" setActionHandlers={(name,handler)=> actionHandlers[name]=handler} triggerActionHandler={(name)=>{actionHandlers[name]()}} />
            <Scene key='skillView' component={SkillView} title='Setup - Skills' navBar={CustomNavBar} onDelete="true" onConfirm="true" leftButton="back" setActionHandlers={(name,handler)=> actionHandlers[name]=handler} triggerActionHandler={(name)=>{actionHandlers[name]()}} />
             <Scene key='addSkillView' component={SkillView} title='Setup - Add Skills' navBar={CustomNavBar} create={true} onConfirm="true" leftButton="back" setActionHandlers={(name,handler)=> actionHandlers[name]=handler} triggerActionHandler={(name)=>{actionHandlers[name]()}} />
            <Scene key='roleView' component={RoleView} title='Setup - Role' navBar={CustomNavBar} onDelete="true" onConfirm="true" leftButton="back" setActionHandlers={(name,handler)=> actionHandlers[name]=handler} triggerActionHandler={(name)=>{actionHandlers[name]()}} />
              <Scene key='addRoleView' component={RoleView} title='Setup - Add Role' navBar={CustomNavBar} create={true} onConfirm="true" leftButton="back" setActionHandlers={(name,handler)=> actionHandlers[name]=handler} triggerActionHandler={(name)=>{actionHandlers[name]()}} />
            <Scene key='DepartmentSetup' component={DepartmentSetup} title='Setup - Department' navBar={CustomNavBar} onDelete="true" onConfirm="true" leftButton="back" setActionHandlers={(name,handler)=> actionHandlers[name]=handler} triggerActionHandler={(name)=>{actionHandlers[name]()}} />
            <Scene key='WorkflowSetup' component={WorkflowSetup} title='Setup - Workflow' navBar={CustomNavBar} onDelete="true" onConfirm="true" leftButton="back" setActionHandlers={(name,handler)=> actionHandlers[name]=handler} triggerActionHandler={(name)=>{actionHandlers[name]()}} />
            <Scene key='ChecklistSetup' component={ChecklistSetup} title='Setup - Checklist' navBar={CustomNavBar} onDelete="true" onConfirm="true" leftButton="back" setActionHandlers={(name,handler)=> actionHandlers[name]=handler} triggerActionHandler={(name)=>{actionHandlers[name]()}} />

            <Scene key='profileScreen' component={ProfileScreen} title='Profile' navBar={CustomNavBar} leftButton="hamburger"/>
            <Scene key='calendarScreen' component={CalendarScreen} title='Calendar' navBar={CustomNavBar} leftButton="hamburger"/>
              <Scene key='HolidayScreen' component={HolidayScreen} navBar={CustomNavBar} leftButton="hamburger" />
              <Scene key='CellExpandView' component={CellExpandView} navBar={CustomNavBar} leftButton="back" />
              <Scene key='PatternView' component={PatternView} title = 'Create Pattern' navBar={CustomNavBar} onDelete= "true" onConfirm="true"  leftButton="back" setActionHandlers={(name,handler)=> actionHandlers[name]=handler} triggerActionHandler={(name)=>{actionHandlers[name]()}} />
            <Scene key='historyScreen' component={HistoryScreen} title='History' navBar={CustomNavBar} leftButton="hamburger"/>
            <Scene key='messagesScreen' component={MessagesScreen}  navBar={CustomNavBar} leftButton="hamburger"/>
            <Scene key='murphyScreen' component={MurphyScreen} title='Murphy' navBar={CustomNavBar} leftButton="hamburger"/>
            <Scene key='reportingScreen' component={ReportingScreen} title='Reporting' navBar={CustomNavBar} leftButton="hamburger"/>
            <Scene key='workScreen' component={WorkScreen} title='Work' navBar={CustomNavBar} leftButton="hamburger"/>


            {/* Custom navigation bar example */}
          </Scene>
        </Scene>
      </Router>
    )
  }
}

// <Scene key='userManagementScreen' component={UserManagementScreen} title='User Management Screen' />

export default NavigationRouter


 