// @flow

const colors = {
  background: '#ffff',//'#f0f0f0',
  clear: 'rgba(0,0,0,0)',
  facebook: '#3b5998',
  transparent: 'rgba(0,0,0,0)',
  silver: '#F7F7F7',
  steel: '#CCCCCC',
  error: 'rgba(200, 0, 0, 0.8)',
  ricePaper: 'rgba(255,255,255, 0.75)',
  frost: '#D8D8D8',
  cloud: 'rgba(200,200,200, 0.35)',
  windowTint: 'rgba(0, 0, 0, 0.4)',
  panther: '#161616',
  charcoal: '#595959',
  coal: '#2d2d2d',
  bloodOrange: '#fb5f26',
  snow: '#a3a3a3',
  ember: 'rgba(0, 0, 0, 0.5)',
  fire: '#e73536',
  drawer: 'rgba(0, 0, 0, 0.95)',
  black:'rgba(0,0,0,1)'
}

export default colors
