// @flow

// leave off @2x/@3x
const images = {
  logo: require('../Images/logo.png'),
  clearLogo: require('../Images/top_logo.png'),
  ignite: require('../Images/ignite_logo.png'),
  tileBg: require('../Images/tile_bg.png'),
  background: require('../Images/BG.png'),
  Loginback: require('../Images/Login.jpg'),
  Drawerback: require('../Images/nav.jpg'),
  clearIcon: require('../Images/clear.png'),
  backIcon: require('../Images/back.png'),
  FABIcon: require('../Images/FAB.png'),
  DefaultProfImg: require('../Images/DefaultProfile.png'),
  levelUp: require('../Images/levelUp.png')
}

export default images
