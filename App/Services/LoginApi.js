//import apisauce from 'apisauce'
import {api} from '../Lib/helpers'

// "constructor"
const create = () => {

  // Create and configure an apisauce-based api object. 
   api().setHeader({
     'Content-Type': 'application/json',
     'Accept': 'application/json'
   });

  // const api = apisauce.create({
  //   // base URL is read from the "constructor"
  //   baseURL,
  //   // default headers
  //   headers: {
  //     'Content-Type': 'application/json',
  //     'Accept': 'application/json'
  //   },
  //   // 10 second timeout...
  //   timeout: 10000
  // })
  // Define some functions that call the api.  
  //
  const getToken = (name, passwd, sitemail) => api().post('login', {username: name, password: passwd, siteEmail:'lasitha.petthawadu@bback.com'})
  

  // Return back a collection of functions that we would consider our
  // interface.  
  return {
    // a list of the API functions
    getToken
  }
}

// let's return back our create method as the default.
export default {
  create
}
