FROM lovato/react-native-android
MAINTAINER Lasitha Petthawadu <petthawadu3@gmail.com>
RUN mkdir /mobile
COPY . /mobile
WORKDIR  /mobile
RUN chmod 777 -R *.*
RUN npm install
RUN  cd /mobile/android && chmod 777 gradlew
WORKDIR /mobile/android
RUN gradle clean
CMD ["gradle", "testfairyRelease","--stacktrace"]